var gulp = require('gulp');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var fs = require("fs");
var browserify = require('browserify');
var vueify = require('vueify');
var envify = require('envify/custom');
var path = require('path');

function makeBundle(file) {
    var layouts = [ 'signin', 'master' ];
     for (var index in layouts) {
        var pattern = new RegExp(layouts[index] + '.*\.js');

        if (file.match(pattern) != null) {
            browserify(file)
                .transform(vueify)
                .transform({global: true}, envify('development'))
                .bundle()
                .pipe(fs.createWriteStream('public/js/'+ path.basename(file).slice(0, -3) +'.vue-bundle.js'));

            return true;
        }
    }

    makeVueBundles('development');
}

function makeVueBundles(env) {
    var layouts = [ 'signin', 'master' ];

    var files = fs.readdirSync('resources/assets/vue-application').filter( function( element ) {

        for (var index in layouts) {
            var pattern = new RegExp(layouts[index] + '.*\.js');

            if (element.match(pattern) != null) {
                return true;
            }
        }

        return false;
    });

    if (files.length) {
        for (var index in files) {
            var item = files[index];

            browserify('resources/assets/vue-application/' + item)
                .transform(vueify)
                .transform({global: true}, envify(env))
                .bundle()
                .pipe(fs.createWriteStream('public/js/'+ item.slice(0, -3) +'.vue-bundle.js'));
        }
    }
}

gulp.task('default', function() {
    makeVueBundles('development');
});

gulp.task('deploy', function() {
    makeVueBundles('production');
});

gulp.task('vendor', function() {
    var src = {
        js: [
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
            'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.uk.min.js',
            'node_modules/lodash/lodash.min.js',
            'node_modules/blockui-npm/jquery.blockUI.js',
            'node_modules/sweetalert/dist/sweetalert.min.js',
            'node_modules/moment/min/moment.min.js',
            'node_modules/sprintf-js/dist/sprintf.min.js',
            'node_modules/ion-rangeslider/js/ion.rangeSlider.min.js',
            'node_modules/metismenu/dist/metisMenu.min.js',
            'node_modules/bootstrap-filestyle/src/bootstrap-filestyle.min.js',
            'node_modules/jquery-slimscroll/jquery.slimscroll.min.js'
        ],
        styles: [
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'node_modules/ion-rangeslider/css/ion.rangeSlider.css',
            'node_modules/ion-rangeslider/css/ion.rangeSlider.skinFlat.css',
            'node_modules/metismenu/dist/metisMenu.css',
            'node_modules/vue-multiselect/dist/vue-multiselect.min.css',
            'node_modules/vue-select/dist/vue-select.css',
            'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        ]
    };

    gulp.src(src.js)
        .pipe(concat('vendor.build.js'))
        .pipe(gulp.dest('public/js'));

    gulp.src(src.styles)
        .pipe(concat('vendor.build.css'))
        .pipe(gulp.dest('public/css'));
});

gulp.watch('resources/assets/vue-application/**', function(e) {
    makeBundle(e.path);
});
