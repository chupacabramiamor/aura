<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Route::group([ 'prefix' => 'auth' ], function() {
    Route::get('login', 'AuthController@getLogin')->name('login');
    Route::post('login', 'AuthController@postLogin');
    Route::get('logout', 'AuthController@getLogout')->name('logout');
});

Route::group([ 'middleware' => 'auth' ], function() {
    Route::get('/home', 'HomeController@getIndex')->name('home');

    Route::group([ 'prefix' => 'sync' ], function() {
        Route::get('/', 'SyncController@getIndex');
        Route::post('/', 'SyncController@postIndex');
        Route::post('/uploading-confirm', 'SyncController@postUploadingConfirm');
    });

    Route::group([ 'prefix' => 'operators' ], function() {
        Route::get('/', 'OperatorsController@getIndex');
        Route::get('/remove/{id}', 'OperatorsController@getRemove');
    });

    Route::group([ 'prefix' => 'questionnaire' ], function() {
        Route::get('/', 'QuestionnaireController@getIndex');
        Route::get('/answer-groups', 'QuestionnaireController@getAnswerGroups');
        Route::get('/answer-groups/type-list', 'Resources\AnswerGroups@getTypeList');
    });

    Route::group([ 'prefix' => 'notifies' ], function() {
        Route::get('/data', 'NotificationsController@getData');
        Route::get('/row-data/{id}', 'NotificationsController@getRowData');
        Route::get('/mark-as-opened/{id}', 'NotificationsController@getMarkAsOpened');
    });

    Route::group(['prefix' => 'addresses'], function() {
        Route::get('/', 'AddressesController@getIndex')->name('address_list');
        Route::get('/add', 'AddressesController@getAdd')->name('address_add');
        Route::post('/add', 'AddressesController@postAdd');
        Route::get('/edit/{address}', 'AddressesController@getEdit');
        Route::post('/edit/{address}', 'AddressesController@postEdit');
        Route::get('/locality-data/{id}', 'AddressesController@getLocalityData');
        Route::get('/locality-list', 'AddressesController@getLocalityList');
        Route::get('/reject/{notify_id}', 'AddressesController@getReject');
        Route::post('/search', 'AddressesController@postSearch');
        Route::post('/send-creating-request', 'AddressesController@postSendCreatingRequest');
    });

    Route::group([ 'prefix' => 'streets' ], function() {
        Route::get('/', 'StreetsController@getIndex');
        Route::post('/', 'StreetsController@postIndex');
        Route::get('/{street}', 'StreetsController@getShow');
        Route::patch('/{street}', 'StreetsController@patchUpdate');
    });

    Route::group([ 'prefix' => 'voters' ], function () {
        Route::get('/', 'VotersController@getIndex')->name('voters');
        Route::get('/add', 'VotersController@gatAdd');
        Route::post('/add', 'VotersController@postAdd');
        Route::get('/edit/{id}', 'VotersController@getEdit');
        Route::post('/edit/{id}', 'VotersController@postEdit');
        Route::post('/search', 'VotersController@postSearch');
        Route::get('/answers/{questionnaire_id}/{voter_id}', 'VotersController@getAnswers');
        Route::post('/answer/{inquire_id}/{voter_id}', 'VotersController@postAnswer');
        Route::get('/remove/{id}', 'VotersController@getRemove');
        Route::post('/export', 'VotersController@postExport');
    });

    Route::group(['prefix' => 'resources'], function() {
        Route::resource('operators', 'Resources\Operators');
        Route::resource('questionnaires', 'Resources\Questionnaires');
        Route::resource('answer-groups', 'Resources\AnswerGroups');
        Route::resource('answers', 'Resources\Answers');
        Route::resource('inquiries', 'Resources\Inquiries');
    });

    Route::group([ 'prefix' => 'svcs'], function() {
        Route::get('psc-list', 'SvcsController@getPscs');
        Route::get('education-list', 'SvcsController@getEducationList');
        Route::get('occupation-list', 'SvcsController@getOccupacyList');
        Route::get('party-list', 'SvcsController@getPartyList');
        Route::get('locality-list', 'SvcsController@getLocalityList');
        Route::get('address-list', 'SvcsController@getAddressList');
        Route::get('street-type-list', 'SvcsController@getStreetTypeList');
        Route::get('questionnaires', 'SvcsController@getQuestionnaires');
        Route::get('answer-list', 'SvcsController@getAnswerList');
        Route::get('campaign-positions-list', 'SvcsController@getCampaignPositionsList');
        Route::get('roles-list', 'SvcsController@getRolesList');
        Route::get('streets', 'SvcsController@getStreetList');
        Route::get('dictonary', 'SvcsController@getDictonary');
    });
});