<?php
namespace Andruleez\DataImport\Exceptions;

use Exception;

/**
 * 	DocumentStructureException
 */
class DocumentStructureException extends Exception
{

	function __construct($message = '', $code = 0, Exception $previous = null)
	{
		$this->message = $message;
		parent::__construct($message, $code, $previous);
	}
}