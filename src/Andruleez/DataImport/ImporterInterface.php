<?php
namespace Andruleez\DataImport;

interface ImporterInterface {

	public function prepare($worksheetIndex = 0);
	public static function commit($filename);
	public function getStat();
}