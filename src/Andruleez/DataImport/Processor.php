<?php
namespace Andruleez\DataImport;

use Symfony\Component\HttpFoundation\File\File;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;
use Carbon\Carbon;
use Auth;
use App;
use DB;

/**
 *  Processor
 */
abstract class Processor
{
    protected $spreadsheet;
    protected $pscList = [];
    protected $localityList = [];
    protected $rowSkeleton = [];
    protected $headerMapping;

    public function __construct(File $file)
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $this->spreadsheet = $reader->load($file->getPathname());

        $this->log = App::make(\Monolog\Logger::class);
        $this->pscList = DB::select('SELECT * FROM `pscs`');
        $this->localityList = DB::select('SELECT * FROM `localities`');
    }


    public function prepare($worksheetIndex = 0)
    {
        $worksheet = $this->spreadsheet->getSheet($worksheetIndex);

        $result = [];
        $startRow = 1;
        $chunkSize = 300;

        while ($startRow < 200000) {

            if ($startRow > $worksheet->getHighestRow()) {
                break;
            }

            $this->log->addDebug('startRow', [ $startRow, $this->convertSize(memory_get_usage(true)) ]);

            foreach ($worksheet->getRowIterator($startRow, $startRow + $chunkSize - 1) as $row) {
                $output = $this->rowSkeleton;

                if ($row->getRowIndex() == 1) {
                    $this->verifyHeaderRow($row);
                    continue;
                }

                if ($worksheet->getCellByColumnAndRow(1, $row->getRowIndex())->getValue() == null) {
                    $this->log->addDebug('end_of_spreadsheet', [ $row->getRowIndex() ]);
                    break;
                }

                foreach (array_keys($this->headerMapping) as $index => $alias) {
                    $cell = $worksheet->getCellByColumnAndRow($index + 1, $row->getRowIndex());

                    $methodName = $this->getHandleMethodName($alias);
                    $this->$methodName($cell, $output, $row);
                }

                $result[] = $output;
            }

            $startRow += $chunkSize;
        }

        file_put_contents(storage_path('_import.s'), serialize($result));

        $filename = md5_file(storage_path('_import.s')) . '.s';

        rename(storage_path('_import.s'), storage_path($filename));

        $this->stat['prepared_count'] = count($result);
        $this->stat['filename'] = $filename;

        $this->log->addDebug('statistic', [ $this->stat ]);
        $this->log->addDebug('filename', [ count($filename) ]);
    }


    /*public static function commit($filename)
    {
    }*/


    public function getStat()
    {
        return $this->stat;
    }


    public static function convertSize($size)
    {
        $unit = [ 'b','kb','Mb','Gb','Tb','pb' ];
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).$unit[$i];
    }


    private function verifyHeaderRow(Row $row)
    {
        $cellIterator = $row->getCellIterator();
        $index = 0;

        foreach ($cellIterator as $cell) {
            if (isset(array_values($this->headerMapping)[$index])) {
                $title = array_values($this->headerMapping)[$index];

                if (strpos($cell->getValue(), $title) === false) {
                    throw new Exceptions\DocumentStructureException("columns_mismatch:{$index}:".$cell->getValue());
                }
            }

            $index++;
        }
    }


    protected function processCell($value, $pattern)
    {
        if (preg_match("~". $pattern ."~u", $value, $matches) == 0) {
            throw new Exceptions\DocumentStructureException("invalid_content:{$value}:{$pattern}");
        }

        return $matches;
    }


    protected function getHandleMethodName($alias)
    {
        $value = 'handle_' . $alias;
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/_([a-z])/', $func, $value);
    }

}