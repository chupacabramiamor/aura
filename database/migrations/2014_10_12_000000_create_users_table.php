<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname')->nullable();
            $table->string('login')->unique();
            $table->string('password');
            $table->integer('role_id');
            $table->boolean('is_enabled')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Eloquent::unguard();
        User::create([ 'login' => 'sa', 'password' => 'U8UcD9', 'role_id' => User::ROLE_SA ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
