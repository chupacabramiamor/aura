<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePscsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pscs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locality_id')->unsigned();
            $table->string('name', 200);
            $table->text('placement')->nullable();
            $table->boolean('is_hidden')->default(0);
        });

        Schema::create('psc_user', function(Blueprint $table) {
            $table->integer('psc_id')->unsigned();
            $table->integer('user_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pscs');
        Schema::dropIfExists('psc_user');
    }
}
