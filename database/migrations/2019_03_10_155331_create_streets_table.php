<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Street;

class CreateStreetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->tinyInteger('type')->unsigned()->default(Street::TYPE_UNDEFINED);
            $table->integer('locality_id')->unsigned();
            $table->integer('prev_id')->unsigned()->nullable()->unique();
            $table->boolean('is_outated')->default(0);
            $table->timestamp('created_at');

            $table->unique([ 'name', 'type', 'locality_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streets');
    }
}
