<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifies', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->unsigned()->default(0);
            $table->string('title', 50);
            $table->string('message', 100);
            $table->integer('initiator_id')->unsigned();
            $table->integer('receiver_id')->unsigned()->default(0);
            $table->integer('broadcast_for')->default(0);
            $table->boolean('is_opened')->default(0);
            $table->text('payload')->nullable();
            $table->timestamp('created_at');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifies');
    }
}
