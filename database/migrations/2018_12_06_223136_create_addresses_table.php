<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locality_id')->unsigned();
            $table->integer('psc_id')->unsigned();
            $table->char('postcode', 5)->nullable();
            $table->tinyInteger('street_type')->unsigned();
            $table->string('street_name', 100);
            $table->string('house_number', 10);
            $table->string('apartment_number', 5)->nullable();
            $table->string('building', 20)->nullable();
            $table->boolean('is_newspaper_subscribed')->default(0);

            $table->index([ 'locality_id', 'street_name' ]);
            $table->unique([ 'locality_id', 'street_type', 'street_name', 'house_number', 'apartment_number', 'building' ], 'locality');
        });

        DB::statement("ALTER TABLE `addresses` ADD FULLTEXT search(postcode, street_name, house_number, apartment_number, building)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
