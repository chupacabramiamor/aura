<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Role;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alias');
            $table->text('description')->nullable();
        });

        Role::unguard();
        Role::create([ 'name' => 'Super Administration', 'alias' => 'sa' ]);
        Role::create([ 'name' => 'Organization Administration', 'alias' => 'admins' ]);
        Role::create([ 'name' => 'Organization Managers', 'alias' => 'manager' ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }
}
