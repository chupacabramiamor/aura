<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkplaceColumnToVoters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voters', function (Blueprint $table) {
            $table->tinyInteger('workposition_id')->unsigned()->nullable()->after('campaign_position');
            $table->tinyInteger('workplace_id')->unsigned()->nullable()->after('campaign_position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voters', function (Blueprint $table) {
            $table->dropColumn('workplace_id');
            $table->dropColumn('workposition_id');
        });
    }
}
