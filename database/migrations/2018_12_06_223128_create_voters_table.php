<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('address_id')->unsigned();
            $table->string('surname', 30);
            $table->string('firstname', 30);
            $table->string('patronymic', 30)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->string('additional_phone_number', 20)->nullable();
            $table->date('birthday')->nullable();
            $table->tinyInteger('education_type')->unsigned()->nullable();
            $table->tinyInteger('gender_type')->unsigned()->nullable();
            $table->tinyInteger('occupation_type')->unsigned()->nullable();
            $table->tinyInteger('campaign_position')->unsigned()->nullable();
            $table->tinyInteger('party_id')->unsigned()->default(1);
            $table->text('birth_locality')->nullable();
            $table->text('description')->nullable();

            $table->index([ 'address_id' ]);
            $table->index([ 'address_id', 'surname' ]);
        });

        DB::statement("ALTER TABLE `voters` ADD FULLTEXT search(surname, firstname, patronymic, phone_number, additional_phone_number)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voters');
        Schema::dropIfExists('voter_answer');
    }
}
