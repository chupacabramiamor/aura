<?php

use Faker\Generator as Faker;
use App\Models;

$factory->define(Models\Voter::class, function(Faker $faker) {
    $addresses = Models\Address::where('psc_id', 1)->pluck('id');

    return [
        'address_id' => $faker->randomElement($addresses),
        'surname' => $faker->lastName,
        'firstname' => $faker->firstName(),
        'patronymic' => '',
        'phone_number' => $faker->e164PhoneNumber,
        'additional_phone_number' => $faker->e164PhoneNumber,
        'birthday' => $faker->dateTimeThisCentury(),
        'description' => 'For testing only'
    ];
});

$factory->define(Models\VoterAnswer::class, function (Faker $faker) {
    $voter = Models\Voter::with('answers')->inRandomOrder()
        ->whereHas('address.psc', function($q) {
            $q->where('name', '999999');
        })
        ->first();

    $inquire = Models\Inquire::inRandomOrder()
        ->with('group.answers')
        ->where('questionnaire_id', 1)
        ->whereHas('group', function($q) {
            $q->where('type_id', Models\AnswerGroup::TYPE_LIST);
        })
        ->get()
        ->filter(function($inquire) use($voter) {
            return !in_array($inquire->id, $voter->answers->toArray());
        })->first();

    return [
        'voter_id' => $voter->id,
        'inquire_id' => $inquire->id,
        'answer_id' => $faker->randomElement($inquire->group->answers),
    ];
});

