<?php

use Faker\Generator as Faker;
use App\Models;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Models\Notify::class, function(Faker $faker) {

    $localities = Models\Locality::all()->pluck('id');
    $pscs = Models\Psc::all()->pluck('id');
    $streetTypes = Models\Address::getStreetTypes();
    $inititators = Models\User::where('role_id', Models\User::ROLE_OPERATOR)->get()->pluck('id');
    $receivers = Models\User::where('role_id', Models\User::ROLE_ADMINISTRATOR)->get()->pluck('id');

    $payload = [
        'locality_id' => $faker->randomElement($localities),
        'psc_id' => $faker->randomElement($pscs),
        'postcode' => $faker->numberBetween(10000, 99999),
        'street_type' => $faker->randomElement($streetTypes),
        'street_name' => $faker->streetName,
        'house_number' => $faker->buildingNumber,
        'apartment_number' => $faker->numberBetween(1, 200),
        'building' => '',
    ];

    return [
        'type' => Models\Notify::TYPE_NEW_ADDR_REQUEST,
        'title' => 'Нова адреса',
        'message' => implode(', ', [ $payload['street_name'], $payload['house_number'], $payload['apartment_number'] ]),
        'initiator_id' => $faker->randomElement($inititators),
        'receiver_id' => $faker->randomElement($receivers),
        'payload' => $payload
    ];
});

$factory->define(Models\Address::class, function(Faker $faker) {
    $localities = Models\Locality::whereName('Кременчук')->pluck('id');
    $psc = Models\Psc::whereName('999999')->first();
    $streetTypes = Models\Address::getStreetTypes();

    return  [
        'locality_id' => $faker->randomElement($localities),
        'psc_id' => $psc->id,
        'postcode' => $faker->numberBetween(10000, 99999),
        'street_type' => $faker->randomElement($streetTypes),
        'street_name' => $faker->streetName,
        'house_number' => $faker->buildingNumber,
        'apartment_number' => $faker->numberBetween(1, 200),
        'building' => '',
    ];
});
