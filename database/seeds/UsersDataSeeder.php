<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        User::create([ 'login' => 'figaro', 'password' => 'Napalm', 'role_id' => User::ROLE_ADMINISTRATOR ]);
        User::create([ 'login' => 'admin', 'password' => 'tatHx4', 'role_id' => User::ROLE_ADMINISTRATOR ]);

        User::create([ 'login' => 'manager1', 'password' => 'kREUvM', 'role_id' => User::ROLE_OPERATOR ]);
        User::create([ 'login' => 'manager2', 'password' => 'kREUvM', 'role_id' => User::ROLE_OPERATOR ]);
    }
}
