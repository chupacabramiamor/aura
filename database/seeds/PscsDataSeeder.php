<?php

use Illuminate\Database\Seeder;
use App\Models\Psc;

class PscsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $areas = [
            [ 'name' => '999999', 'locality_id' => 84, 'placement' => 'Тестовий ДВК. НЕ ВИКОРИСТОВУВАТИ!', 'is_hidden' => true ],
            [ 'name' => '531040', 'locality_id' => 84, 'placement' => 'Кременчуцький національний університет ім.М.Остроградського, корпус 1, хол' ],
            [ 'name' => '531041', 'locality_id' => 84, 'placement' => 'школа №10, актова зала' ],
            [ 'name' => '531042', 'locality_id' => 84, 'placement' => 'ліцей №4, хол' ],
            [ 'name' => '531043', 'locality_id' => 84, 'placement' => 'Дитячо-юнацька спортивна школа №2, спортивний зал, 1-й поверх' ],
            [ 'name' => '531044', 'locality_id' => 84, 'placement' => 'комітет товариства сприяння обороні України, каб.4' ],
            [ 'name' => '531045', 'locality_id' => 84, 'placement' => 'Льотний коледж, фойє, 1-й поверх' ],
            [ 'name' => '531046', 'locality_id' => 84, 'placement' => 'ВТФ "Кремтекс", хол' ],
            [ 'name' => '531047', 'locality_id' => 84, 'placement' => 'дитячий садок №32, спортивна зала, 1-й поверх' ],
            [ 'name' => '531048', 'locality_id' => 84, 'placement' => 'Кременчуцький хлібокомбінат, червоний куток' ],
            [ 'name' => '531049', 'locality_id' => 84, 'placement' => 'школа №2, хол' ],
            [ 'name' => '531050', 'locality_id' => 84, 'placement' => 'рада мікрорайону с.Ревівки, хол' ],
            [ 'name' => '531051', 'locality_id' => 84, 'placement' => 'клуб 1-ї міської лікарні, актова зала' ],
            [ 'name' => '531052', 'locality_id' => 84, 'placement' => 'завод залізо-бетонних виробів №2, їдальня' ],
            [ 'name' => '531053', 'locality_id' => 84, 'placement' => 'Кременчуцьке комунальне автотранспортне підприємство 1628, хол' ],
            [ 'name' => '531054', 'locality_id' => 84, 'placement' => 'ліцей імені А.С.Макаренка, їдальня' ],
            [ 'name' => '531055', 'locality_id' => 84, 'placement' => 'технічна бібліотека, зала мистецтв' ],
            [ 'name' => '531056', 'locality_id' => 84, 'placement' => 'дитячий садок №57, спортивна зала' ],
            [ 'name' => '531057', 'locality_id' => 84, 'placement' => 'гімназія №5, хол' ],
            [ 'name' => '531058', 'locality_id' => 84, 'placement' => 'ДНЗ №68, музична зала, 1-й поверх' ],
            [ 'name' => '531059', 'locality_id' => 84, 'placement' => 'ВАТ "Кременчукгаз", актова зала' ],
            [ 'name' => '531060', 'locality_id' => 84, 'placement' => 'ВПУ №7, спортивна зала' ],
            [ 'name' => '531061', 'locality_id' => 84, 'placement' => 'ВПУ №7, корпус 2, спортивна зала' ],
            [ 'name' => '531062', 'locality_id' => 84, 'placement' => 'школа №28, спортивна зала' ],
            [ 'name' => '531063', 'locality_id' => 84, 'placement' => 'школа №30, хол' ],
            [ 'name' => '531064', 'locality_id' => 84, 'placement' => 'палац культури "АвтоКраз", хол' ],
            [ 'name' => '531065', 'locality_id' => 84, 'placement' => 'школа №27, хол' ],
            [ 'name' => '531066', 'locality_id' => 84, 'placement' => 'філія бібліотеки №2, читальна зала' ],
            [ 'name' => '531067', 'locality_id' => 84, 'placement' => 'КІДУ, ауд.3104' ],
            [ 'name' => '531068', 'locality_id' => 84, 'placement' => 'колегіум №25, спортивна зала' ],
            [ 'name' => '531069', 'locality_id' => 84, 'placement' => 'НВК "Дніпряночка", музична зала' ],
            [ 'name' => '531070', 'locality_id' => 84, 'placement' => 'філія №8 ради мікрорайону, фойє' ],
            [ 'name' => '531071', 'locality_id' => 84, 'placement' => 'КП "Кременчукводоканал", актова зала' ],
            [ 'name' => '531072', 'locality_id' => 84, 'placement' => 'гуртожиток Кременчуцького педагогічного училища, хол' ],
            [ 'name' => '531073', 'locality_id' => 84, 'placement' => 'КІДУ, корпус 4, хол' ],
            [ 'name' => '531074', 'locality_id' => 84, 'placement' => 'КП "Міськсвітло", хол, 1-й поверх' ],
            [ 'name' => '531075', 'locality_id' => 84, 'placement' => 'Кременчуцький медичний коледж імені В.І.Литвиненка' ],
            [ 'name' => '531076', 'locality_id' => 84, 'placement' => 'школа №18, к.7' ],
            [ 'name' => '531077', 'locality_id' => 84, 'placement' => 'спеціалізована школа-інтернат, хол' ],
            [ 'name' => '531078', 'locality_id' => 84, 'placement' => 'еколого-натуралістичний центр, актова зала' ],
            [ 'name' => '531079', 'locality_id' => 84, 'placement' => 'школа №24, початкові класи, хол' ],
            [ 'name' => '531080', 'locality_id' => 84, 'placement' => 'школа №24, кабінет історії' ],
            [ 'name' => '531081', 'locality_id' => 84, 'placement' => 'рада мікрорайону №10, актова зала' ],
            [ 'name' => '531082', 'locality_id' => 84, 'placement' => 'приміщення ПП "Плахотнік Г.О.", 1-й поверх, хол' ],
            [ 'name' => '531083', 'locality_id' => 84, 'placement' => 'лікарня №4, актова зала' ],
            [ 'name' => '531084', 'locality_id' => 84, 'placement' => 'дитячий садок №80, музична зала' ],
            [ 'name' => '531085', 'locality_id' => 84, 'placement' => 'спортивний комплекс КНУ ім. Остроградського, спортивна зала' ],
            [ 'name' => '531101', 'locality_id' => 84, 'placement' => 'рада громадської самоорганізації населення мікрорайону №3, зал засідань)' ],
            [ 'name' => '531102', 'locality_id' => 84, 'placement' => 'ліцей №11, хол' ],
            [ 'name' => '531103', 'locality_id' => 84, 'placement' => 'школа №14, актовий зал' ],
            [ 'name' => '531104', 'locality_id' => 84, 'placement' => 'ДЮСШ №1, тренувальний зал' ],
            [ 'name' => '531105', 'locality_id' => 84, 'placement' => 'палац культури ПАТ "Кредмаш", хол' ],
            [ 'name' => '531106', 'locality_id' => 84, 'placement' => 'ПВНЗ КУЕІТУ, хол' ],
            [ 'name' => '531107', 'locality_id' => 84, 'placement' => 'школа №19, хол' ],
            [ 'name' => '531108', 'locality_id' => 84, 'placement' => 'Кременчуцький міський центр позашкільної освіти, хол' ],
            [ 'name' => '531109', 'locality_id' => 84, 'placement' => 'професійно-технічне училище №26, к.3' ],
            [ 'name' => '531110', 'locality_id' => 84, 'placement' => 'школа №1, їдальня' ],
            [ 'name' => '531111', 'locality_id' => 84, 'placement' => 'клуб локомотивного депо, актовий зал' ],
            [ 'name' => '531112', 'locality_id' => 84, 'placement' => 'дитячий дошкільний заклад №60, спортзал' ],
            [ 'name' => '531113', 'locality_id' => 84, 'placement' => 'школа №16, вестибюль' ],
            [ 'name' => '531114', 'locality_id' => 84, 'placement' => 'технікум залізничного транспорту, гімнаст. Зал' ],
            [ 'name' => '531115', 'locality_id' => 84, 'placement' => 'Кременчуцька дистанція колії, зал засідань' ],
            [ 'name' => '531116', 'locality_id' => 84, 'placement' => 'школа №3, їдальня' ],
            [ 'name' => '531117', 'locality_id' => 84, 'placement' => 'молодіжний центр, великий зал' ],
            [ 'name' => '531118', 'locality_id' => 84, 'placement' => 'Кременчуцький коледж Кременчуцького національного університету, фойє' ],
            [ 'name' => '531119', 'locality_id' => 84, 'placement' => 'школа №9, їдальня' ],
            [ 'name' => '531120', 'locality_id' => 84, 'placement' => 'ПК ім. Котлова, дзеркальний зал' ],
            [ 'name' => '531121', 'locality_id' => 84, 'placement' => 'Свято-Успенська церква, актовий зал' ],
            [ 'name' => '531122', 'locality_id' => 84, 'placement' => 'школа №23, фойє' ],
            [ 'name' => '531123', 'locality_id' => 84, 'placement' => 'допоміжна школа-інтернат, актовий зал' ],
            [ 'name' => '531124', 'locality_id' => 84, 'placement' => 'школа №7, фойє' ],
            [ 'name' => '531125', 'locality_id' => 84, 'placement' => 'Державне підприємство "УППДАЗТУ" Крюківське кар’єроуправління, актовий зал' ],
            [ 'name' => '531126', 'locality_id' => 84, 'placement' => 'школа №29, спортзал' ],
            [ 'name' => '531127', 'locality_id' => 84, 'placement' => 'центр культури і дозвілля молоді, 1-й корпус, хол' ],
            [ 'name' => '531128', 'locality_id' => 84, 'placement' => 'гімназія №6, хол' ],
            [ 'name' => '531129', 'locality_id' => 84, 'placement' => 'КГЖЕП "Крюківське", хол' ],
            [ 'name' => '531130', 'locality_id' => 84, 'placement' => 'поліклініка 2-ї міської лікарні, хол' ],
            [ 'name' => '531131', 'locality_id' => 84, 'placement' => 'рада громадської самоорганізації населення мікрорайону №13, актовий зал' ],
            [ 'name' => '531132', 'locality_id' => 84, 'placement' => 'професійно-технічне училище №6, хол' ],
            [ 'name' => '531133', 'locality_id' => 84, 'placement' => 'гуртожиток сталеливарного заводу, хол' ],
            [ 'name' => '531134', 'locality_id' => 84, 'placement' => 'центр культури і дозвілля молоді, 2-й корпус, актовий зал' ],
            [ 'name' => '531135', 'locality_id' => 84, 'placement' => 'школа №22, хол' ],
            [ 'name' => '530089', 'locality_id' => 63, 'placement' => 'школа №5, фойє' ],
            [ 'name' => '530090', 'locality_id' => 63, 'placement' => 'приміщення райспоживспілки, актовий зал' ],
            [ 'name' => '530091', 'locality_id' => 63, 'placement' => 'школа №4, фойє' ],
            [ 'name' => '530092', 'locality_id' => 63, 'placement' => 'гімназія №1, хореографічний клас' ],
            [ 'name' => '530093', 'locality_id' => 63, 'placement' => 'школа №3, фойє' ],
            [ 'name' => '530094', 'locality_id' => 132, 'placement' => 'школа, фойє' ],
            [ 'name' => '530095', 'locality_id' => 98, 'placement' => 'школа, с.Новомосковське, фойє' ],
            [ 'name' => '530096', 'locality_id' => 67, 'placement' => 'селищний будинок культури, фойє' ],
            [ 'name' => '530097', 'locality_id' => 67, 'placement' => 'лікарня, актовий зал' ],
            [ 'name' => '530098', 'locality_id' => 67, 'placement' => 'зональний будинок культури, фойє' ],
            [ 'name' => '530099', 'locality_id' => 128, 'placement' => 'школа, фойє' ],
            [ 'name' => '530100', 'locality_id' => 53, 'placement' => 'школа, спортивний зал' ],
            [ 'name' => '530101', 'locality_id' => 55, 'placement' => 'сільський клуб, фойє' ],
            [ 'name' => '530102', 'locality_id' => 56, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530103', 'locality_id' => 102, 'placement' => 'сільський клуб, фойє' ],
            [ 'name' => '530104', 'locality_id' => 81, 'placement' => 'сільський клуб, актовий зал' ],
            [ 'name' => '530105', 'locality_id' => 57, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530106', 'locality_id' => 64, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530107', 'locality_id' => 68, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530108', 'locality_id' => 72, 'placement' => 'контора товариства з обмеженою відповідальністю "Лан-Агро", актовий зал' ],
            [ 'name' => '530109', 'locality_id' => 73, 'placement' => 'адмінбудинок товариства з обмеженою відповідальністю "Юнігрейн", каб.1, 2' ],
            [ 'name' => '530110', 'locality_id' => 75, 'placement' => 'сільський клуб, фойє' ],
            [ 'name' => '530111', 'locality_id' => 92, 'placement' => 'школа, каб.1' ],
            [ 'name' => '530112', 'locality_id' => 116, 'placement' => 'сільський клуб, фойє' ],
            [ 'name' => '530113', 'locality_id' => 76, 'placement' => 'Зубанівська школа, каб.1, 2' ],
            [ 'name' => '530114', 'locality_id' => 120, 'placement' => 'школа, каб.2' ],
            [ 'name' => '530115', 'locality_id' => 77, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530116', 'locality_id' => 61, 'placement' => 'школа, спортивний зал' ],
            [ 'name' => '530117', 'locality_id' => 60, 'placement' => 'зональний будинок культури, фойє' ],
            [ 'name' => '530118', 'locality_id' => 60, 'placement' => 'школа, фойє' ],
            [ 'name' => '530119', 'locality_id' => 125, 'placement' => 'адмінприміщення виробничого підрозділу агрофірми "Степове", актовий зал' ],
            [ 'name' => '530120', 'locality_id' => 131, 'placement' => 'виробничий підрозділ агрофірми "Степове", каб.1' ],
            [ 'name' => '530121', 'locality_id' => 86, 'placement' => 'Куп’єватівська школа, спортивний зал' ],
            [ 'name' => '530122', 'locality_id' => 94, 'placement' => 'сільський клуб, фойє' ],
            [ 'name' => '530123', 'locality_id' => 93, 'placement' => 'сільська рада, зал засідань' ],
            [ 'name' => '530124', 'locality_id' => 87, 'placement' => 'школа, спортивний зал' ],
            [ 'name' => '530125', 'locality_id' => 99, 'placement' => 'сільська рада, зал засідань' ],
            [ 'name' => '530126', 'locality_id' => 101, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530127', 'locality_id' => 103, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530128', 'locality_id' => 127, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530129', 'locality_id' => 104, 'placement' => 'школа, фойе' ],
            [ 'name' => '530130', 'locality_id' => 135, 'placement' => 'фельдшерсько-акушерський пункт, каб.2' ],
            [ 'name' => '530131', 'locality_id' => 107, 'placement' => 'сільський будинок культури, зал 2' ],
            [ 'name' => '530132', 'locality_id' => 107, 'placement' => 'школа, їдальня' ],
            [ 'name' => '530133', 'locality_id' => 78, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530134', 'locality_id' => 112, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530135', 'locality_id' => 95, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530136', 'locality_id' => 133, 'placement' => 'контора товариства з обмеженою відповідальністю "Агро-Сула", актовий зал' ],
            [ 'name' => '530137', 'locality_id' => 113, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530138', 'locality_id' => 114, 'placement' => 'сільський будинок культури, глядацький зал' ],
            [ 'name' => '530139', 'locality_id' => 123, 'placement' => 'школа, актовий зал' ],
            [ 'name' => '530140', 'locality_id' => 88, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530141', 'locality_id' => 129, 'placement' => 'школа, фойє' ],
            [ 'name' => '530142', 'locality_id' => 108, 'placement' => 'фельдшерсько-акушерський пункт, фойє' ],
            [ 'name' => '530143', 'locality_id' => 126, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530144', 'locality_id' => 124, 'placement' => 'будинок культури, глядацький зал' ],
            [ 'name' => '530145', 'locality_id' => 63, 'placement' => 'Глобинська центральна районна лікарня' ],
            [ 'name' => '530379', 'locality_id' => 54, 'placement' => 'навчально-виховний комплекс, фойє' ],
            [ 'name' => '530380', 'locality_id' => 58, 'placement' => 'фельдшерсько-акушерський пункт, фойє' ],
            [ 'name' => '530381', 'locality_id' => 54, 'placement' => 'сільський будинок культури, глядацький зал' ],
            [ 'name' => '530382', 'locality_id' => 130, 'placement' => 'сільський клуб, фойє' ],
            [ 'name' => '530384', 'locality_id' => 65, 'placement' => 'навчально-виховний комплекс, фойє' ],
            [ 'name' => '530385', 'locality_id' => 69, 'placement' => 'сільська рада, зал засідань' ],
            [ 'name' => '530386', 'locality_id' => 117, 'placement' => 'фельдшерсько-акушерський пункт, фойє' ],
            [ 'name' => '530387', 'locality_id' => 74, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530388', 'locality_id' => 121, 'placement' => 'школа, фойє' ],
            [ 'name' => '530389', 'locality_id' => 121, 'placement' => 'амбулаторія загальної практики сімейної медицини, фойє' ],
            [ 'name' => '530390', 'locality_id' => 79, 'placement' => 'центр культури і дозвілля, фойє' ],
            [ 'name' => '530391', 'locality_id' => 79, 'placement' => 'сільська рада, зал засідань' ],
            [ 'name' => '530392', 'locality_id' => 118, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530393', 'locality_id' => 80, 'placement' => 'сільська рада, зал засідань' ],
            [ 'name' => '530394', 'locality_id' => 83, 'placement' => 'сільський будинок культури, глядацький зал' ],
            [ 'name' => '530395', 'locality_id' => 89, 'placement' => 'школа, фойє' ],
            [ 'name' => '530396', 'locality_id' => 105, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530397', 'locality_id' => 90, 'placement' => 'навчально-виховний комплекс, актовий зал' ],
            [ 'name' => '530398', 'locality_id' => 96, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530399', 'locality_id' => 70, 'placement' => 'фельдшерсько-акушерський пункт, фойє' ],
            [ 'name' => '530400', 'locality_id' => 100, 'placement' => 'навчально-виховний комплекс, спортивний зал' ],
            [ 'name' => '530401', 'locality_id' => 59, 'placement' => 'фельдшерсько-акушерський пункт, фойє' ],
            [ 'name' => '530402', 'locality_id' => 106, 'placement' => 'центр культури і дозвілля, фойє' ],
            [ 'name' => '530403', 'locality_id' => 106, 'placement' => 'амбулаторія загальної практики сімейної медицини, фойє' ],
            [ 'name' => '530404', 'locality_id' => 85, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530405', 'locality_id' => 109, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530406', 'locality_id' => 91, 'placement' => 'школа, фойє' ],
            [ 'name' => '530407', 'locality_id' => 110, 'placement' => 'сільський клуб, глядацький зал' ],
            [ 'name' => '530408', 'locality_id' => 111, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530409', 'locality_id' => 71, 'placement' => 'фельдшерсько-акушерський пункт, фойє' ],
            [ 'name' => '530410', 'locality_id' => 119, 'placement' => 'школа, актовий зал' ],
            [ 'name' => '530411', 'locality_id' => 115, 'placement' => 'сільський будинок культури, глядацький зал' ],
            [ 'name' => '530412', 'locality_id' => 97, 'placement' => 'сільський клуб, фойє' ],
            [ 'name' => '530413', 'locality_id' => 62, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530414', 'locality_id' => 122, 'placement' => 'школа, актовий зал' ],
            [ 'name' => '530415', 'locality_id' => 134, 'placement' => 'сільський будинок культури, фойє' ],
            [ 'name' => '530416', 'locality_id' => 82, 'placement' => 'фельдшерсько-акушерський пункт, фойє' ],
            [ 'name' => '530983', 'locality_id' => 66, 'placement' => 'технікум, фойє' ],
            [ 'name' => '530984', 'locality_id' => 66, 'placement' => 'Вище професійне гірниче училище, фойє' ],
            [ 'name' => '530985', 'locality_id' => 66, 'placement' => 'школа №1, фойє' ],
            [ 'name' => '530986', 'locality_id' => 66, 'placement' => 'палац культури та творчості, фойє' ],
            [ 'name' => '530987', 'locality_id' => 66, 'placement' => 'палац культури та творчості, фойє' ],
            [ 'name' => '530988', 'locality_id' => 66, 'placement' => 'фізкультурно-оздоровчий комплекс, спортзала' ],
            [ 'name' => '530989', 'locality_id' => 66, 'placement' => 'школа №6, вестибюль, 1-й поверх' ],
            [ 'name' => '530990', 'locality_id' => 66, 'placement' => 'школа №6, вестибюль, 2-й поверх' ],
            [ 'name' => '530991', 'locality_id' => 66, 'placement' => 'виконком Комсомольської міської ради, фойє великої зали' ],
            [ 'name' => '530992', 'locality_id' => 66, 'placement' => 'гімназія ім.В.О.Нижниченка, фойє актової зали' ],
            [ 'name' => '530993', 'locality_id' => 66, 'placement' => 'гімназія ім.В.О.Нижниченка, фойє' ],
            [ 'name' => '530994', 'locality_id' => 66, 'placement' => 'школа №2, фойє' ],
            [ 'name' => '530995', 'locality_id' => 66, 'placement' => 'школа №2, спортзала' ],
            [ 'name' => '530996', 'locality_id' => 66, 'placement' => 'гуртожиток ВАТ "ПГЗК", кімната відпочинку' ],
            [ 'name' => '530997', 'locality_id' => 66, 'placement' => 'спортивний комплекс "Динамо", спортзала' ],
            [ 'name' => '530998', 'locality_id' => 66, 'placement' => 'навчально-виховний комплекс ім. Л.І.Бугаєвської, вестибюль' ],
            [ 'name' => '530999', 'locality_id' => 66, 'placement' => 'школа №4, вестибюль' ],
            [ 'name' => '531000', 'locality_id' => 66, 'placement' => 'Дмитрівська школа, фойє' ],
            [ 'name' => '531274', 'locality_id' => 66, 'placement' => 'Центральна публічна бібліотека, фойє' ],
            [ 'name' => '531275', 'locality_id' => 66, 'placement' => 'Міська бібліотека для дітей, фойє' ],
            [ 'name' => '531001', 'locality_id' => 66, 'placement' => 'Комсомольський виправний центр, кімната виховної роботи відділення СПС-2' ],
            [ 'name' => '531086', 'locality_id' => 84, 'placement' => 'школа №12, спортивна зала' ],
            [ 'name' => '531087', 'locality_id' => 84, 'placement' => 'навчально-виробничий комбінат, автомобільний клас' ],
            [ 'name' => '531088', 'locality_id' => 84, 'placement' => 'дитячий садок №50, музична зала' ],
            [ 'name' => '531089', 'locality_id' => 84, 'placement' => 'школа №31, хол' ],
            [ 'name' => '531090', 'locality_id' => 84, 'placement' => 'школа №31, вестибюль, 1-й поверх' ],
            [ 'name' => '531091', 'locality_id' => 84, 'placement' => 'палац культури "Нафтохімік", хол' ],
            [ 'name' => '531092', 'locality_id' => 84, 'placement' => 'ліцей нафтопереробної промисловості, їдальня' ],
            [ 'name' => '531093', 'locality_id' => 84, 'placement' => 'ліцей нафтопереробної промисловості, корпус 2, хол' ],
            [ 'name' => '531094', 'locality_id' => 84, 'placement' => 'школа "Вибір" №17, корпус 1, хол' ],
            [ 'name' => '531095', 'locality_id' => 84, 'placement' => 'школа "Вибір" №17, корпус 2, хол' ],
            [ 'name' => '531096', 'locality_id' => 84, 'placement' => 'дитяча бібліотека, кімната казок' ],
            [ 'name' => '531097', 'locality_id' => 84, 'placement' => 'школа №8, хол' ],
            [ 'name' => '531098', 'locality_id' => 84, 'placement' => 'школа №26, спортивна зала' ],
        ];

        foreach ($areas as $item) {
            Psc::create([
                'name' => $item['name'],
                'locality_id' => $item['locality_id'],
                'placement' => $item['placement'],
                'is_hidden' => isset($item['is_hidden']) ? $item['is_hidden'] : 0
            ]);
        }
    }
}



























































































































































































































































































































































































































































































































































