<?php

use Illuminate\Database\Seeder;
use App\Models\Street;

class StreetsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

       	$street = Street::create([ 'name' => 'Жовтнева площа', 'type' => Street::TYPE_SQUARE, 'locality_id' => 84 ]);
       	Street::create([ 'name' => 'Незалежності', 'type' => Street::TYPE_SQUARE, 'locality_id' => 84, 'prev_id' =>  $street->id]);
    }
}
