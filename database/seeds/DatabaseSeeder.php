<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersDataSeeder::class);
        $this->call(LocalitiesDataSeeder::class);
        $this->call(PscsDataSeeder::class);
        $this->call(PartiesDataSeeder::class);
        $this->call(AnswerGroupsDataSeeder::class);
        $this->call(InquiresDataSeeder::class);
        // $this->call(AddressesDataSeeder::class);
        // $this->call(VotersDataSeeder::class);
    }
}
