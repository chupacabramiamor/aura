<?php

use Illuminate\Database\Seeder;
use App\Models\Address;
use App\Models\Locality;

class AddressesDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $locality = Locality::whereName('Кременчук')->first();

        factory(Address::class, 30)->create([ 'locality_id' => $locality->id ]);
        // factory(Address::class, 20)->create();
    }
}
