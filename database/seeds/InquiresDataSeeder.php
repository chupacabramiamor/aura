<?php

use Illuminate\Database\Seeder;
use App\Models\Inquire;
use App\Models\AnswerGroup;

class InquiresDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        Inquire::create([
            'group_id' => $this->fetchGroupId('Участь у виборах'),
            'questionnaire_id' => 1,
            'question' => "Чи підете Ви голосувати на вибори Презідента України, або парламентські вибори, якщо вони відбулися б у найближчу неділю?"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Кандидати Президент'),
            'questionnaire_id' => 1,
            'question' => "За кого з кандидатів у Президенти України Ви би проголосували?"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Кандидати Президент'),
            'questionnaire_id' => 1,
            'question' => "Якщо кандидат, за якого Ви готові проголосувати, НЕ буде балотуватися, за якого іншого кандидата Ви б проголосували?"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Кандидати Президент'),
            'questionnaire_id' => 1,
            'question' => "За кого з кандидатів в Президенти України Ви б НЕ проголосували ні при яких обставинах?"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Партії'),
            'questionnaire_id' => 1,
            'question' => "Якби позачергові парламентські вибори відбувалися наступної неділі, за яку політичну партію Ви б проголосували?"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Партії'),
            'questionnaire_id' => 1,
            'question' => "За яку політичну партію Ви б НЕ проголосували ні при яких обставинах?"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Кандидати на окрузі'),
            'questionnaire_id' => 1,
            'question' => "За кого з кандидатів у ВР на вашому окрузі Ви б проголосували, якби позачергові парламентські вибори відбувалися наступної неділі?"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Проблеми міста'),
            'questionnaire_id' => 1,
            'question' => "Назвіть, будь ласка, найбільш актуальні проблеми Вашого міста/села/селища/?"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Місцеві діячі'),
            'questionnaire_id' => 1,
            'question' => "Хто у Вашому місті/селі/селищі позитивно впливає на економічну, політичну ситуацію? (або негативно)"
        ]);

        Inquire::create([
            'group_id' => $this->fetchGroupId('Місцеві діячі'),
            'questionnaire_id' => 1,
            'question' => "До кого з місцевих діячів Ви ставитеся негативно?"
        ]);
    }


    private function fetchGroupId($keyword)
    {
        return AnswerGroup::where('name', 'like', "%{$keyword}%")->first()->id;
    }
}
