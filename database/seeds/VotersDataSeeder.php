<?php

use Illuminate\Database\Seeder;
use App\Models\Voter;

class VotersDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        factory(Voter::class, 100)->create();
    }
}
