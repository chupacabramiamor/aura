<?php

namespace App\Services;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Storage;
use stdClass;
use App\Models\Address;

/**
 *  VotersExport
 */
class VotersExport
{

    private $outputFormat = 'Xlsx';
    private $fields;

    public function __construct()
    {
        $this->fields = [
            'index_number'     => new TableColumn(1, 'Системний ID'),
            'gender'           => new TableColumn(2, 'Стать', DataType::TYPE_STRING),
            'psc_number'       => new TableColumn(3, '№ дільниці'),
            'street_type'      => new TableColumn(4, 'Тип вулиці', DataType::TYPE_STRING),
            'street_name'      => new TableColumn(5, 'Назва вулиці', DataType::TYPE_STRING),
            'house_number'     => new TableColumn(6, 'Будинок', DataType::TYPE_STRING),
            'building'         => new TableColumn(7, 'Корпус', DataType::TYPE_STRING),
            'apartment_number' => new TableColumn(8, 'Квартира (секція, кімната)', DataType::TYPE_STRING),
            'fullname'         => new TableColumn(9, 'П.І.Б.', DataType::TYPE_STRING),
            'phone_number'     => new TableColumn(10, 'Контактний телефон', DataType::TYPE_STRING),
            // 'voting_date'      => new TableColumn(11, 'Дата опитування'),
        ];
    }

    public function run($votersData, $questionnaireData = null) : string
    {
        $filename = str_random(12) . '.' . strtolower($this->outputFormat);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $writer = IOFactory::createWriter($spreadsheet, $this->outputFormat);

        $sheet->getStyle('A:AZ')->getFont()->setName('Calibri')->setSize(10);

        // Making a heading
        // $highestRow = $sheet->getRowIterator()->current();

        $sheet->getRowDimension(1)->setRowHeight(60);

        $sheet->getStyle('A1:AZ1')->getAlignment()->setHorizontal('center')->setVertical('center')->setWrapText(true);

        if ($questionnaireData) {
            foreach ($questionnaireData['inquires'] as $index => $item) {
                $column = new TableColumn(count($this->fields) + $index + 1, $item['question'], DataType::TYPE_STRING);

                $coordinate = $column->getPosition() . '1';

                $style = $sheet->getStyle($coordinate);

                $style->getBorders()->getBottom()->setBorderStyle(Border::BORDER_MEDIUM);
                $style->getFont()->setBold(true);

                $sheet->getCell($coordinate)->setValue($column->getTitle());
            }
        }

        // Generate the general data cells
        foreach ($votersData as $index => $row) {
            foreach ($this->fields as $key => $column) {
                $coordinate = $column->getPosition() . (string) ($index + 2);

                $methodName = $this->getHandleMethodName($key);

                $sheet->setCellValueExplicit($coordinate, $this->$methodName($row), $column->getFormat());
            }

            if ($questionnaireData) {
                $answers = array_values($questionnaireData['data']->where('voter_id', $row->id)->toArray());

                if (count($answers)) {
                    foreach($questionnaireData['inquires'] as $key => $inquire) {
                        $letter = TableColumn::convert(count($this->fields) + $key + 1);
                        $coordinate = $letter . (string) ($index + 2);

                        $answer = array_first($answers, function($item) use($inquire) {
                            return $item['inquire_id'] == $inquire->id;
                        });

                        if (in_array($inquire->id, array_pluck($answers, 'inquire_id'))) {
                            $sheet->setCellValueExplicit($coordinate, $answer['answer']['value'], DataType::TYPE_STRING);
                            $sheet->getColumnDimension($letter)->setWidth(40);
                        }
                    }
                }
            }
        }

        foreach ($this->fields as $column) {
            $coordinate = $column->getPosition() . '1';

            $style = $sheet->getStyle($coordinate);

            $style->getBorders()->getBottom()->setBorderStyle(Border::BORDER_MEDIUM);
            $style->getFont()->setBold(true);

            $sheet->getCell($coordinate)->setValue($column->getTitle());

            $sheet->getColumnDimension($column->getPosition())->setAutoSize(true);
        }

        $writer->save(storage_path('app/public/' . $filename));

        return asset(Storage::url($filename));
    }

    private function getHandleMethodName($alias)
    {
        return preg_replace_callback('~_([a-z])~', function($c) {
            return strtoupper($c[1]);
        }, 'handle_' . $alias);
    }

    private function handleIndexNumber(stdClass $rowData)
    {
        return $rowData->id;
    }

    private function handleGender(stdClass $rowData)
    {
        switch ($rowData->gender_type) {
            case 1:
                $result = 'Чоловіча';
                break;

            case 2:
                $result = 'Жіноча';
                break;

            default:
                $result = 'Невідомо';
                break;
        }

        return $result;
    }

    private function handlePscNumber(stdClass $rowData)
    {
        return $rowData->psc_name;
    }

    private function handleStreetType(stdClass $rowData)
    {
        return array_first(Address::getStreetTypesData(), function($item) use($rowData) {
            return $item['id'] == $rowData->street_type;
        })['value'] ?: 'Невідомо';
    }

    private function handleStreetName(stdClass $rowData)
    {
        return $rowData->street_name;
    }

    private function handleHouseNumber(stdClass $rowData)
    {
        return $rowData->house_number;
    }

    private function handleBuilding(stdClass $rowData)
    {
        return $rowData->building;
    }

    private function handleApartmentNumber(stdClass $rowData)
    {
        return $rowData->apartment_number;
    }

    private function handleFullname(stdClass $rowData)
    {
        return sprintf('%s %s', $rowData->surname, $rowData->firstname);
    }

    private function handlePhoneNumber(stdClass $rowData)
    {
        return $rowData->phone_number ? '+' . $rowData->phone_number : null;
    }
}

class TableColumn {

    private $position;
    private $title;
    private $format;

    public function __construct(int $position, string $title, string $format = DataType::TYPE_NUMERIC)
    {
        $this->position = $position;
        $this->title = $title;
        $this->format = $format;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPosition(bool $returnInt = false)
    {
        return $returnInt ? $this->position : static::convert($this->position);
    }

    public function getFormat()
    {
        return $this->format;
    }

    public static function convert(int $position)
    {
        $position = intval($position);

        if ($position <= 0) return '';

        $letter = '';

        while($position != 0) {
           $p = ($position - 1) % 26;
           $position = intval(($position - $p) / 26);
           $letter = chr(65 + $p) . $letter;
        }

        return $letter;
    }
}