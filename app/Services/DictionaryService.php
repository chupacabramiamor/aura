<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Dictionary;

class DictionaryService
{

    /**
     * Adding a value in dictionary Registry
     * @param  string  $value Value
     * @param  int     $scope Scope of Dictionary
     * @return Dictionary
     */
    public function add($value, int $scope = Dictionary::SCOPE_GENERAL) : Dictionary
    {
        return Dictionary::create([
            'value' => $value,
            'scope' => $scope
        ]);
    }

    public function make($value, int $scope = Dictionary::SCOPE_GENERAL) : Dictionary
    {
        $dictionary = Dictionary::whereValue($value)->whereScope($scope)->first();

        if ($dictionary) {
            return $dictionary;
        }

        return $this->add($value, $scope);
    }

    /**
     * Obtaining the list of all dictionary items in a specified scope
     * @param  int     $scope Scope of Dictionary
     * @return Collection
     */
    public function fetch(int $scope = Dictionary::SCOPE_GENERAL) : Collection
    {
        return Dictionary::whereScope($scope)->get();
    }

    /**
     * Handler that would be executed if necessary updating of the dicitonary
     * @param  string  $value Searching Value
     * @param  int     $scope Scope of Dictionary
     * @return void
     */

    public function onUpdatingHandler($value, $scope = Dictionary::SCOPE_GENERAL)
    {
        if (!$value) {
            return;
        }

        if ($this->isKeywordExisted($value, $scope)) {
            return;
        }

        $this->add($value, (int) $scope);
    }

    /**
     * Verify for existed value in a dictionary registry
     * @param  string  $value Searching Value
     * @param  int     $scope Scope of Dictionary
     * @return boolean
     */
    public function isKeywordExisted($value, $scope = Dictionary::SCOPE_GENERAL) : bool
    {
        $dictionary = Dictionary::whereValue($value)->whereScope($scope)->first();

        return (bool) $dictionary;
    }
}