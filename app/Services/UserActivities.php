<?php
namespace App\Services;

use App\Models;
use Auth;

/**
 *  UserActivities
 */
class UserActivities
{
    private $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function __call($method, $args)
    {
        if (strpos($method, 'addFor') !== false) {
            return $this->add('App\Models\\' . substr($method, 6), $args[0], $args[1], @$args[2]);
        }

        parent::__call();
    }

    private function add(string $modelName, int $loggable_id, $body, array $payload = [])
    {
        return Models\UserActivity::create([
            'user_id' => $this->user->id,
            'loggable_id' => $loggable_id,
            'loggable_type' => $modelName,
            'body' => $body,
            'payload' => $payload
        ]);
    }
}