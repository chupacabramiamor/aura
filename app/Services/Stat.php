<?php
namespace App\Services;

use App\Models;
use DB;

/**
 *  Stat
 */
class Stat
{

    public function votersCount($useCache = true)
    {
        $result = DB::select(DB::raw('SELECT COUNT(*) as count FROM `voters`'));
        return $result[0]->count;
    }


    public function addressesCount($useCache = true)
    {
        $result = DB::select(DB::raw('SELECT COUNT(*) as count FROM `addresses`'));
        return $result[0]->count;
    }


    public function localitiesCount($useCache = true)
    {
        $result = DB::select(DB::raw('SELECT COUNT(*) as count FROM `localities`'));
        return $result[0]->count;
    }
}