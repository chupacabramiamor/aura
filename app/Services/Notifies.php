<?php
namespace App\Services;

use App\Models;
use App\Mail\NewAddrRequest;
use Illuminate\Support\Collection;
use Auth;
use Mail;

/**
 *  Notifies
 */
class Notifies
{
    private $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }


    public function fetch()
    {
        return Models\Notify::where(function($q) {
            $q->where('broadcast_for', $this->user->role_id)->where('receiver_id', 0);
        })->orWhere(function($q) {
            $q->where('receiver_id', $this->user->id);
        })->orderBy('id', 'desc')->get();
    }


    public function send(\Illuminate\Database\Eloquent\Model $receiver, $message, Array $payload = [], $type = Models\Notify::TYPE_GENERAL)
    {
        if (!in_array(get_class($receiver), [ Models\User::class, Models\Role::class ])) {
            throw new \Exception("error_processing_notify");
        }

        $titleMapping = [
            Models\Notify::TYPE_GENERAL => 'Нове повідомлення',
            Models\Notify::TYPE_ALLOW_RESPONSE => 'Погоджено',
            Models\Notify::TYPE_REJECT_RESPONSE => 'Відмовлено',
            Models\Notify::TYPE_NEW_ADDR_REQUEST => 'Нова адреса',
        ];

        $notify = new Models\Notify();

        $notify->type = $type;
        $notify->title = $titleMapping[$type];
        $notify->message = $message;
        $notify->initiator_id = $this->user->id;
        $notify->payload = $payload;

        $notify->save();

        if ($receiver instanceOf Models\User) {
            $notify->receiver_id = $receiver->id;
            $notify->save();

            if ($receiver->email) {
                Mail::to($receiver->email)->send(new NewAddrRequest($notify));
            }
        }

        if ($receiver instanceOf Models\Role) {
            $notify->broadcast_for = $receiver->id;
            $notify->save();

            Mail::to($receiver->users()->whereNotNull('email')->pluck('email'))->send(new NewAddrRequest($notify));
        }


        return $notify->fresh();
    }


    public function setAsOpened(Models\Notify $notify)
    {
        if ($notify->broadcast_for == 0) {
            if ($notify->receiver_id != $this->user->id) {
                throw new \Exception("error_processing_notify");
            }
        } else {
            $notify->receiver_id = $this->user->id;
        }

        $notify->is_opened = true;
        $notify->save();

        return $notify->fresh();
    }


    public function remove(Models\Notify $notify)
    {
        if ($notify->broadcast_for == 0) {
            if ($notify->receiver_id != $this->user->id) {
                throw new \Exception("error_processing_notify");
            }
        } else {
            if ($notify->broadcast_for != $this->user->role_id) {
                throw new \Exception("error_processing_notify");
            }
        }

        $notify->delete();
    }
}