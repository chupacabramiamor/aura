<?php
namespace App\Services\Importers;

use Andruleez\DataImport\Processor as ImportProcessor;
use Andruleez\DataImport\ImporterInterface;
use Andruleez\DataImport\Exceptions;
use App\Models;
use Carbon\Carbon;
use DB;

/**
 *  KremenArea
 */
class KremenArea extends Initial implements ImporterInterface
{

    protected $area_id = 38;
    protected $headerMapping = [
        'index' => 'Індекс',
        'locality' => 'Нас.Пункт',
        'street_name' => 'Вул',
        'house_number' => 'Буд',
        'apartment_number' => 'кв',
        'psc' => 'ДВК',
        'surname' => 'Прізвище',
        'firstname' => "Ім'я",
        'patronymic' => 'По-батькові',
        'birthday' => 'ДН',
        'description' => 'МН',
    ];
}