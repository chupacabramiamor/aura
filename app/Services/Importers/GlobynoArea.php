<?php
namespace App\Services\Importers;

use Andruleez\DataImport\Processor as ImportProcessor;
use Andruleez\DataImport\ImporterInterface;
use Andruleez\DataImport\Exceptions;
use App\Models;
use Carbon\Carbon;
use DB;

/**
 *  GlobynoArea
 */
class GlobynoArea extends Initial implements ImporterInterface
{

    protected $area_id = 30;
    protected $headerMapping = [
        'index' => 'Індекс',
        'locality' => 'Нас.Пункт',
        'street_name' => 'Вул',
        'psc' => 'ДВК',
        'surname' => 'Прізвище',
        'firstname' => "Імя",
        'patronymic' => 'По-батькові',
        'birthday' => 'ДН',
        'description' => 'МН',
    ];


    protected function handleStreetName($cell, Array &$output)
    {
        $value = trim($cell->getValue());

        if (preg_match('~(бульв\.|вул\.|пров\.|проїзд|квартал|просп\.|тупик|набережна|узвіз|майдан|заїзд)\.{0,1}\s*(.+),\s?(.+),\s?кв\.(\w+)~u', $value, $matches)) {
            $output['address']['street_type'] = $this->guessStreetType($matches[1])['id'];
            $output['address']['street_name'] = $matches[2];
            $output['address']['house_number'] = $matches[3];
            $output['address']['apartment_number'] = $matches[4];
            return;
        }

        if (preg_match('~(бульв\.|вул\.|пров\.|проїзд|квартал|просп\.|тупик|набережна|узвіз|майдан|заїзд)\.{0,1}\s*(.+),\s?(.+)~u', $value, $matches)) {
            $output['address']['street_type'] = $this->guessStreetType($matches[1])['id'];
            $output['address']['street_name'] = $matches[2];
            $output['address']['house_number'] = $matches[3];
            return;
        }

        $output['address']['street_type'] = Models\Address::STREET_TYPE_UNDEFINED;
        $output['address']['street_name'] = $value;
        $output['address']['house_number'] = 0;
    }
}