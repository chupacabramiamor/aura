<?php
namespace App\Services\Importers;

use Andruleez\DataImport\Processor as ImportProcessor;
use Andruleez\DataImport\Exceptions;
use App\Models;
use DB;
use App;
use Auth;
use Carbon\Carbon;

class Initial extends ImportProcessor {

    protected $area_id;
    protected $rowSkeleton = [
        'address' => []
    ];

    public static function commit($filename)
    {
        $log = App::make(\Monolog\Logger::class);
        $file = storage_path($filename);

        if (!file_exists($file)) {
            throw new \Exception('file_does_not_exist');
        }

        $data = unserialize(file_get_contents($file));
        $stat = [
            'ignored' => 0,
            'added' => 0
        ];

        $log->addDebug('import_started', [
            'filename' => $filename,
            'user_id' => Auth::user()->id
        ]);

        foreach ($data as $item) {
            // Обработка адреса
            $query = "SELECT * FROM `addresses`WHERE `locality_id` = :locality_id  AND `street_type` = :street_type AND `street_name` = :street_name AND `house_number` = :house_number AND `apartment_number` = :apartment_number AND `building` = :building LIMIT 1";

            $address = DB::select($query, [
                'locality_id' => (Int) $item['address']['locality_id'],
                'street_type' => (Int) $item['address']['street_type'],
                'street_name' => (String) $item['address']['street_name'],
                'house_number' => (String) $item['address']['house_number'],
                'apartment_number' => isset($item['address']['apartment_number']) ? $item['address']['apartment_number'] : 'NULL',
                'building' => isset($item['address']['building']) ? $item['address']['building'] : 'NULL',
            ]);

            if (!count($address)) {
                $address_id = DB::table('addresses')->insertGetId([
                    'locality_id' => $item['address']['locality_id'],
                    'psc_id' => $item['address']['psc_id'],
                    'postcode' => isset($item['address']['postcode']) ? $item['address']['postcode'] : 'NULL',
                    'street_type' => $item['address']['street_type'],
                    'street_name' => $item['address']['street_name'],
                    'house_number' => $item['address']['house_number'],
                    'apartment_number' => isset($item['address']['apartment_number']) ? $item['address']['apartment_number'] : NULL,
                    'building' => isset($item['address']['building']) ? $item['address']['building'] : NULL,
                ]);
            } else $address_id = $address[0]->id;

            // Проверка на дубликат телефонного номера
            if (isset($item['phone_number'])) {
                $voter = DB::table('voters')->where('phone_number', $item['phone_number'])->first();

                if ($voter) {
                    $log->addDebug('voter_ignored', $voter);
                    $stat['ignored']++;
                    continue;
                }
            }

            DB::table('voters')->insert([
                'address_id' => $address_id,
                'surname' => isset($item['surname']) ? $item['surname'] : '',
                'firstname' => isset($item['firstname']) ? $item['firstname'] : '',
                'patronymic' => isset($item['patronymic']) ? $item['patronymic'] : '',
                'email' => isset($item['email']) ? $item['email'] : '',
                'phone_number' => isset($item['phone_number']) ? $item['phone_number'] : '',
                'additional_phone_number' => isset($item['additional_phone_number']) ? $item['additional_phone_number'] : '',
                'birthday' => isset($item['birthday']) ? $item['birthday'] : '',
                'education_type' => isset($item['education_type']) ? $item['education_type'] : 0,
                'gender_type' => isset($item['gender_type']) ? $item['gender_type'] : 0,
                'occupation_type' => isset($item['occupation_type']) ? $item['occupation_type'] : 0,
                'party_id' => isset($item['party_id']) ? $item['party_id'] : 0,
                'birth_locality' => isset($item['birth_locality']) ? $item['birth_locality'] : '',
                'description' => isset($item['description']) ? $item['description'] : '',
            ]);

            $stat['added']++;
        }

        $log->addDebug('import_completed', [
            'filename' => $filename,
            'user_id' => Auth::user()->id,
            'stat' => $stat
        ]);

        return $stat;
    }


    protected function handleIndex($cell, Array &$output)
    {
        $matches = $this->processCell($cell->getValue(), '^(\d+).?');

        $output['address']['postcode'] = $matches[1];
    }


    protected function handleLocality($cell, Array &$output)
    {
        $value = trim(str_replace('’', "'", $cell->getValue()));
        $matches = $this->processCell($value, "([а-яіїє]+\.?)\s?([А-ЯІЇЄа-яіїє\-\s\']+)$");

        $locality = $this->guessLocality($matches[2]);

        if (!$locality) {
            throw new Exceptions\DocumentStructureException("invalid_content:$value:" . count($output));
        }

        $output['address']['locality_id'] = $locality->id;
    }


    protected function handleStreetName($cell, Array &$output)
    {
        try {
            $matches = $this->processCell(trim($cell->getValue()), '(бульв\.|вул\.|пров\.|проїзд|квартал|просп\.|тупик|набережна|узвіз|майдан|заїзд)\.{0,1}\s*(.+)');
            $streetType = array_first(Models\Address::getStreetTypesData(), function($item) use($matches) {
                return $item['short'] == $matches[1];
            });
            $streetName = $matches[2];
        } catch (\Exception $e) {
            $streetType = Models\Address::getStreetType(1);
            $streetName = trim($cell->getValue());
        }

        $output['address']['street_type'] = $streetType['id'];
        $output['address']['street_name'] = $streetName;
    }


    protected function handleHouseNumber($cell, Array &$output)
    {
        $output['address']['house_number'] = trim($cell->getValue());
    }


    protected function handleApartmentNumber($cell, Array &$output)
    {
        $value = trim($cell->getValue());
        $result = null;


        if ($value) {
            if (preg_match('~(\d+)\-?([A-Za-zА-ЯІЇЄа-яіїє])?$u~', $value, $matches)) {
                $result = isset($matches[2]) ? sprintf('%s%s', $matches[1], $matches[2]) : $matches[1];
            } elseif (preg_match('~кв\.(\d+)~', $value, $matches)) {
                $output['description'] = "Сумісне або переплановане житло:" . $value;
                $result = $matches[1];
            }
        }

        $output['address']['apartment_number'] = $result;
    }


    protected function handleBuilding($cell, Array &$output)
    {
        $output['address']['building'] = trim($cell->getValue());
    }


    protected function handlePsc($cell, Array &$output)
    {
        $value = $cell->getValue();

        $psc = array_first($this->pscList, function($item) use($value) {
            return $item->name == trim($value);
        });

        if (!$psc) {
            $localityCell = $cell->getWorksheet()->getCell('B'. $cell->getRow());
            $matches = $this->processCell(trim(str_replace('’', "'", $localityCell->getValue())), "((смт\s)?)([А-Яа-яіїє\-\s\']+)$");

            $locality = $this->guessLocality($matches[3]);

            if (!$locality) {
                throw new Exceptions\DocumentStructureException("invalid_content:$value:" . count($output));
            }

            $psc_id = DB::table('pscs')->insertGetId([
                'name' => $value,
                'locality_id' => $locality->id
            ]);

            $this->pscList = DB::select('SELECT * FROM `pscs`');

            $this->log->addDebug('new_psc_added', [ $value, $psc_id ]);
        } else $psc_id = $psc->id;

        $output['address']['psc_id'] = $psc_id;
    }


    protected function handleSurname($cell, Array &$output)
    {
        $output['surname'] = $cell->getValue();
    }


    protected function handleFirstname($cell, Array &$output)
    {
        $output['firstname'] = $cell->getValue();
    }


    protected function handlePatronymic($cell, Array &$output)
    {
        $output['patronymic'] = $cell->getValue();
    }


    protected function handleBirthday($cell, Array &$output)
    {
        $cell->getStyle()->getNumberFormat()->setFormatCode('m/d/yyyy');
        $value = $cell->getFormattedValue();

        $output['birthday'] = Carbon::createFromFormat('m/d/Y', $value)->format('Y-m-d');
    }


    protected function handleDescription($cell, Array &$output)
    {
        $output['birth_locality'] = $cell->getValue();
    }


    protected function guessLocality($value)
    {
        return array_first($this->localityList, function($item) use ($value) {
            return $item->name == $value && $item->submission_id == $this->area_id;
        });
    }


    protected function guessStreetType($value)
    {
        $streetType = array_first(Models\Address::getStreetTypesData(), function($item) use($value) {
            return $item['short'] == $value;
        });

        return $streetType ?: Models\Address::getStreetType(1);
    }
}