<?php
namespace App\Services\Importers;

use Andruleez\DataImport\Processor as ImportProcessor;
use Andruleez\DataImport\ImporterInterface;
use Andruleez\DataImport\Exceptions;
use App\Models;
use Carbon\Carbon;
use DB;

/**
 *  HorishniPlavni
 */
class HorishniPlavni extends Initial implements ImporterInterface
{

    protected $area_id = 38;
    protected $headerMapping = [
        'index' => 'Індекс',
        'locality' => 'Нас.Пункт',
        'street_name' => 'Вул',
        'house_number' => 'Буд',
        'apartment_number' => 'кв',
        'psc' => 'ДВК',
        'surname' => 'Прізвище',
        'firstname' => "Ім'я",
        'patronymic' => 'По-батькові',
        'birthday' => 'ДН',
        'description' => 'МН',
    ];


    public function handleLocality($cell, Array &$output)
    {
        $output['address']['locality_id'] = $this->guessLocality('Горішні Плавні')->id;
    }
}