<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use DB;
use App\Http\Resources\Addresses as AddressesResource;

class AddressesController extends Controller
{

    public function getIndex()
    {
        return view('templates.master.addresses.index');
    }


    public function getAdd(Request $request)
    {
        if ($request->has('notify')) {
            $notify = Models\Notify::where('id', $request->input('notify'))->first();
        }

        return view('templates.master.addresses.add', [
            'defaults' => isset($notify) ? $notify->payload : [],
            'notify_id' => isset($notify) ? $notify->id : false,
            'pscList' => Models\Psc::with('locality')->get()->toArray(),
            'localityList' => Models\Locality::localitiesOnly()->get()->toArray(),
        ]);
    }


    public function getEdit($address)
    {
        return view('templates.master.addresses.edit', [
            'address' => $address,
            'pscList' => Models\Psc::with('locality')->get()->toArray(),
            'localityList' => Models\Locality::localitiesOnly()->get()->toArray(),
        ]);
    }


    public function postAdd(Request $request)
    {
        $this->validate($request, [
            'psc_id' => [ 'required', 'integer', 'min:1' ],
            'locality_id' => [ 'required', 'integer', 'min:1' ],
            'postcode' => [ 'integer', 'between:10000,99999' ],
            'street_id' => [ 'required', 'integer', 'exists:streets,id' ],
            'house_number' => [ 'required', 'max:10' ],
            'apartment_number' => [ 'min:1' ],
        ]);

        if ($request->has('notify_id')) {
            $notify = Models\Notify::where('id', $request->input('notify_id'))->first();

            if ($notify) {
                $payload = [ 'notify_id' => $notify_id ];
                app('Notifies')->send($notify->initer, 'Нова адреса додана адміністратором', $payload, Models\Notify::TYPE_ALLOW_RESPONSE);

                app('Notifies')->remove($notify);
            }
        }

        $builder = Models\Address::where('locality_id', $request->input('locality_id'))
            ->where('psc_id', $request->input('psc_id'))
            ->where('street_id', $request->input('street_id'))
            ->where('house_number', $request->input('house_number'));

        if ($request->input('postcode')) {
            $builder = $builder->where('postcode', $request->input('postcode'));
        }

        if ($request->input('apartment_number')) {
            $builder = $builder->where('apartment_number', $request->input('apartment_number'));
        }

        if ($request->input('building')) {
            $builder = $builder->where('building', $request->input('building'));
        }

        if ($builder->first()) {
            $this->badRequest('address_already_exsited');
        }

        $address = new Models\Address();

        $address->locality_id = $request->input('locality_id');
        $address->street_id = $request->input('street_id');
        $address->psc_id = $request->input('psc_id');
        $address->postcode = $request->input('postcode');
        $address->house_number = $request->input('house_number');
        $address->apartment_number = $request->input('apartment_number');
        $address->building = $request->input('building');

        $address->save();

        return [
            'url' => route('address_list')
        ];
    }


    public function postEdit(Request $request, $address)
    {
        $this->validate($request, [
            'psc_id' => [ 'required' ],
            'locality_id' => [ 'required', 'integer', 'min:1' ],
            // 'postcode' => [ 'integer', 'between:10000,99999' ],
            'street_id' => [ 'required', 'integer', 'exists:streets,id' ],
            'house_number' => [ 'required', 'max:10' ],
            'apartment_number' => [ 'min:1' ],
        ]);

        $address->locality_id = $request->input('locality_id');
        $address->psc_id = $request->input('psc_id');
        $address->postcode = $request->input('postcode');
        $address->street_id = $request->input('street_id');
        $address->house_number = $request->input('house_number');
        $address->apartment_number = $request->input('apartment_number');
        $address->building = $request->input('building');

        $address->save();

        return [
            'url' => route('address_list')
        ];
    }


    public function getLocalityList()
    {
        return Models\Locality::localitiesOnly()->orderBy('name')->get();
    }


    public function getLocalityData($id)
    {
        $locality = Models\Locality::with([ 'area', 'area.region' ])->find($id);

        if (!$locality) {
            $this->notFound('wrong_locality');
        }

        return $locality;
    }


    public function postSearch(Request $request)
    {
        $this->validate($request, [
            'locality_id' => [ 'required', 'integer', 'min:1' ],
            'street_name' => [ 'required', 'min:3' ],
            'house_number' => [ 'min:1' ],
            'building' => [ 'min:1' ],
            'apartment_number' => [ 'min:1' ]
        ]);

        $query = DB::table('addresses')
            ->selectRaw('`addresses`.`*`, `pscs`.`name`')
            ->where('addresses.locality_id', $request->input('locality_id'))
            ->where('addresses.street_name', 'like', sprintf('%%%s%%', $request->input('street_name')))
            ->leftJoin('pscs', 'pscs.id', '=', 'addresses.psc_id');

        if ($request->has('house_number')) {
            $query = $query->where('house_number', 'like', sprintf('%s%%', $request->input('house_number')));
        }

        if ($request->has('building')) {
            $query = $query->where('building', 'like', sprintf('%s%%', $request->input('building')));
        }

        if ($request->has('apartment_number')) {
            $query = $query->where('apartment_number', $request->input('apartment_number'));
        }

        return new AddressesResource($query->get());
    }


    public function getReject($notify_id)
    {
        // Устанавлниваем входящее уведомление как использованное
        try {
            $notify = app('Notifies')->remove($notify_id);
        } catch (\Exception $e) {
            $this->badRequest('notify_process_failed');
        }

        // Добавляем уведомление для оператора
        $payload = [ 'notify_id' => $notify->id ];
        app('Notifies')->send($notify->initer, 'У додаванні нової адреси відмовлено', $payload, Models\Notify::TYPE_REJECT_RESPONSE);

        return [
            'url' => route('address_list')
        ];
    }


    public function postSendCreatingRequest(Request $request)
    {
        $this->validate($request, [
            'locality_id' => [ 'required', 'integer', 'min:1' ],
            'psc_id' => [ 'required', 'integer', 'min:1' ],
            // 'postcode' => [ 'required' ],
            'street_id' => [ 'required', 'integer', 'min:1' ],
            'house_number' => [ 'required' ],
            // 'apartment_number' => [ 'required' ],
            // 'building' => [ 'required' ],
        ]);

        $notify = app('Notifies')->send(
            Models\Role::find(Models\User::ROLE_ADMINISTRATOR),
            $this->addressFormat($request->only([ 'locality_id', 'psc_id', 'postcode', 'street_id', 'house_number', 'apartment_number', 'building' ])),
            $request->only([ 'locality_id', 'psc_id', 'postcode', 'street_id', 'house_number', 'apartment_number', 'building' ]),
            Models\Notify::TYPE_NEW_ADDR_REQUEST
        );

        return $notify;
    }


    private function addressFormat(Array $fields)
    {
        $locality = Models\Locality::find($fields['locality_id']);

        if (!$locality) {
            throw new Exception("locality_not_found");
        }

        $street = Models\Street::find($fields['street_id']);

        if (!$street) {
            throw new Exception("street_not_found");
        }

        $locality_type = array_first(Models\Locality::getLocalityTypeData(), function($item) use($locality) {
            return $item['id'] == $locality->type;
        });

        $street_type = array_first(Models\Street::getStreetTypesData(), function($item) use($street) {
            return $item['id'] == $street->type;
        });

        $result = '';

        if (isset($fields['postcode'])) {
            $result .= $fields['postcode'] . ', ';
        }

        $result .= sprintf('%s %s, %s %s, б. %s', $locality_type['name'], $locality->name, $street_type['short'], $street->name, $fields['house_number']);

        if (isset($fields['building'])) {
            $result .= ", корпус {$fields['building']}";
        }

        if (isset($fields['apartment_number'])) {
            $result .= ", кв. {$fields['apartment_number']}";
        }

        return $result;
    }
}
