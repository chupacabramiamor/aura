<?php

namespace App\Http\Controllers\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inquire;

class Inquiries extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Inquire::query()->with('group');

        if ($request->input('questionnaire_id', 0) > 0) {
            $query = $query->where('questionnaire_id', $request->input('questionnaire_id'));
        }

        return $query->orderBy('id')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => [ 'required' ],
            'group_id' => [ 'required', 'integer', 'min:1' ],
            'questionnaire_id' => [ 'required', 'integer', 'min:1' ]
        ]);

        $inquire = new Inquire();

        $inquire->question = $request->input('question');
        $inquire->group_id = $request->input('group_id');
        $inquire->questionnaire_id = $request->input('questionnaire_id');

        $inquire->save();

        return $inquire;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inquiry = Inquire::find($id)->load('group');

        if (!$inquiry) {
            $this->notFound('inquiry_not_found');
        }

        return $inquiry;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inquiry = Inquire::find($id)->load('group');

        if (!$inquiry) {
            $this->notFound('inquiry_not_found');
        }

        $this->validate($request, [
            'question' => [ 'required' ],
        ]);

        $inquiry->question = $request->input('question');
        $inquiry->save();

        return $inquiry;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inquiry = Inquire::find($id)->load('group');

        if (!$inquiry) {
            $this->notFound('inquiry_not_found');
        }

        $inquiry->delete();

        return $this->success();
    }
}
