<?php

namespace App\Http\Controllers\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class Operators extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::where('role_id', User::ROLE_OPERATOR)
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'login' => [ 'required' ],
            'email' => [ 'required', 'email' ],
            'password' => [ 'required', 'min:6', 'confirmed'],
            'password_confirmation' => [ 'required' ],
        ]);

        $user = new User();

        $user->login = $request->input('login');
        $user->password = $request->input('password');
        $user->fullname = $request->input('fullname');
        $user->is_enabled = $request->input('is_enabled', 0);
        $user->role_id = User::ROLE_OPERATOR;
        $user->email = $request->input('email');

        $user->save();

        if (is_array($request->input('pscs', []))) {
            $user->pscs()->sync($request->input('pscs'));
        }

        return [
            'id' => $user->id
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('role_id', User::ROLE_OPERATOR)
            ->where('id', $id)
            ->with('pscs')
            ->first();

        if (!$user) {
            $this->notFound('user_does_not_exist');
        }

        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'login' => [ 'required' ],
            'email' => [ 'required', 'email' ],
            'password' => [ 'min:6', 'confirmed'],
        ]);

        $user = User::where('role_id', User::ROLE_OPERATOR)
            ->where('id', $id)
            ->first();

        if (!$user) {
            $this->notFound('user_does_not_exist');
        }

        $user->login = $request->input('login');
        $user->fullname = $request->input('fullname');
        $user->is_enabled = $request->input('is_enabled');
        $user->email = $request->input('email');

        if ($request->input('password')) {
            $user->password = $request->input('password');
        }

        $user->save();

        if (is_array($request->input('pscs', []))) {
            $user->pscs()->sync($request->input('pscs'));
        }

        return $this->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('role_id', User::ROLE_OPERATOR)
            ->where('id', $id)
            ->first();

        if (!$user) {
            $this->notFound('user_does_not_exist');
        }

        if ($user->pscs->count()) {
            $this->badRequest('has_active_pscs');
        }

        User::destroy($id);

        return $this->success('manager_removed');
    }
}
