<?php

namespace App\Http\Controllers\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Questionnaire;

class Questionnaires extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [ 'required' ]
        ]);

        $questionnaire = new Questionnaire();

        $questionnaire->name = $request->input('name');
        $questionnaire->save();

        return $questionnaire->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionnaire = Questionnaire::where('id', $id)->with('inquiries.group')->first();

        if (!$questionnaire) {
            $this->notFound('questionnaire_does_not_exist');
        }

        return $questionnaire;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $questionnaire = Questionnaire::where('id', $id)->with('inquiries.group')->first();

        if (!$questionnaire) {
            $this->notFound('questionnaire_does_not_exist');
        }

        $this->validate($request, [
            'name' => [ 'required' ]
        ]);

        $questionnaire->name = $request->input('name');
        $questionnaire->save();

        return $this->success('questionnaire_updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = Questionnaire::where('id', $id)->first();

        if (!$questionnaire) {
            $this->notFound('questionnaire_does_not_exist');
        }

        if (Questionnaire::count() == 1) {
            $this->badRequest('unable_to_remove_last_item');
        }

        if ($questionnaire->inquiries->count()) {
            $this->badRequest('unable_to_remove_filled_item');
        }

        $questionnaire->delete();

        return $this->success('removed');
    }
}
