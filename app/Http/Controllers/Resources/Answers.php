<?php

namespace App\Http\Controllers\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Answer;

class Answers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Answer::query();

        if ($request->input('group_id', 0) > 0) {
            $query = $query->where('group_id', $request->input('group_id'));
        }

        return $query->orderBy('value')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'group_id' => [ 'required', 'integer', 'min:1' ],
            'value' => [ 'required' ]
        ]);

        $answer = new Answer();

        $answer->group_id = $request->input('group_id');
        $answer->value = $request->input('value');

        $answer->save();

        return $answer;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = Answer::find($id);

        if (!$answer) {
            $this->notFound('answer_not_found');
        }

        $this->validate($request, [
            'value' => [ 'required' ]
        ]);

        $answer->value = $request->input('value');
        $answer->save();

        return $answer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answer = Answer::find($id);

        if (!$answer) {
            $this->notFound('answer_not_found');
        }

        $answer->delete();

        return $this->success();
    }
}
