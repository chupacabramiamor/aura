<?php

namespace App\Http\Controllers\Resources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AnswerGroup;

class AnswerGroups extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AnswerGroup::orderBy('name')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [ 'required' ],
            'type_id' => [ 'required', 'integer', 'min:0' ]
        ]);

        $answerGroup = new AnswerGroup();

        $answerGroup->name = $request->input('name');
        $answerGroup->type_id = $request->input('type_id');

        $answerGroup->save();

        return $answerGroup;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $answerGroup = AnswerGroup::find($id);

        if (!$answerGroup) {
            $this->notFound('answer_does_not_exist');
        }

        return $answerGroup;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => [ 'required' ],
        ]);

        $answerGroup = AnswerGroup::find($id);

        if (!$answerGroup) {
            $this->notFound('answer_does_not_exist');
        }

        $answerGroup->name = $request->input('name');
        $answerGroup->save();

        return $this->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answerGroup = AnswerGroup::find($id);

        if (!$answerGroup) {
            $this->notFound('answer_does_not_exist');
        }

        if ($answerGroup->answers->count()) {
            $this->badRequest('unable_to_remove_filled_item');
        }

        $answerGroup->delete();

        return $this->success();
    }


    public function getTypeList()
    {
        $mapping = AnswerGroup::getTypesMapping();
        $result = [];

        foreach ($mapping as $key => $value) {
            $result[] = [
                'id' => $key,
                'name' => $value
            ];
        }

        return $result;
    }
}
