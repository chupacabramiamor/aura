<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Street;
use Gate;

class StreetsController extends Controller
{

    public function getIndex(Request $request)
    {
        $builder = Street::query();

        if ($request->has('locality_id')) {
            $builder = $builder->where('locality_id', $request->input('locality_id'));
        }

        return $builder->get();
    }

    public function postIndex(Request $request)
    {
        if (Gate::denies('street.add')) {
            $this->forbidden('adding_a_streets_is_not_allowed');
        }

        $this->validate($request, [
            'locality_id' => [ 'required', 'exists:localities,id' ],
            'name'        => [ 'required' ],
            'type'        => [ 'required', 'street_in_type' ],
            'prev_id'     => [ 'street_is_outdated' ]
        ]);

        $street = new Street();

        $street->locality_id = $request->input('locality_id');
        $street->name = $request->input('name');
        $street->type = $request->input('type');

        if ($request->has('prev_id')) {
            $street->prev_id = $request->input('prev_id');
        }

        try {
            $street->save();
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() === '23000') {
                $this->badRequest('street_is_now_existed');
            }

            $this->badRequest('unable_to_create_specified_street');
        }

        return $street;
    }

    public function patchUpdate(Request $request, $street)
    {
        if (Gate::denies('street.add')) {
            $this->forbidden('adding_a_streets_is_not_allowed');
        }

        if ($street->is_outated) {
            $this->badRequest('street_is_outdated');
        }

        $this->validate($request, [
            'locality_id' => [ 'exists:localities,id' ],
            'name' => [],
            'type' => [ 'street_in_type' ],
            'prev_id' => [ 'street_is_outdated' ]
        ]);

        $street->fill($request->all());

        $street->save();

        return $street;
    }

    public function getShow(Street $street)
    {
        return $street;
    }

}
