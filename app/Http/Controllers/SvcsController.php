<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DictionaryService;
use App\Models;

class SvcsController extends Controller
{

	public function getPscs(Request $request)
	{
		if ($request->user()->role_id == Models\User::ROLE_ADMINISTRATOR) {
			$pscs = Models\Psc::all();
		} else {
			$pscs = $request->user()->pscs;
		}

		return $pscs->load([ 'locality', 'locality.area', 'locality.area.region' ]);
	}


	public function getEducationList(Request $request)
	{
		return Models\Voter::getEducationTypesData();
	}


	public function getOccupacyList(Request $request)
	{
		return Models\Voter::getOccupacyTypesData();
	}


	public function getPartyList(Request $request)
	{
		return Models\Party::all();
	}


	public function getLocalityList(Request $request)
	{
		return Models\Locality::localitiesOnly()->orderBy('name')->get();
	}


	public function getAddressList(Request $request)
	{
		$builder = Models\Address::query();

		if ($request->input('locality_id') > 0) {
			$builder = $builder->where('locality_id', $request->input('locality_id'));
		}

		if ($request->input('psc_id') > 0) {
			$builder = $builder->where('psc_id', $request->input('psc_id'));
		}

		return $builder->get();
	}


	public function getStreetTypeList(Request $request)
	{
		return Models\Address::getStreetTypesData();
	}


	public function getQuestionnaires(Request $request)
	{
		return Models\Questionnaire::all();
	}


	public function getAnswerList(Request $request)
	{
		$query = Models\Answer::query();

		if ($request->has('group_id')) {
			$query = $query->where('group_id', $request->input('group_id'));
		}

		return $query->get();
	}


	public function getCampaignPositionsList(Request $request)
	{
		return Models\Voter::getCampaignPositionsData();
	}


	public function getRolesList()
	{
		return Models\Role::all();
	}


	public function getStreetList(Request $request)
	{
		$this->validate($request, [
			'locality_id' => [ 'exists:localities,id' ]
		]);

		$builder = Models\Street::query();

		if ($request->has('locality_id')) {
			$builder = $builder->where('locality_id', $request->input('locality_id'));
		}

		return $builder
			->where('is_outated', false)
			->get();
	}

	public function getDictonary(Request $request, DictionaryService $dictonary)
	{
		return $dictonary->fetch($request->input('scope', Models\Dictionary::SCOPE_GENERAL));
	}
}
