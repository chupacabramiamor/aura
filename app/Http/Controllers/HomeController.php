<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function getIndex()
    {
        return view('templates.master.home.index', [
            'voters_count' => app('stat')->votersCount(),
            'addresses_count' => app('stat')->addressesCount(),
            'localities_count' => app('stat')->localitiesCount()
        ]);
    }
}
