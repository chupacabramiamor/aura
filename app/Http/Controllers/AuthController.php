<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Monolog\Logger;

class AuthController extends Controller
{

    public function getLogin()
    {
    	return view('templates.signin.login');
    }


    public function postLogin(Request $request, Logger $log)
    {
    	$this->validate($request, [
    		'login' => [ 'required' ],
    		'password' => [ 'required' ],
    	]);

    	$credentials = array_merge($request->only('login', 'password'), [
            'is_enabled' => 1
        ]);

    	if(!Auth::attempt($credentials)) {
            $log->addError('auth:wrong_credentials', $request->all());
    		return $this->badRequest('wrong_credentials');
    	}

        $log->addInfo('auth:success', $request->only('login'));

    	return [
    		'url' => route('home')
    	];
    }


    public function getLogout()
    {
        Auth::logout();

        return redirect(route('login'));
    }
}
