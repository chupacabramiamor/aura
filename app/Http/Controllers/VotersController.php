<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Monolog\Logger;
use App\Models;
use DB;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Requests\SearchVotersRequest;
use App\Http\Requests\ExportVotersRequest;
use App\Services\VotersExport;
use App\Services\DictionaryService;

class VotersController extends Controller
{

    public function getIndex()
    {
        return view('templates.master.voters.index');
    }


    public function gatAdd()
    {
        return view('templates.master.voters.add');
    }


    public function postAdd(Request $request, Logger $log, DictionaryService $dictionarySvc)
    {
        $this->validate($request, [
            'gender_type' => [ 'required', 'in:1,2' ],
            'surname' => [ 'required' ],
            'firstname' => [ 'required' ],
            'patronymic' => [ '' ],
            // 'phone_number' => [ 'required' ],
            // 'additional_phone_number' => [ 'required' ],
            'email' => [ 'email' ],
            'education_type' => [ 'integer', 'min:1' ],
            'occupation_type' => [ 'integer', 'min:1' ],
            'party_id' => [ 'integer', 'min:1' ],
            'address_id' => [ 'required', 'integer', 'min:1' ],
            'is_newspaper_subscribed' => [ 'boolean' ],
            'birthday' => [ 'date' ]
        ]);

        $log->addInfo('trying_to_adding_new_voter', $request->all(), $request->user()->id);

        // Проверка на одинаковые телефонные номера
        $phoneNumber = Models\Voter::wherePhoneNumber(Models\Voter::msisdn($request->input('phone_number')))->first();
        // $additionalPhoneNumber = Models\Voter::whereAdditionalPhoneNumber(Models\Voter::msisdn($request->input('additional_phone_number')))->first();

        if ($phoneNumber) {
            $log->addError('phone_number_already_exist', [
                'phone_number' => $request->input('phone_number'),
                'user_id' => $request->user()->id
            ]);
            $this->badRequest('phone_number_already_exist');
        }

        $voter = new Models\Voter();

        $voter->fill($request->only([
            'gender_type',
            'surname',
            'firstname',
            'patronymic',
            'phone_number',
            'additional_phone_number',
            'email',
            'education_type',
            'occupation_type',
            'party_id',
            'address_id',
        ]));

        if ($request->has('workplace_keyword')) {
            $voter->workplace_id = $dictionarySvc->make($request->input('workplace_keyword'), Models\Dictionary::SCOPE_WORKPLACE)->id;
        }

        if ($request->has('workposition_keyword')) {
            $voter->workposition_id = $dictionarySvc->make($request->input('workposition_keyword'), Models\Dictionary::SCOPE_WORKPOSITION)->id;
        }

        if ($request->has('birthday')) {
            $voter->birthday = $request->input('birthday');
        }

        $voter->save();
        $voter = $voter->fresh('address');

        $voter->address->is_newspaper_subscribed = $request->input('is_newspaper_subscribed', 0);
        $voter->address->save();

        app('UserActivities')->addForVoter($voter->id, 'Виборець був успішно доданий через інтерфейс', $voter->toArray());

        return [
            'url' => route('voters')
        ];
    }


    public function postSearch(SearchVotersRequest $request)
    {
        if (!count($request->all())) {
            $this->badRequest('empty_parameters_not_allowed');
        }

        $builder = $this->makeSearchBuilder($request, 20);

        $items = $builder
            ->get()
            ->map(function($item) {
                return [
                    'additional_phone_number' => $item->additional_phone_number,
                    'address_id' => $item->address_id,
                    'answer_id' => $item->answer_id,
                    'apartment_number' => $item->apartment_number,
                    'birth_locality' => $item->birth_locality,
                    'birthday' => $item->birthday,
                    'building' => $item->building,
                    'created_at' => $item->created_at,
                    'description' => $item->description,
                    'education_type' => $item->education_type,
                    'email' => $item->email,
                    'firstname' => $item->firstname,
                    'gender_type' => $item->gender_type,
                    'house_number' => $item->house_number,
                    'id' => $item->id,
                    'inquire_id' => $item->inquire_id,
                    'locality_id' => $item->locality_id,
                    'city_name' => $item->name,
                    'occupation_type' => $item->occupation_type,
                    'party_id' => $item->party_id,
                    'patronymic' => $item->patronymic,
                    'phone_number' => $item->phone_number,
                    'postcode' => $item->postcode,
                    'psc_id' => $item->psc_id,
                    'psc_name' => $item->psc_name,
                    'street_name' => $item->street_name,
                    'street_type' => $item->street_type,
                    'submission_id' => $item->submission_id,
                    'surname' => $item->surname,
                    'city_type' => $item->type,
                    'area_type' => $item->area_type,
                    'area_name' => $item->area_name,
                    'region_type' => $item->region_type,
                    'region_name' => $item->region_name,
                ];
            }
        );

        $totalAmount = DB::select(DB::raw("SELECT FOUND_ROWS() AS count"));

        return new LengthAwarePaginator($items, $totalAmount[0]->count, 20, $request->input('page'));
    }


    public function getAnswers($questionnaire_id, $voter_id)
    {
        $questionnaire = Models\Questionnaire::find($questionnaire_id);

        if (!$questionnaire) {
            $this->notFound('questionnaire_does_not_exist');
        }

        $voter_answers = Models\VoterAnswer::with('voter')->where('voter_id', $voter_id)->get();

        $questionnaire->load('inquiries.group');

        return $questionnaire->inquiries->map(function(Models\Inquire $inquire) use($voter_answers) {
            $va = $voter_answers->firstWhere('inquire_id', $inquire->id);

            if ($va) {
                $va->load('answer');
            }

            return [
                'id' => $inquire->id,
                'question' => $inquire->question,
                'answer_id' => $va ? $va->answer->id : null,
                'answer_value' => $va ? $va->answer->value : null,
                'group_id' => $inquire->group_id,
                'group_name' => $inquire->group->name,
                'group_type' => $inquire->group->type_id
            ];
        });
    }


    public function postAnswer(Request $request, Logger $log, $inquire_id, $voter_id)
    {
        $inquire = Models\Inquire::find($inquire_id);

        if (!$inquire) {
            $this->notFound();
        }

        $this->validate($request, [
            'value' => 'required'
        ]);

        $inquire->load('group.answers');

        $answer = $inquire->group->answers->first(function($item) use($request) {
            return $item->value == $request->input('value');
        });

        if (!$answer) {
            if ($inquire->group->type_id == Models\AnswerGroup::TYPE_QUESTION) {
                $answer = new Models\Answer([
                    'value' => $request->input('value'),
                    'group_id' => $inquire->group->id
                ]);
                $answer->save();
            } else {
                $log->addError('incorrect_input_value', [
                    'value' => $request->input('value'),
                    'inquire_id' => $inquire_id,
                    'voter_id' => $voter_id,
                ]);

                $this->badRequest('incorrect_input_value');
            }
        }

        $va = Models\VoterAnswer::where('inquire_id', $inquire_id)->where('voter_id', $voter_id)->first();

        if (!$va) {
            $va = new Models\VoterAnswer([
                'answer_id' => $answer->id,
                'voter_id' => $voter_id,
                'inquire_id' => $inquire_id,
            ]);
        } else {
            $va->answer_id = $answer->id;
        }

        $va->save();

        $log->addInfo('answer_saved', [
            'answer_id' => $answer->id,
            'voter_id' => $voter_id,
            'inquire_id' => $inquire_id,
        ]);

        return $answer;
    }


    public function getEdit($id)
    {
        $voter = Models\Voter::find($id);

        if (!$voter) {
            $this->notFound('item_does_not_exist');
        }

        if (!Auth::user()->pscs->contains('id', $voter->address->psc_id) && Auth::user()->role_id >= Models\User::ROLE_OPERATOR) {
            $this->forbidden('access_to_voter_denied');
        }

        if (Auth::user()->role_id == Models\User::ROLE_ADMINISTRATOR) {
            $voter->load('activities.user');
        }

        return view('templates.master.voters.edit', [
            'voterData' => $voter
        ]);
    }


    public function postEdit(Request $request, DictionaryService $dictionarySvc, $id)
    {
        $voter = Models\Voter::find($id);

        if (!$voter) {
            $this->notFound('item_does_not_exist');
        }

        if (!$request->user()->pscs->contains('id', $voter->address->psc_id) && Auth::user()->role_id >= Models\User::ROLE_OPERATOR) {
            $this->forbidden('access_to_voter_denied');
        }

        $this->validate($request, [
            'gender_type' => [ 'required', 'in:1,2,0' ],
            'surname' => [ 'required' ],
            'firstname' => [ 'required' ],
            'patronymic' => [ '' ],
            // 'phone_number' => [ 'required' ],
            // 'email' => [ 'email' ],
            'education_type' => [ 'integer', 'min:0' ],
            'occupation_type' => [ 'integer', 'min:0' ],
            'party_id' => [ 'integer', 'min:0' ],
            'address_id' => [ 'required', 'integer', 'min:1' ],
            'is_newspaper_subscribed' => [ 'boolean' ],
            'birthday' => [ 'date' ]
        ]);

        // Проверка на одинаковые телефонные номера
        $phoneNumber = Models\Voter::wherePhoneNumber(Models\Voter::msisdn($request->input('phone_number')))->where('phone_number', '<>', $voter->phone_number)->first();

        if ($phoneNumber) {
            $this->badRequest('phone_number_already_exist');
        }

        $voter->load('address');

        $voter->gender_type = $request->input('gender_type');
        $voter->surname = $request->input('surname');
        $voter->firstname = $request->input('firstname');
        $voter->patronymic = $request->input('patronymic');
        $voter->phone_number = $request->input('phone_number');
        $voter->additional_phone_number = $request->input('additional_phone_number');
        $voter->email = $request->input('email');
        $voter->education_type = $request->input('education_type');
        $voter->occupation_type = $request->input('occupation_type');
        $voter->party_id = $request->input('party_id');
        $voter->address_id = $request->input('address_id');
        $voter->campaign_position = $request->input('campaign_position');
        $voter->birthday = $request->input('birthday');
        $voter->address->is_newspaper_subscribed = $request->input('is_newspaper_subscribed', 0);

        if ($request->has('workplace_keyword')) {
            $voter->workplace_id = $dictionarySvc->make($request->input('workplace_keyword'), Models\Dictionary::SCOPE_WORKPLACE)->id;
        }

        if ($request->has('workposition_keyword')) {
            $voter->workposition_id = $dictionarySvc->make($request->input('workposition_keyword'), Models\Dictionary::SCOPE_WORKPOSITION)->id;
        }

        $voter->push();

        $voter = $voter->fresh(['address']);

        app('UserActivities')->addForVoter($voter->id, 'Дані виборця були успішно збережені через інтерфейс', $voter->toArray());

        return [
            'url' => route('voters')
        ];
    }

    public function getRemove($id)
    {
        $voter = Models\Voter::find($id);

        if (!$voter) {
            $this->notFound('item_does_not_exist');
        }

        if (Auth::user()->role_id != Models\User::ROLE_ADMINISTRATOR ) {
            $this->forbidden('access_denied');
        }

        $voter->delete();

        return [
            'url' => route('voters')
        ];
    }


    public function postExport(ExportVotersRequest $request)
    {
        $votersData = $this->makeSearchBuilder($request)->get();
        $questionnaireData = null;

        if ($request->has('questionnaire_id')) {
            $inquires = Models\Inquire::where('questionnaire_id', $request->input('questionnaire_id'))->get();
            $questionnaireData = [
                'inquires' => $inquires,
                'data' => Models\VoterAnswer::with('answer')
                    ->whereIn('voter_id', array_pluck($votersData, 'id'))
                    ->whereIn('inquire_id', $inquires->pluck('id'))
                    ->get()
            ];
        }

        return [
            'download' => (new VotersExport())->run($votersData, $questionnaireData)
        ];
    }

    private function makeSearchBuilder(Request $request, int $limit = null)
    {
        $builder = DB::table('voters')
            ->whereNull('deleted_at')
            ->selectRaw('
                SQL_CALC_FOUND_ROWS
                `voter_answers`.`*`,
                `addresses`.`*`,
                `localities`.`type`, `localities`.`submission_id`, `localities`.`name`,
                `areas`.`name` area_name, `areas`.`type` area_type,
                `regions`.`name` as region_name, `regions`.`type` as region_type,
                `voters`.`*`,
                `pscs`.`name` AS `psc_name`
                ')
            ->leftJoin('voter_answers', 'voter_answers.voter_id', '=', 'voters.id')
            ->leftJoin('addresses', 'addresses.id', '=', 'voters.address_id')
            ->leftJoin('pscs', 'pscs.id', '=', 'addresses.psc_id')
            ->leftJoin('localities', 'localities.id', '=', 'addresses.locality_id')
            ->leftJoin('localities AS areas', 'areas.id', '=', 'localities.submission_id')
            ->leftJoin('localities AS regions', 'regions.id', '=', 'areas.submission_id');

        if ($limit) {
            $builder = $builder
                ->limit($limit)
                ->offset($request->input('page', 1) * $request->input('limit', $limit) - $limit);
        }

        if (!$request->user()->isAdministration()) {
            $builder = $builder
                ->leftJoin('psc_user AS pu', 'pu.psc_id', '=', 'pscs.id')
                ->where('pu.user_id', $request->user()->id);
        }

        if ($request->has('psc_id')) {
            $builder = $builder->where('addresses.psc_id', $request->input('psc_id'));
        }

        if ($request->has('keyword')) {
            $builder = $builder->where(function($q) use($request) {
                $q->where('surname', 'LIKE', sprintf('%s%%', $request->input('keyword')));
                $q->orWhere('firstname', 'LIKE', sprintf('%s%%', $request->input('keyword')));
                $q->orWhere('patronymic', 'LIKE', sprintf('%s%%', $request->input('keyword')));
                $q->orWhere('phone_number', 'LIKE', sprintf('%%%s%%', $request->input('keyword')));
                $q->orWhere('additional_phone_number', 'LIKE', sprintf('%%%s%%', $request->input('keyword')));
            });
        }

        if ($request->has('locality_name')) {
            $builder = $builder->where('localities.name', 'LIKE', sprintf('%s%%', $request->input('locality_name')));
        }

        if ($request->has('street_name')) {
            $builder = $builder->where('street_name', 'LIKE', sprintf('%s%%', $request->input('street_name')));
        }

        if ($request->has('house_number')) {
            $builder = $builder->where('house_number', $request->input('house_number'));
        }

        if ($request->has('apartment_number')) {
            $builder = $builder->where('apartment_number', $request->input('apartment_number'));
        }

        if ($request->has('building')) {
            $builder = $builder->where('building', $request->input('building'));
        }

        if ($request->input('is_newspaper_subscribed')) {
            $builder = $builder->where('is_newspaper_subscribed', 1);
        }

        if ($request->has('filters')) {
            if ($request->has('filters.opinions')) {
                $builder = $builder->where(function($q) use($request) {
                    foreach ($request->input('filters.opinions') as $item) {
                        $q->orWhere(function($q) use($item) {
                            $q->where('inquire_id', $item['option_id']);
                            $q->whereIn('answer_id', $item['values']);
                        });
                    }
                });
            }
        }

        return $builder;
    }
}
