<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notify;
use Auth;

class NotificationsController extends Controller
{

    public function getData()
    {
        $user = Auth::user();

        return app('Notifies')->fetch();
    }


    public function getRowData($id)
    {
        $user = Auth::user();

        $notify = Notify::where('id', $id)
            ->with('initer')
            ->first();

        if (!$notify) {
            $this->notFound('notify_does_not_exist');
        }

        return app('Notifies')->setAsOpened($notify);
    }


    public function getMarkAsOpened($id)
    {
        $notify = Notify::where('id', $id)
            ->with('initer')
            ->first();

        if (!$notify) {
            $this->notFound('notify_does_not_exist');
        }

        return app('Notifies')->setAsOpened($notify);
    }
}
