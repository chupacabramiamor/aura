<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Questionnaire;

class QuestionnaireController extends Controller
{

    public function getIndex()
    {
    	$questionnaireList = Questionnaire::all()->toArray();

    	return view('templates.master.questionnaire.index', [
    		'questionnaireList' => $questionnaireList
    	]);
    }


    public function getAnswerGroups()
    {
    	return view('templates.master.questionnaire.answer-groups');
    }
}
