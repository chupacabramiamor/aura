<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Psc;

class OperatorsController extends Controller
{

    public function getIndex()
    {
        return view('templates.master.operators.index', [
            'operatorList' => User::where('role_id', User::ROLE_OPERATOR)->orderBy('id', 'desc')->get(),
            'pscList' => Psc::all(),
        ]);
    }
}
