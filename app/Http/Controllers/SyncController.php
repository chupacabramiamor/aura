<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;
use Monolog\Logger;
use Andruleez\DataImporter\Processor as ImportProcessor;
use App\Services\Importers;

class SyncController extends Controller
{
    const MOD_KREM = 1;
    const MOD_HP = 2;
    const MOD_KREMAREA = 3;
    const MOD_GLOBAREA = 4;

    public function getIndex()
    {
        return view('templates.master.sync.index');
    }


    public function postIndex(Request $request, Logger $log)
    {
        $this->validate($request, [
            'mod' => [ 'required' ]
        ]);

        if (!$request->file('file')) {
            $this->badRequest('no_file_occurred');
        }

        switch ($request->input('mod')) {
            case static::MOD_KREM:
                $processor = new Importers\Kremen($request->file('file'));
                break;

            case static::MOD_KREMAREA:
                $processor = new Importers\KremenArea($request->file('file'));
                break;

            case static::MOD_GLOBAREA:
                $processor = new Importers\GlobynoArea($request->file('file'));
                break;

            case static::MOD_HP:
                $processor = new Importers\HorishniPlavni($request->file('file'));
                break;

            default:
                $this->badRequest('invalid_mod');
                break;
        }

        $log->addInfo('Extracting data from file', []);

        try {
            $processor->prepare();
        } catch (\Exception $e) {
            $this->badRequest($e->getMessage());
        }

        return $processor->getStat();
    }


    public function postUploadingConfirm(Request $request)
    {
        $this->validate($request, [
            'filename' => [ 'required' ],
        ]);

        $stat = Importers\Initial::commit($request->input('filename'));

        return [
            'status' => $stat
        ];
    }
}
