<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Addresses extends ResourceCollection
{
    public static $wrap = null;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function($item) {
            return [
                "id" => $item->id,
                "locality_id" => $item->locality_id,
                "psc_id" => $item->psc_id,
                "postcode" => $item->postcode,
                "street_type" => $item->street_type,
                "street_name" => $item->street_name,
                "house_number" => $item->house_number,
                "apartment_number" => $item->apartment_number,
                "building" => $item->building,
                "psc_number" => $item->name,
            ];
        });
    }
}
