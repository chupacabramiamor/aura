<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExportVotersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'psc_id' => [ 'integer', 'min:1' ],
            'keyword' => [ 'min:3' ],
            'filters.opinions.*.option_id' => [ 'integer', 'min:1' ],         // question_id
            'filters.opinions.*.values.*' => [ 'min:1' ],                     // answer_ids
            'questionnaire_id' => [ 'integer', 'exists:questionnaires,id' ]
        ];
    }
}
