<?php
namespace App\Validators;

use App\Models\Street;
use DB;

/**
 *  Streets validation class
 */
class Streets
{
    public function streetInType($attribute, $value)
    {
        return in_array($value, Street::TYPES);
    }


    public function streetIsOutdated($attribute, $value)
    {
        $result = DB::table('streets')
            ->where('is_outated', 0)
            ->where('id', $value)
            ->first();

        return (bool) $result;
    }
}