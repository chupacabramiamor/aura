<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('street.add', function ($user) {
            return in_array($user->role_id, [
                Models\User::ROLE_SA,
                Models\User::ROLE_ADMINISTRATOR
            ]);
        });

        Gate::define('street.update', function ($user) {
            return in_array($user->role_id, [
                Models\User::ROLE_SA,
                Models\User::ROLE_ADMINISTRATOR
            ]);
        });
    }
}
