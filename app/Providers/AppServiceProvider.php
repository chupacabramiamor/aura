<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\Services as AppServices;
use App\Models;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Models\Address::observe(Models\Observers\Addresses::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Notifies', function($app) {
            return new AppServices\Notifies();
        });

        $this->app->bind(Logger::class, function($app) {
            $logger = new Logger('app');
            $logger->pushHandler(new StreamHandler(base_path() . '/storage/logs/app_'. date('Y-m-d') .'.log', Logger::DEBUG));

            return $logger;
        });

        $this->app->singleton('UserActivities', function($app) {
            return new AppServices\UserActivities();
        });

        $this->app->singleton('stat', function($app) {
            return new AppServices\Stat();
        });

        $this->app->singleton(DictionaryService::class, function() {
            return new AppServices\DictionaryService();
        });
    }
}
