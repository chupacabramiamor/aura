<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'name' ];
    protected $appends = [ 'inquires_amount' ];

    public function inquiries()
    {
    	return $this->hasMany(Inquire::class);
    }


    public function getInquiresAmountAttribute()
    {
    	return $this->inquiries->count();
    }
}
