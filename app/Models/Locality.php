<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    const TYPE_SPECIAL_STATUS = 1;         // Міста що мають спеціальний статус
    const TYPE_REGION = 2;          // Область
    const TYPE_AREA = 3;            // Район области
    const TYPE_CITY = 4;
    const TYPE_URBAN_VILLAGE = 5;
    const TYPE_VILLAGE = 6;
    const TYPE_LARGE_VILLAGE = 7;

    public $timestamps = false;

    public function area()
    {
        return $this->belongsTo(Locality::class, 'submission_id')->where('type', static::TYPE_AREA);
    }


    public function region()
    {
        return $this->belongsTo(Locality::class, 'submission_id')->where('type', static::TYPE_REGION);
    }


    public function scopeLocalitiesOnly($query)
    {
        return $query->whereIn('type', [ static::TYPE_VILLAGE, static::TYPE_CITY, static::TYPE_LARGE_VILLAGE, static::TYPE_URBAN_VILLAGE ]);
    }


    public static function getLocalityTypeData()
    {
        return [
            [ 'id' => static::TYPE_SPECIAL_STATUS, 'name' => 'м.', 'description' => 'Міста що мають спеціальний статус' ],
            [ 'id' => static::TYPE_REGION, 'name' => 'обл.', 'description' => 'Область' ],
            [ 'id' => static::TYPE_AREA, 'name' => 'р-н', 'description' => 'Район області' ],
            [ 'id' => static::TYPE_CITY, 'name' => 'м.', 'description' => 'Місто' ],
            [ 'id' => static::TYPE_URBAN_VILLAGE, 'name' => 'смт.', 'description' => 'Селище міського типу' ],
            [ 'id' => static::TYPE_VILLAGE, 'name' => 'с.', 'description' => 'Село' ],
            [ 'id' => static::TYPE_LARGE_VILLAGE, 'name' => 'селище', 'description' => 'Селище' ],
        ];
    }
}
