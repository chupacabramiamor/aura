<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    const STREET_TYPE_UNDEFINED = 1;   // Не визначено
	const STREET_TYPE_STREET = 2;	// Вулиця
	const STREET_TYPE_SQUARE = 3;	// майдан
	const STREET_TYPE_LANE = 4;		// пров
	const STREET_TYPE_AVENUE = 5;	// просп
	const STREET_TYPE_BOULEVARD = 6; // бульв.
	const STREET_TYPE_DESCENT = 7;	// узвіз
	const STREET_TYPE_HOSTEL = 8;	// гуртожиток
	const STREET_TYPE_PASSAGE = 9;	// проїзд
	const STREET_TYPE_PL = 10; 		// пл. ?????
	const STREET_TYPE_DEADEND = 11;	// тупик
	const STREET_TYPE_QR = 12;		// квартал
	const STREET_TYPE_QUAY = 13;	// набережна
	const STREET_TYPE_LAP = 14;		// заїзд

    public $timestamps = false;

    protected $hidden = [ 'compiled' ];

    public function locality()
    {
    	return $this->belongsTo(Locality::class);
    }


    public function psc()
    {
    	return $this->belongsTo(Psc::class);
    }


    public static function getStreetTypes()
    {
        $list = (new \ReflectionClass(static::class))->getConstants();
        $result = [];

        foreach($list as $key => $value) {
            if (strpos($key, 'STREET_TYPE_') !== false) {
                $result[$key] = $value;
            }
        }

        return $result;
    }


    public static function getStreetTypesData()
    {
        return [
            [ 'id' => static::STREET_TYPE_UNDEFINED, 'value' => 'Не визначено', 'short' => '' ],
            [ 'id' => static::STREET_TYPE_STREET, 'value' => 'Вулиця', 'short' => 'вул.' ],
            [ 'id' => static::STREET_TYPE_SQUARE, 'value' => 'Майдан', 'short' => 'майдан' ],
            [ 'id' => static::STREET_TYPE_LANE, 'value' => 'Провулок', 'short' => 'пров.' ],
            [ 'id' => static::STREET_TYPE_AVENUE, 'value' => 'Проспект', 'short' => 'просп.' ],
            [ 'id' => static::STREET_TYPE_BOULEVARD, 'value' => 'Бульвар', 'short' => 'бульв.' ],
            [ 'id' => static::STREET_TYPE_DESCENT, 'value' => 'Узвіз', 'short' => 'узвіз' ],
            [ 'id' => static::STREET_TYPE_HOSTEL, 'value' => 'Гуртожиток', 'short' => 'гуртожиток' ],
            [ 'id' => static::STREET_TYPE_PASSAGE, 'value' => 'Проїзд', 'short' => 'проїзд' ],
            [ 'id' => static::STREET_TYPE_PL, 'value' => 'пл. ????', 'short' => '??' ],
            [ 'id' => static::STREET_TYPE_DEADEND, 'value' => 'Тупик', 'short' => 'тупик' ],
            [ 'id' => static::STREET_TYPE_QR, 'value' => 'Квартал', 'short' => 'квартал' ],
            [ 'id' => static::STREET_TYPE_QUAY, 'value' => 'Набережна', 'short' => 'набережна' ],
            [ 'id' => static::STREET_TYPE_LAP, 'value' => 'Заїзд', 'short' => 'заїзд' ],
        ];
    }


    public static function getStreetType($id)
    {
        return array_first(static::getStreetTypesData(), function($item) use($id) {
            return $id == $item['id'];
        });
    }
}
