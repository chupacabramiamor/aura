<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    const TYPE_UNDEFINED = 1;    // Не визначено
    const TYPE_STREET = 2;       // Вулиця
    const TYPE_SQUARE = 3;       // майдан
    const TYPE_LANE = 4;         // пров
    const TYPE_AVENUE = 5;       // просп
    const TYPE_BOULEVARD = 6;    // бульв.
    const TYPE_DESCENT = 7;      // узвіз
    const TYPE_HOSTEL = 8;       // гуртожиток
    const TYPE_PASSAGE = 9;      // проїзд
    const TYPE_PL = 10;          // пл. ?????
    const TYPE_DEADEND = 11;     // тупик
    const TYPE_QR = 12;          // квартал
    const TYPE_QUAY = 13;        // набережна
    const TYPE_LAP = 14;         // заїзд

    const TYPES = [
        self::TYPE_UNDEFINED,
        self::TYPE_STREET,
        self::TYPE_SQUARE,
        self::TYPE_LANE,
        self::TYPE_AVENUE,
        self::TYPE_BOULEVARD,
        self::TYPE_DESCENT,
        self::TYPE_HOSTEL,
        self::TYPE_PASSAGE,
        self::TYPE_PL,
        self::TYPE_DEADEND,
        self::TYPE_QR,
        self::TYPE_QUAY,
        self::TYPE_LAP,
    ];

    public $timestamps = false;

    protected $fillable = [ 'name', 'type', 'locality_id', 'prev_id' ];

    public function prev()
    {
        return $this->belongsTo(Street::class, 'prev_id');
    }


    public function locality()
    {
        return $this->belongsTo(Locality::class);
    }


    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public static function getStreetTypesData()
    {
        return [
            [ 'id' => static::TYPE_UNDEFINED, 'value' => 'Не визначено', 'short' => '' ],
            [ 'id' => static::TYPE_STREET, 'value' => 'Вулиця', 'short' => 'вул.' ],
            [ 'id' => static::TYPE_SQUARE, 'value' => 'Майдан', 'short' => 'майдан' ],
            [ 'id' => static::TYPE_LANE, 'value' => 'Провулок', 'short' => 'пров.' ],
            [ 'id' => static::TYPE_AVENUE, 'value' => 'Проспект', 'short' => 'просп.' ],
            [ 'id' => static::TYPE_BOULEVARD, 'value' => 'Бульвар', 'short' => 'бульв.' ],
            [ 'id' => static::TYPE_DESCENT, 'value' => 'Узвіз', 'short' => 'узвіз' ],
            [ 'id' => static::TYPE_HOSTEL, 'value' => 'Гуртожиток', 'short' => 'гуртожиток' ],
            [ 'id' => static::TYPE_PASSAGE, 'value' => 'Проїзд', 'short' => 'проїзд' ],
            [ 'id' => static::TYPE_PL, 'value' => 'пл. ????', 'short' => '??' ],
            [ 'id' => static::TYPE_DEADEND, 'value' => 'Тупик', 'short' => 'тупик' ],
            [ 'id' => static::TYPE_QR, 'value' => 'Квартал', 'short' => 'квартал' ],
            [ 'id' => static::TYPE_QUAY, 'value' => 'Набережна', 'short' => 'набережна' ],
            [ 'id' => static::TYPE_LAP, 'value' => 'Заїзд', 'short' => 'заїзд' ],
        ];
    }
}
