<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
	/* Public properties */
    public $timestamps = false;


    /* Protected properties */
    protected $dates = [ 'created_at' ];

    protected $guarded = [ 'id' ];

    protected $casts = [
    	'payload' => 'array'
    ];

    protected $attributes = [
    	'payload' => '{}'
    ];


/* Relation Methods */

	public function user()
	{
		return $this->belongsTo(User::class);
	}


    public function loggable()
    {
    	return $this->morphTo();
    }
}
