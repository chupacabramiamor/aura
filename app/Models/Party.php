<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    public $timestamps = false;

    public function voters()
    {
    	return $this->hasMany(Voter::class);
    }
}
