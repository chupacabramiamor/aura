<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notify extends Model
{
    use SoftDeletes;

    const TYPE_GENERAL = 1;
    const TYPE_ALLOW_RESPONSE = 5;
    const TYPE_REJECT_RESPONSE = 6;
    const TYPE_NEW_ADDR_REQUEST = 10;

    public $timestamps = false;

    protected $casts = [
        'payload' => 'array'
    ];


    public function initer()
    {
        return $this->belongsTo(User::class, 'initiator_id');
    }


    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }
}
