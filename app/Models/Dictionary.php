<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    const SCOPE_GENERAL = 1;
    const SCOPE_WORKPLACE = 2;
    const SCOPE_WORKPOSITION = 3;

    const SCOPES = [
        Dictionary::SCOPE_GENERAL => 'General Dictionary',
        Dictionary::SCOPE_WORKPLACE => 'Місце роботи',
        Dictionary::SCOPE_WORKPOSITION => 'Посада',
    ];

    public $timestamps = false;

    protected $fillable = [ 'value', 'scope' ];
    protected $hidden = [ 'scope' ];
}
