<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inquire extends Model
{
	public $timestamps = false;

    public function group()
    {
    	return $this->belongsTo(AnswerGroup::class);
    }


    public function questionnaire()
    {
    	return $this->belongsTo(Questionnaire::class);
    }


    public function voterAnswers()
    {
    	return $this->hasMany(VoterAnswer::class);
    }
}
