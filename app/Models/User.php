<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    const ROLE_SA = 1;
    const ROLE_ADMINISTRATOR = 2;
    const ROLE_OPERATOR = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = \Hash::make($value);
    }


    public function role()
    {
        return $this->belongsTo(Role::class);
    }


    public function pscs()
    {
        return $this->belongsToMany(Psc::class);
    }


    public function isAdministration()
    {
        return in_array($this->role_id, [ static::ROLE_SA, static::ROLE_ADMINISTRATOR ]);
    }
}
