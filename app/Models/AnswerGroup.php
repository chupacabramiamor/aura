<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerGroup extends Model
{
    const TYPE_QUESTION = 1;    // Відкрите питання
    const TYPE_LIST = 2;        // Список

    public $timestamps = false;

    protected $appends = [ 'type_caption' ];
    protected $fillable = [ 'value' ];

    public function answers()
    {
        return $this->hasMany(Answer::class, 'group_id');
    }


    public function getTypeCaptionAttribute()
    {
        $mapping = static::getTypesMapping();

        return $mapping[$this->type_id];
    }


    public static function getTypesMapping()
    {
        return [
            static::TYPE_QUESTION => 'Відкрите питання',
            static::TYPE_LIST => 'Вибрати зі списку',
        ];
    }
}
