<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voter extends Model
{
    use SoftDeletes;

    const EDUCATION_TYPE_HIGH = 1;
    const EDUCATION_TYPE_UNFINISHED_HIGH = 2;
    const EDUCATION_TYPE_VOCATIONAL = 3;
    const EDUCATION_TYPE_SECONDARY = 4;
    const EDUCATION_TYPE_ELEMENTARY = 5;
    const OCCUPACY_TYPE_IT = 1;                     // працівник IT сфери
    const OCCUPACY_TYPE_MEDICAL = 2;                // працівник медичної сфери
    const OCCUPACY_TYPE_EDUCATIONAL = 3;            // працівник освіти
    const OCCUPACY_TYPE_STUDENT = 4;                // студент
    const OCCUPACY_TYPE_EMPLOYEE = 5;               // найманий працівник
    const OCCUPACY_TYPE_MILITARY = 6;               // військовослужбовець
    const OCCUPACY_TYPE_CIVIL = 7;                  // держслужбовець
    const OCCUPACY_TYPE_POLICE = 8;                 // правоохоронець
    const OCCUPACY_TYPE_HOUSEWIFE = 9;              // домогосподарка
    const OCCUPACY_TYPE_PENSIONER = 10;             // пенсіонер
    const OCCUPACY_TYPE_UNEMPLOYED = 11;            // безробітний
    const OCCUPACY_TYPE_BUSINESSMAN = 12;           // приватний підприємець
    const OCCUPACY_TYPE_ANSWER_REJECTED = 13;       // відмовився відповідати
    const OCCUPACY_TYPE_NO_PERMANENT = 14;          // не маю постійного місця роботи
    const EMPLOYEE_OCCUPACY_TYPE_AGITATOR = 1;
    const EMPLOYEE_OCCUPACY_TYPE_DEC_CHAIRMAN = 2;
    const EMPLOYEE_OCCUPACY_TYPE_PSC_CHAIRMAN = 3;
    const EMPLOYEE_OCCUPACY_TYPE_DEC_DEP_CHAIRMAN = 4;
    const EMPLOYEE_OCCUPACY_TYPE_PSC_DEP_CHAIRMAN = 5;
    const EMPLOYEE_OCCUPACY_TYPE_DEC_MEMBER = 6;
    const EMPLOYEE_OCCUPACY_TYPE_PSC_MEMBER = 7;
    const EMPLOYEE_OCCUPACY_TYPE_PS_HEAD = 8;
    const EMPLOYEE_OCCUPACY_TYPE_EMPLOYEE = 9;
    const EMPLOYEE_OCCUPACY_TYPE_SECRETARY = 10;
    const EMPLOYEE_OCCUPACY_TYPE_OBSERVER = 11;
    const EMPLOYEE_OCCUPACY_TYPE_VOTER = 12;
    const CAMPAIGN_POSITION_SVD1 = 1;               // СВД 1
    const CAMPAIGN_POSITION_SVD2 = 2;               // СВД 2
    const CAMPAIGN_POSITION_SVD3 = 3;               // СВД 3
    const CAMPAIGN_POSITION_VOLUNTEER1 = 4;         // ВОЛОНТЕР 1
    const CAMPAIGN_POSITION_VOLUNTEER2 = 5;         // ВОЛОНТЕР 2
    const CAMPAIGN_POSITION_VOLUNTEER3 = 6;         // ВОЛОНТЕР 3
    const CAMPAIGN_POSITION_PSC1 = 7;               // ДВК 1
    const CAMPAIGN_POSITION_PSC2 = 8;               // ДВК 2
    const CAMPAIGN_POSITION_PSC3 = 9;               // ДВК 3
    const CAMPAIGN_POSITION_OBSERVER1 = 10;         // СПОСТЕРІГАЧ 1
    const CAMPAIGN_POSITION_OBSERVER2 = 11;         // СПОСТЕРІГАЧ 2
    const CAMPAIGN_POSITION_OBSERVER3 = 12;         // СПОСТЕРІГАЧ 3
    const CAMPAIGN_POSITION_CANDIDATE1 = 13;        // КАНДИДАТ 1
    const CAMPAIGN_POSITION_CANDIDATE2 = 14;        // КАНДИДАТ 2
    const CAMPAIGN_POSITION_CANDIDATE3 = 15;        // КАНДИДАТ 3

    public $timestamps = false;

    public $fillable = [
        'gender_type',
        'surname',
        'firstname',
        'patronymic',
        'phone_number',
        'additional_phone_number',
        'email',
        'education_type',
        'occupation_type',
        'party_id',
        'address_id',
    ];

    public function address()
    {
        return $this->belongsTo(Address::class);
    }


    public function party()
    {
        return $this->belongsTo(Party::class);
    }


    public function answers()
    {
        return $this->hasMany(VoterAnswer::class);
    }


    public function activities()
    {
        return $this->morphMany(UserActivity::class, 'loggable');
    }


    public function workplace()
    {
        return $this->belongsTo(Dictionary::class, 'workplace_id');
    }


    public function workposition()
    {
        return $this->belongsTo(Dictionary::class, 'workposition_id');
    }


    public static function getEducationTypesData()
    {
        return [
            [ 'id' => static::EDUCATION_TYPE_HIGH, 'value' => 'Вища' ],
            [ 'id' => static::EDUCATION_TYPE_UNFINISHED_HIGH, 'value' => 'Незакінчена вища' ],
            [ 'id' => static::EDUCATION_TYPE_VOCATIONAL, 'value' => 'Професіно-технічна' ],
            [ 'id' => static::EDUCATION_TYPE_SECONDARY, 'value' => 'Середня' ],
            [ 'id' => static::EDUCATION_TYPE_ELEMENTARY, 'value' => 'Початкова' ],
        ];
    }


    public static function getOccupacyTypesData()
    {
        return [
            [ 'id' => static::OCCUPACY_TYPE_IT, 'value' => 'працівник IT сфери' ],
            [ 'id' => static::OCCUPACY_TYPE_MEDICAL, 'value' => 'працівник медичної сфери' ],
            [ 'id' => static::OCCUPACY_TYPE_EDUCATIONAL, 'value' => 'працівник освіти' ],
            [ 'id' => static::OCCUPACY_TYPE_STUDENT, 'value' => 'студент' ],
            [ 'id' => static::OCCUPACY_TYPE_EMPLOYEE, 'value' => 'найманий працівник' ],
            [ 'id' => static::OCCUPACY_TYPE_MILITARY, 'value' => 'військовослужбовець' ],
            [ 'id' => static::OCCUPACY_TYPE_CIVIL, 'value' => 'держслужбовець' ],
            [ 'id' => static::OCCUPACY_TYPE_POLICE, 'value' => 'правоохоронець' ],
            [ 'id' => static::OCCUPACY_TYPE_HOUSEWIFE, 'value' => 'домогосподарка' ],
            [ 'id' => static::OCCUPACY_TYPE_PENSIONER, 'value' => 'пенсіонер' ],
            [ 'id' => static::OCCUPACY_TYPE_UNEMPLOYED, 'value' => 'безробітний' ],
            [ 'id' => static::OCCUPACY_TYPE_BUSINESSMAN, 'value' => 'приватний підприємець' ],
            [ 'id' => static::OCCUPACY_TYPE_ANSWER_REJECTED, 'value' => 'відмовився відповідати' ],
            [ 'id' => static::OCCUPACY_TYPE_NO_PERMANENT, 'value' => 'не маю постійного місця роботи' ],
        ];
    }


    public static function getCampaignPositionsData()
    {
        return [
            [ 'id' => static::CAMPAIGN_POSITION_SVD1, 'value' => 'СВД 1' ],
            [ 'id' => static::CAMPAIGN_POSITION_SVD2, 'value' => 'СВД 2' ],
            [ 'id' => static::CAMPAIGN_POSITION_SVD3, 'value' => 'СВД 3' ],
            [ 'id' => static::CAMPAIGN_POSITION_VOLUNTEER1, 'value' => 'ВОЛОНТЕР 1' ],
            [ 'id' => static::CAMPAIGN_POSITION_VOLUNTEER2, 'value' => 'ВОЛОНТЕР 2' ],
            [ 'id' => static::CAMPAIGN_POSITION_VOLUNTEER3, 'value' => 'ВОЛОНТЕР 3' ],
            [ 'id' => static::CAMPAIGN_POSITION_PSC1, 'value' => 'ДВК 1' ],
            [ 'id' => static::CAMPAIGN_POSITION_PSC2, 'value' => 'ДВК 2' ],
            [ 'id' => static::CAMPAIGN_POSITION_PSC3, 'value' => 'ДВК 3' ],
            [ 'id' => static::CAMPAIGN_POSITION_OBSERVER1, 'value' => 'СПОСТЕРІГАЧ 1' ],
            [ 'id' => static::CAMPAIGN_POSITION_OBSERVER2, 'value' => 'СПОСТЕРІГАЧ 2' ],
            [ 'id' => static::CAMPAIGN_POSITION_OBSERVER3, 'value' => 'СПОСТЕРІГАЧ 3' ],
            [ 'id' => static::CAMPAIGN_POSITION_CANDIDATE1, 'value' => 'КАНДИДАТ 1' ],
            [ 'id' => static::CAMPAIGN_POSITION_CANDIDATE2, 'value' => 'КАНДИДАТ 2' ],
            [ 'id' => static::CAMPAIGN_POSITION_CANDIDATE3, 'value' => 'КАНДИДАТ 3' ],
        ];
    }


    public function setPhoneNumberAttribute($value)
    {
        return $this->attributes['phone_number'] = $value ? static::msisdn($value) : '';
    }


    public function setAdditionalPhoneNumberAttribute($value)
    {
        return $this->attributes['additional_phone_number'] = $value ? static::msisdn($value) : '';
    }


    public static function msisdn($value)
    {
        return str_pad(substr(preg_replace('~[^\d]~', '', $value), 0, 12), 12, '380', STR_PAD_LEFT);
    }
}
