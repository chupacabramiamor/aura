<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Psc extends Model
{
    public $timestamps = false;

    public function locality()
    {
        return $this->belongsTo(Locality::class);
    }
}
