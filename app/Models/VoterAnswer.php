<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoterAnswer extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'answer_id', 'voter_id', 'inquire_id' ];

    public function voter()
    {
    	return $this->belongsTo(Voter::class);
    }


    public function answer()
    {
    	return $this->belongsTo(Answer::class);
    }
}
