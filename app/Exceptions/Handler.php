<?php

namespace App\Exceptions;

use Exception;
use Brizlibs\BillingClients\Exceptions as BrizExceptions;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception as SymfonyHttpExceptions;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        SymfonyHttpExceptions\HttpException::class,
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if ($exception instanceof BrizExceptions\ApiException) {
            $data = json_decode($exception->getMessage());

            if (json_last_error() == JSON_ERROR_NONE) {
                $statusCode = 503;

                if ($data->errors == 'token_expired') {
                    $statusCode = 401;
                }

                return response()->view('errors.service', [
                    'statusCode' => $statusCode,
                    'message' => $data->errors
                ]);
            }
        }

        if ($request->expectsJson() || $request->segment(1) == 'api') {

            if ($exception instanceof \Illuminate\Validation\ValidationException) {
                return parent::render($request, $exception);
            }

            if ($exception instanceof SymfonyHttpExceptions\HttpExceptionInterface) {
                $statusCode = $exception->getStatusCode();
            } else $statusCode = 500;

            if ($exception instanceof AuthenticationException) {
                $statusCode = 401;
            }

            return response()->json([
                'error' => (new \ReflectionClass($exception))->getShortName(),
                'message' => $exception->getMessage()
            ], $statusCode);
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson() || $request->segment(1) == 'api') {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
