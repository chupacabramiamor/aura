<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Address;
use App\Models\Street;
use DB;

class StreetsRestructure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restructure:streets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restructure DB structure of addresses and streets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expression = "SELECT `street_name`, `street_type`, `locality_id`, count(*) AS `amount`
            FROM `addresses`
            GROUP BY `street_name`, `street_type`, `locality_id`
            ORDER BY `locality_id`
        ";
        $result = DB::select(DB::raw($expression));

        foreach ($result as $item) {
            $street = Street::create([
                'name' => $item->street_name,
                'type' => $item->street_type,
                'locality_id' => $item->locality_id,
            ]);

            Address::where([
                'street_name' => $item->street_name,
                'street_type' => $item->street_type,
                'locality_id' => $item->locality_id,
            ])->get()->each(function($address) use($street) {
                $address->street_id = $street->id;
                $address->save();
            });
        }
    }
}
