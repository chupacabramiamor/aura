<?php

namespace App\Observers;

use App\Models\Voter;

class Voters
{

    public function saving(Voter $model)
    {
        if ($model->isDirty('workplace')) {
            Event::fire('dictionary.updating', [
                'scope' => Dictionary::SCOPE_WORKPLACE,
                'value' => $model->workplace
            ]);
        }

        if ($model->isDirty('workposition')) {
            Event::fire('dictionary.updating', [
                'scope' => Dictionary::SCOPE_WORKPOSITION,
                'value' => $model->workposition
            ]);
        }
    }
}
