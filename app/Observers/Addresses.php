<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use App\Models\Street;

class Addresses
{
    public function saving(Model $model)
    {
        $street = Street::find($model->street_id);

        $model->street_type = $street->type;
        $model->street_name = $street->name;
    }
}
