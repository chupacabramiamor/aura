<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class Streets
{
    public function saved(Model $model)
    {
        if ($model->prev_id) {
            $model->prev->is_outated = 1;
            $model->prev->save();

            $model->prev->addresses->each(function($address) use($model) {
                $address->street_id = $model->id;
                $address->street_name = $model->name;
                $address->street_type = $model->type;

                $address->unsetEventDispatcher();

                $address->save();
            });
        }
    }
}
