<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta id="_token" content="{{ csrf_token() }}">
        <title></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link href="/css/vendor.build.css" rel="stylesheet">
        <link href="/css/icons.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">

        <script src="/js/vendor.build.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

    </head>

    <body>

        <div id="page-wrapper">
            <div class="topbar">

                <div class="topbar-left">
                    <div class="">
                        <a href="{{ route('home') }}" class="logo">
                            <img src="/images/logo.png" alt="logo" class="logo-lg" />
                            <img src="/images/logo_sm.png" alt="logo" class="logo-sm hidden" />
                        </a>
                    </div>
                </div>


                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                            <ul class="nav navbar-nav hidden-sm hidden-xs top-navbar-items">
                                <li><a href="#">Техподдержка</a></li>
                            </ul>
                        <div class="">
                            <!-- Mobile menu button -->
                            <div class="pull-left">
                                <button type="button" class="button-menu-mobile visible-xs visible-sm">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav navbar-right top-navbar-items-right pull-right">
                                <li id="vue-notifies-app"><notify-tray></notify-tray></li>
                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle menu-right-item profile" data-toggle="dropdown" aria-expanded="false"><img src="/images/avatar-1.jpg" alt="user-img" class="img-circle"> <strong>{{ Auth::user()->login }}</strong></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('logout') }}"><i class="ti-power-off m-r-10"></i> Выход из системы</a></li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    </div> <!-- end container -->
                </div> <!-- end navbar -->
            </div>

            <div class="page-contentbar">

                <!--left navigation start-->
                <aside class="sidebar-navigation">
                    <div class="scrollbar-wrapper">
                        <div>
                            <button type="button" class="button-menu-mobile btn-mobile-view visible-xs visible-sm">
                                <i class="mdi mdi-close"></i>
                            </button>
                            <!-- User Detail box -->
                            <div class="user-details">
                                <div class="pull-left">
                                    <img src="/images/avatar-1.jpg" alt="" class="thumb-md img-circle">
                                </div>
                                <div class="user-info">
                                    <a href="#">{{ Auth::user()->fullname }}</a>
                                    <p class="text-muted m-0">{{ Auth::user()->role->name }}</p>
                                </div>
                            </div>
                            <!--- End User Detail box -->

                            <!-- Left Menu Start -->
                            <ul class="metisMenu nav" id="side-menu">
                                <li><a href="{{ route('home') }}"><i class="ti-home"></i> Головна </a></li>
                                @if (Auth::user()->role_id == \App\Models\User::ROLE_ADMINISTRATOR)
                                <li><a href="/sync"><i class="ti-import"></i> Імпорт даних </a></li>
                                <li><a href="/operators"><i class="ti-user"></i> Оператори </a></li>
                                <li>
                                    <a href="javascript: void(0);"><i class="ti-light-bulb"></i> Питання <span class="fa arrow"></span></a>
                                    <ul class="nav-second-level nav collapse" aria-expanded="false">
                                        <li><a href="/questionnaire">Опитування</a></li>
                                        <li><a href="/questionnaire/answer-groups">Группи відповідей</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript: void(0);"><i class="ti-layout-cta-left"></i> Адресна книга <span class="fa arrow"></span></a>
                                    <ul class="nav-second-level nav collapse" aria-expanded="false">
                                        <li><a href="/addresses">Адресні списки</a></li>
                                        <li><a href="/addresses/add">Додати нову адресу</a></li>
                                    </ul>
                                </li>
                                <li><a href="/voters"><i class="ti-harddrives"></i> База виборців </a></li>
                                @endif

                                @if (Auth::user()->role_id == \App\Models\User::ROLE_OPERATOR)
                                <li><a href="/voters"><i class="ti-harddrives"></i> База виборців </a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </aside>

                <div id="page-right-content">

                    <div class="container">
                        @yield('content')
                    </div>
                </div>

            </div>

        </div>

        <script type="text/x-template" id="notify-tray-component-tpl">
            <div class="dropdown top-menu-item-xs">
                <a href="#" data-target="#" class="dropdown-toggle menu-right-item" data-toggle="dropdown">
                    <i class="mdi mdi-bell"></i> <span class="label label-danger" v-show="amount > 0">@{{ amount }}</span>
                </a>
                <notify-list @change="refreshAmount"></notify-list>
            </div>
        </script>
        <script type="text/x-template" id="notify-list-component-tpl">
            <ul class="dropdown-menu p-0 dropdown-menu-lg" v-if="list.length > 0">
                <li class="list-group notification-list" style="height: 267px;">
                   <div class="slimscroll">
                        <a href="javascript:void(0);" class="list-group-item" v-for="notify in list" @click="markAsOpened(notify.id)" :class="{new: !notify.is_opened}">
                            <div class="media">
                                <div class="media-left p-r-10">
                                    <em class="fa fa-building bg-warning"></em>
                                </div>
                                <div class="media-body">
                                    <h5 class="media-heading">@{{ notify.title }}</h5>
                                    <p class="m-0"><small>@{{ notify.message }}</small></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
            </ul>
        </script>

        <script src="/js/jquery.app.js"></script>
        <script src="/js/master.vue-bundle.js"></script>
        @yield('scripts')
    </body>
</html>