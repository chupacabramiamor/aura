<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta id="_token" content="{{ csrf_token() }}">
        <title></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link href="/css/vendor.build.css" rel="stylesheet">
        <link href="/css/icons.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">

        <script src="/js/vendor.build.js"></script>

    </head>

    <body>
        <!-- HOME -->
        <section>
            <div class="container">
                @yield('content')
            </div>
        </section>
        <!-- END HOME -->

        @yield('scripts')
    </body>
</html>