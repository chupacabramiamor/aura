@extends('layouts.master')

@section('content')
<div id="vue-answer-groups-app">
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right">
                <button class="btn btn-primary btn-bordered btn-sm" @click="showGroupCreatingModal()"> Добавить новую группу </button>
            </div>
            <h4 class="header-title m-t-10">Группы ответов</h4>
        </div>
    </div>
    <div class="row">
        <v-answer-groups-list></v-answer-groups-list>
    </div>

    <v-modal id="answer-group-creating-modal">
        <v-answer-group-creating-form></v-answer-group-creating-form>
    </v-modal>

    <v-modal id="answer-group-editing-modal">
        <v-answer-group-editing-form></v-answer-group-editing-form>
    </v-modal>
</div>

<script type="text/x-template" id="answer-groups-list-tpl">
<div class="card-box" v-blockui="preloaders.list">
    <div v-if="list.length > 0">
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-hovered">
                    <thead>
                        <tr>
                            <th class="col-xs-7">Группа</th>
                            <th class="col-xs-2 text-center">Тип</th>
                            <th class="col-xs-3"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="answerGroup in list">
                            <td>@{{ answerGroup.name }}</td>
                            <td class="text-center">@{{ answerGroup.type_caption }}</td>
                            <td class="text-right">
                                <button class="btn btn-default btn-bordered btn-sm" @click="edit(answerGroup.id)"><i class="ti-more"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr class="m-t-0" />
        </div>
    </div>
    <div v-else class="alert alert-icon alert-white alert-info">
        <i class="mdi mdi-information"></i>
        <p>Групп ответов не обнаружено</p>
    </div>
</div>
</script>

<script type="text/x-template" id="answer-group-form-tpl">
<form @submit.prevent="save()" v-blockui="preloaders.form">
    <v-modal-header title="Добавление новой группы"></v-modal-header>
    <div class="modal-body">
        <div class="row">
            <div class="col-xs-12">
                <v-control-group label="Название группы" :errors="controlValidation.name">
                    <v-input-control v-model="model.name"></v-input-control>
                </v-control-group>
            </div>
            <div v-if="model.id">
                <div class="col-xs-12 text-right">
                    <span style="border-bottom: dotted 1px #676a6c">Тип:</span> <span class="label label-info">@{{ model.type_caption }}</span>
                </div>
            </div>
            <div v-else class="col-xs-12">
                <v-control-group label="Тип ответов" :srOnly="false" :errors="controlValidation.type_id">
                    <v-select-control v-model="model.type_id" :list="typeList"></v-select-control>
                </v-control-group>
            </div>
        </div>
        <transition name="slide">
            <div class="row" v-if="model.type_id">
                <div class="col-xs-12">
                    <answer-list></answer-list>
                </div>
            </div>
        </transition>
    </div>
    <div class="modal-footer">
        <div class="pull-left" v-show="isRemovable">
            <button type="button" class="btn btn-default btn-bordered" @click="remove()">Удалить</button>
        </div>
        <div class="pull-right">
            <button type="submit" class="btn btn-primary btn-bordered">Сохранить</button>
        </div>
    </div>
</form>
</script>

<script type="text/x-template" id="answer-list-tpl">
<div v-blockui="preloaders.form">
    <h4 class="text-center">Варианты</h4>
    <hr class="m-t-0">
    <v-control-group label="Ответ">
        <div class="input-group">
            <v-input-control v-model="answer"></v-input-control>
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" @click="add()"><i class="ti-plus"></i></button>
            </span>
        </div>
    </v-control-group>
    <p class="text-muted text-right" v-if="list.length == 0"><small>Добавьте в эту секцию варианты ответов для данной группы вопросов.</small></p>
    <div v-else>
        <table class="table table-hover borderless">
            <tbody>
                <tr v-for="(item, index) in list" :key="index">
                    <td class="col-xs-10">
                        <div v-if="editable === index">
                            <v-input-control class="input-sm" v-model="alterData.value"></v-input-control>
                        </div>
                        <div v-else><strong>@{{ index + 1 }}.</strong> @{{ item.value }}</div>
                    </td>
                    <td class="col-xs-2 text-right">
                        <div v-if="editable === index">
                            <button type="button" class="btn btn-custom btn-sm" @click="save(index)"><i class="ti-save"></i></button>
                            <button type="button" class="btn btn-default btn-sm" @click="edit(index)"><i class="ti-close"></i></button>
                        </div>
                        <div v-else>
                            <button type="button" class="btn btn-default btn-xs" @click="edit(index)"><i class="ti-pencil"></i></button>
                            <button type="button" class="btn btn-dark btn-xs" @click="remove(index)"><i class="ti-close"></i></button>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</script>
@endsection

@section('scripts')
<script src="/js/master.answer-groups.index.vue-bundle.js"></script>
@endsection