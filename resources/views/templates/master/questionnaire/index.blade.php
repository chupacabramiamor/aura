@extends('layouts.master')

@section('content')
<div id="vue-questionnaire-app">
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right">
                <button class="btn btn-primary btn-bordered btn-sm" @click="showQuestionnaireCreatingModal()"> Добавить новый опросник </button>
            </div>
            <h4 class="header-title m-t-10">Список опросников</h4>
        </div>
    </div>
    <div class="row">
        <v-questionnaire-list v-blockui="preloaders.list"></v-questionnaire-list>
    </div>

    <v-modal id="questionnaire-creating-modal">
        <v-questionnaire-creating-form></v-questionnaire-creating-form>
    </v-modal>

    <v-modal id="questionnaire-editing-modal">
        <v-questionnaire-editing-form v-blockui="preloaders.form"></v-questionnaire-editing-form>
    </v-modal>

    <v-modal id="inquiry-creating-modal">
        <v-inquiry-creating-form></v-inquiry-creating-form>
    </v-modal>

    <v-modal id="inquiry-editing-modal">
        <v-inquiry-editing-form v-blockui="preloaders.form"></v-inquiry-editing-form>
    </v-modal>
</div>

<script type="text/x-template" id="questionnaire-creating-form-tpl">
    <form @submit.prevent="save()" v-blockui="preloaders.form">
        <v-modal-header title="Добавление нового опросника"></v-modal-header>
        <div class="modal-body row">
            <div class="col-xs-12">
                <v-control-group label="Название опросника" :errors="controlValidation.name">
                    <v-input-control v-model="model.name"></v-input-control>
                </v-control-group>
            </div>
        </div>
        <div class="modal-footer">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-bordered">Сохранить</button>
            </div>
        </div>
    </form>
</script>

<script type="text/x-template" id="questionnaire-editing-form-tpl">
    <form @submit.prevent="save()" v-blockui="preloaders.form">
        <v-modal-header title="Переименовывание опросника"></v-modal-header>
        <div class="modal-body row">
            <div class="col-xs-12">
                <v-control-group label="Название опросника" :errors="controlValidation.name">
                    <v-input-control v-model="model.name"></v-input-control>
                </v-control-group>
            </div>
        </div>
        <div class="modal-footer">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-bordered">Сохранить</button>
            </div>
        </div>
    </form>
</script>

<script type="text/x-template" id="inquiry-creating-form-tpl">
    <form @submit.prevent="save()" v-blockui="preloaders.form" class="form-horizontal">
        <v-modal-header title="Добавление вопроса"></v-modal-header>
        <div class="modal-body row">
            <div class="col-xs-12">
                <v-control-group label="Текст вопроса" :srOnly="false" :errors="controlValidation.question">
                    <v-textarea-control v-model="model.question"></v-textarea-control>
                </v-control-group>
            </div>

            <div class="col-xs-12">
                <v-control-group label="Група ответов" :srOnly="false" :errors="controlValidation.group_id">
                    <v-select-control v-model="model.group_id" :list="groupList"></v-select-control>
                </v-control-group>
                <div class="alert alert-icon alert-white alert-warning">
                    <i class="mdi mdi-alert"></i> <strong>Обратите внимание!</strong> После добавления вопроса вы не сможете изменить группу ответов для него.
                </div>
            </div>

            <div class="col-xs-12">
            </div>

        </div>
        <div class="modal-footer">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-bordered">Сохранить</button>
            </div>
        </div>
    </form>
</script>

<script type="text/x-template" id="inquiry-editing-form-tpl">
    <form @submit.prevent="save()" v-blockui="preloaders.form">
        <v-modal-header title="Редактирование вопроса"></v-modal-header>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-12">
                    <v-control-group label="Текст вопроса" :srOnly="false" :errors="controlValidation.question">
                        <v-textarea-control v-model="model.question"></v-textarea-control>
                    </v-control-group>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <v-control-group label="Група ответов:" :srOnly="false" :errors="controlValidation.group_id">
                        <span v-if="model.group" class="m-l-10 text-muted">@{{ model.group.name }} <i>(@{{ model.group.type_caption }})</i></span>
                    </v-control-group>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="pull-left">
                <button type="button" class="btn btn-default btn-bordered" @click="remove(model.id)">Удалить</button>
            </div>
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-bordered">Сохранить</button>
            </div>
        </div>
    </form>
</script>

<script type="text/x-template" id="questionnaire-list-tpl">
    <div class="card-box" v-blockui="preloaders.list">
        <div v-if="list.length" v-for="questionnaire in list">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-right">
                        <button class="btn btn-custom btn-bordered btn-sm" @click="showInquiryCreatingModal(questionnaire.id)">Добавить вопрос</button>
                        <button class="btn btn-danger btn-bordered btn-sm" v-show="questionnaire.inquires_amount == 0 && list.length > 1" @click="showQuestionnaireRemovingConfirm(questionnaire.id)"><i class="ti-trash"></i></button>
                    </div>
                    <h4 class="m-t-5">
                        <a href="#" @click.prevent="toggle(questionnaire.id)">@{{ questionnaire.name }} (@{{ questionnaire.inquires_amount }})</a>
                        <button class="btn btn-default btn-bordered btn-sm" @click="showQuestionnaireEditingModal(questionnaire.id)"><i class="ti-pencil"></i></button>
                    </h4>
                </div>
            </div>
            <v-inquiry-list @change="updateItem($event, questionnaire.id)" :questionnaire_id="questionnaire.id" :is-expanded="active_id == questionnaire.id"></v-inquiry-list>
            <hr class="m-t-0" />
        </div>
    </div>
</script>

<script type="text/x-template" id="inquiry-list-tpl">
    <transition name="slide">
        <div v-show="isExpanded">
            <div class="row m-t-10" v-if="list.length > 0">
                <div class="col-xs-12">
                    <table class="table table-hover borderless">
                        <tbody>
                            <tr v-for="inquiry in list">
                                <td>@{{ inquiry.question }} (<span class="label label-info">@{{ inquiry.group.name }}. @{{ inquiry.group.type_caption }}</span>)</td>
                                <td class="col-xs-2 text-right">
                                    <button class="btn btn-default btn-bordered btn-sm" @click="edit(inquiry.id)"><i class="ti-more"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div v-else class="alert alert-icon alert-white alert-info alert-dismissible fade in" role="alert">
                <i class="mdi mdi-information"></i>
                <p>Вопросы пока еще не добавлены</p>
            </div>
        </div>
    </transition>
</script>
@endsection

@section('scripts')
    <script>
        var questionnaireList = {!! json_encode($questionnaireList) !!}
    </script>

    <script src="/js/master.questionnaire.index.vue-bundle.js"></script>
@endsection