@extends('layouts.master')

@section('content')
<div class="row"><div class="col-sm-12"><h4 class="header-title m-t-0 m-b-20">Імпорт виборців</h4></div></div>

<div id="vue-sync-app" v-blockui="preloaders.form">
    <div v-if="xlsx.state >= 3 && xlsx.state != 5">
        <div class="card-box row">
            <div class="col-xs-10">
                <p class="m-t-10 m-b-0">
                    <strong>Кількість підготовлених до імпорту записів:</strong> @{{ xlsx.data.prepared_count }}
                </p>
            </div>
            <div class="col-xs-2 text-right">
                <button type="button" class="btn btn-primary" @click="confirmation()"><i class="fa fa-check m-r-10"></i>Підтвердити</button>
            </div>
        </div>
    </div>
    <div v-else>
        <v-uploader></v-uploader>
        <hr>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-warning alert-white" role="alert">
                    <strong>Користувач!</strong> Перед завантаженням файла імпорту переконайся у коректості данних!
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/x-template" id="uploader-tpl">
<div class="row" v-if="!isUploaded">
    <div class="col-md-3">
        <v-select-control :list="modList" v-model="mod" display="value" placeholder="Оберіть локацію..."></v-select-control>
    </div>
    <div class="col-md-7">
        <v-fileuploader-control v-model="preparedFile"></v-fileuploader-control>
    </div>
    <div class="col-md-2">
        <button class="form-control btn-default" :class="{ 'btn-primary' : preparedFile }" @click="doUpload()" :disabled="!preparedFile"><i class="fa fa-cloud-upload"></i> Завантажити</button>
    </div>
</div>
</script>
@endsection

@section('scripts')
    <script src="/js/master.sync.index.vue-bundle.js"></script>
@endsection