@extends('layouts.master')

@section('content')
<div id="vue-address-adding-app">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="header-title m-t-10">Додавання нової адреси</h4>
        </div>
    </div>
    <address-form></address-form>
    <v-modal id="street-creating-modal" size="large">
        <v-modal-header title="Додавання нової вулиці"></v-modal-header>
        <street-form></street-form>
    </v-modal>
</div>

<script type="text/x-template" id="street-form-tpl">
    <form v-blockui="preloaders.form" class="form-horizontal" @submit.prevent="save()">
        <div class="modal-body">
            <v-control-group label="Населений пункт *" type="horizontal" :errors="controlValidation.locality_id">
                <v-multiselect v-model="locality" :options="localityList" label="name" placeholder="Виберіть значення"></v-multiselect>
            </v-control-group>

            <hr>

            <div v-if="locality">
                <v-control-group label="Тип вулиці *" type="horizontal" :errors="controlValidation.type">
                    <v-select-control v-model="form.type" :list="streetTypeList" display="value"></v-select-control>
                </v-control-group>

                <v-control-group label="Назва *" type="horizontal" :errors="controlValidation.name || controlValidation.prev_id">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" v-model="form.name" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <v-multiselect v-model="prevStreet" :options="streetList" label="name" placeholder="Виберіть значення"></v-multiselect>
                        </div>
                    </div>
                </v-control-group>
            </div>
        </div>
        <div class="modal-footer">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-bordered" :disabled="!locality"><i class="fa fa-check m-r-10"></i>Додати</button>
            </div>
        </div>
    </form>
</script>

<script type="text/x-template" id="address-form-tpl">
    <form v-blockui="preloaders.form" class="form-horizontal" @submit.prevent="save()">
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <v-control-group label="Номер ДВК" type="horizontal" :errors="controlValidation.psc_id">
                    <v-multiselect v-model="psc" :options="pscList" label="name" placeholder="Виберіть значення"></v-multiselect>
                </v-control-group>
            </div>
        </div>
        <div class="row" v-if="notifyIniter">
            <div class="col-xs-12">
                <div class="alert alert-warning alert-white" role="alert">
                    <strong>Увага!</strong> Додавання цієї адреси ініційовано оператором (@{{ notifyIniter.fullname || notifyIniter.login }}) та при затвердженні даних майте додаткову увагу!
                </div>
            </div>
        </div>
        <hr class="m-t-0">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <v-control-group label="Поштовий індекс" type="horizontal" :errors="controlValidation.postcode">
                    <v-input-control v-model="form.postcode"></v-input-control>
                </v-control-group>
            </div>
            <div class="col-xs-12 col-md-8">
                <v-control-group label="Населений пункт" type="horizontal" :errors="controlValidation.locality_id">
                    <v-multiselect v-model="locality" :options="localities" label="name" placeholder="Виберіть значення"></v-multiselect>
                </v-control-group>
            </div>

            <div class="row"></div>

            <div class="col-xs-12 col-md-4">
                <v-control-group label="Вулиця *" type="horizontal" :errors="controlValidation.street">
                    <v-multiselect v-model="street" :options="streetList | transformStreetNames" label="name_humanly" placehoder="Виберіть значення"></v-multiselect>
                </v-control-group>
            </div>

            <div class="col-xs-12 col-md-2">
                <div class="m-t-10">
                    <a href="#" @click="newStreetModal()">Додати вулицю</a>
                </div>
            </div>

            <div class="row"></div>

            <div class="col-xs-12 col-md-4">
                <v-control-group label="Номер будинку *" type="horizontal" :errors="controlValidation.house_number">
                    <v-input-control v-model="form.house_number"></v-input-control>
                </v-control-group>
            </div>

            <div class="col-xs-12 col-md-4">
                <v-control-group label="Корпус" type="horizontal" :errors="controlValidation.building">
                    <v-input-control v-model="form.building"></v-input-control>
                </v-control-group>
            </div>

            <div class="col-xs-12 col-md-4">
                <v-control-group label="Номер квартири" type="horizontal" :errors="controlValidation.apartment_number">
                    <v-input-control v-model="form.apartment_number"></v-input-control>
                </v-control-group>
            </div>

            </div>
            <hr>

            <div class="row">
                <div class="col-xs-6 form-group m-b-0">
                    <button type="button" class="btn btn-warning" v-show="notifyIniter" @click="rejectNewAddr()">Відмовити заявку</button>
                </div>
                <div class="col-xs-6 form-group text-right m-b-0">
                    <a href="/addresses" class="m-r-10">Повернутися до списків адрес</a>
                    <button class="btn btn-primary"><i class="fa fa-save"></i> Зберегти</button>
                </div>
            </div>
        </div>
    </form>
</script>

<script id="psc-placement-tpl" type="text/x-template">
    <div>
        <h5 class="m-t-5 text-info" v-if="psc.locality">@{{ psc.locality | prettifyLocality }}, @{{ psc.locality.area | prettifyLocality}}, @{{ psc.locality.area.region | prettifyLocality}}
        <br>
        <small class="text-muted">@{{ psc.placement }}</small>
        </h5>
    </div>
</script>
@endsection

@section('scripts')
    <script>
        var pscList = {!! json_encode($pscList) !!};
        var localityList = {!! json_encode($localityList) !!};
        var notify_id = {!! json_encode($notify_id) !!};
    </script>
    <script src="/js/master.addresses.add.vue-bundle.js"></script>
@endsection