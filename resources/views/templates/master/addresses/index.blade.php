@extends('layouts.master')

@section('content')
<div id="vue-addresses-app">
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right">
                <button class="btn btn-custom btn-bordered btn-sm" @click="followAdding()"><i class="fa fa-plus m-r-10"></i>Додати нову адресу</button>
            </div>
            <h4 class="header-title m-t-10">Адресные списки</h4>
        </div>
    </div>
    <div class="row">
    	<search-form></search-form>
    </div>
    <hr class="m-t-0">
    <div class="row">
		<address-list></address-list>
    </div>
</div>

<script id="search-form-tpl" type="text/x-template">
	<div v-blockui="preloaders.form">
		<form @submit.prevent="submit()">
			<div class="col-sm-2 form-group" :class="{ 'has-error': controlValidation.locality_id }">
				<v-select-control :list="locationList" v-model="model.locality_id" placeholder="Оберіть населений пункт"></v-select-control>
				<p class="help-block small" v-if="controlValidation.locality_id" v-html="controlValidation.locality_id[0]"></p>
			</div>
			<div class="col-sm-5 form-group" :class="{ 'has-error': controlValidation.street_name }">
				<v-input-control v-model="model.street_name" placeholder="Вулиця"></v-input-control>
				<p class="help-block small" v-if="controlValidation.street_name" v-html="controlValidation.street_name[0]"></p>
			</div>
			<div class="col-sm-1 form-group" :class="{ 'has-error': controlValidation.house_number }">
				<v-input-control v-model="model.house_number" placeholder="Будинок"></v-input-control>
				<p class="help-block small" v-if="controlValidation.house_number" v-html="controlValidation.house_number[0]"></p>
			</div>
			<div class="col-sm-1 form-group" :class="{ 'has-error': controlValidation.building }">
				<v-input-control v-model="model.building" placeholder="Корпус"></v-input-control>
				<p class="help-block small" v-if="controlValidation.building" v-html="controlValidation.building[0]"></p>
			</div>
			<div class="col-sm-1 form-group" :class="{ 'has-error': controlValidation.apartment_number }">
				<v-input-control v-model="model.apartment_number" placeholder="Квартира"></v-input-control>
				<p class="help-block small" v-if="controlValidation.apartment_number" v-html="controlValidation.apartment_number[0]"></p>
			</div>
			<div class="col-sm-2">
				<button class="btn btn-primary form-control"><i class="fa fa-search m-r-10"></i>Шукати</button>
			</div>
		</form>
	</div>
</script>

<script id="address-list-tpl" type="text/x-template">
    <div class="col-sm-12" v-blockui="preloaders.list">
		<table class="table table-hover" v-if="list.length > 0">
			<thead>
				<tr>
					<th class="text-center" style="width: 8%">ДВК</th>
					<th class="text-center" style="width: 8%">Индекс</th>
					<th style="width: 12%">Тип улицы</th>
					<th>Название улицы</th>
					<th class="text-center" style="width: 8%">№ дома</th>
					<th class="text-center" style="width: 8%">Корпус</th>
					<th class="text-center" style="width: 8%">№ квартиры</th>
					<th style="width: 10%"></th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="item in list">
					<td class="text-center"><p class="m-t-5">@{{ item.psc_number }}</p></td>
					<td class="text-center"><p class="m-t-5">@{{ item.postcode }}</p></td>
					<td><p class="m-t-5">@{{ item.street_type | transformStreetType }}</p></td>
					<td><p class="m-t-5">@{{ item.street_name }}</p></td>
					<td class="text-center"><p class="m-t-5">@{{ item.house_number }}</p></td>
					<td class="text-center"><p class="m-t-5">@{{ item.building || '-' }}</p></td>
					<td class="text-center"><p class="m-t-5">@{{ item.apartment_number || '-' }}</p></td>
					<td class="text-right">
						<button class="btn btn-warning btn-bordered btn-sm" @click="followEditing(item.id)">Изменить</button>
					</td>
				</tr>
			</tbody>
		</table>

        <div class="alert alert-warning alert-white" role="alert" v-else>За вашими пошуковими критеріями нічого не знайдено. Для пошуку виберіть населений пункт та введіть адресу (або її частину)</div>
    </div>
</script>
@endsection

@section('scripts')
	<script src="/js/master.addresses.index.vue-bundle.js"></script>
@endsection