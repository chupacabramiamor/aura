@extends('layouts.master')

@section('content')
	<div class="row">
        <div class="col-sm-12">
            <div class="card-box widget-inline">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="widget-inline-box text-center">
                            <h3 class="m-t-10"><i class="text-custom mdi mdi-account-card-details"></i> <b data-plugin="counterup">{{ $voters_count }}</b></h3>
                            <p class="text-muted">Кількість виборців</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="widget-inline-box text-center">
                            <h3 class="m-t-10"><i class="text-info mdi mdi-delta"></i> <b data-plugin="counterup">{{ $addresses_count }}</b></h3>
                            <p class="text-muted">Кількість адрес</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-6">
                        <div class="widget-inline-box text-center b-0">
                            <h3 class="m-t-10"><i class="text-danger mdi mdi-city"></i> <b data-plugin="counterup">{{ $localities_count }}</b></h3>
                            <p class="text-muted">Кількість населених пунктів</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection