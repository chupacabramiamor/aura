@extends('layouts.master')

@section('content')
<div id="vue-voters-edit-app">
    <div class="row">
        <div class="col-xs-12"><h4 class="header-title m-t-10"><a href="/voters"><i class="fa fa-arrow-left m-r-10"></i></a>Редагування виборця</h4></div>
    </div>

    <form class="form-horizontal" @submit.prevent="save()" v-blockui="preloaders.form">
        <h4 class="text-center m-b-20">Основні данні</h4>
        <div class="form-group" :class="{ 'has-error': controlValidation.psc }">
            <label class="col-md-2 control-label">Номер ДВК</label>
            <div class="col-md-2">
                <v-multiselect v-model="psc" :options="pscList" label="name" placeholder="Оберіть ДВК"></v-multiselect>
                <p class="help-block" v-if="controlValidation.psc" v-html="controlValidation.psc[0]"></p>
            </div>
            <div class="col-md-8">
                <psc-placement :psc="psc"></psc-placement>
            </div>
        </div>
        <div class="form-group" :class="{ 'has-error': controlValidation.surname || controlValidation.firstname || controlValidation.patronymic }">
            <label class="col-md-2 control-label">ПІБ</label>
            <div class="col-md-4">
                <input type="text" class="form-control" v-model="model.surname" placeholder="Введіть прізвище виборця">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" v-model="model.firstname" placeholder="Введіть ім'я' виборця">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" v-model="model.patronymic" placeholder="Введіть по-батькові виборця">
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <p class="help-block" v-if="controlValidation.surname" v-html="controlValidation.surname[0]"></p>
                <p class="help-block" v-if="controlValidation.firstname" v-html="controlValidation.firstname[0]"></p>
                <p class="help-block" v-if="controlValidation.patronymic" v-html="controlValidation.patronymic[0]"></p>
            </div>
        </div>

        <div class="form-group" :class="{ 'has-error': controlValidation.gender_type }">
            <label class="col-md-2 control-label">Стать</label>
            <div class="col-md-3">
                <v-select-control :list="[ { id: 1, value: 'Чоловіча' }, { id: 2, value: 'Жіноча' } ]" v-model="model.gender_type" display="value">
            </div>
        </div>

        <div class="form-group" :class="{ 'has-error': controlValidation.birthday }">
            <label class="col-md-2 control-label">Дата народження</label>
            <div class="col-md-3">
                <datepicker v-model="model.birthday" start-date="1900-01-01" :end-date="-18 | momentDiffYears | momentFormat"></datepicker>
            </div>
        </div>

        <div class="form-group" :class="{ 'has-error': controlValidation.phone_number || controlValidation.additional_phone_number }">
            <label class="col-md-2 control-label">Телефони</label>
            <div class="col-md-5">
                <input type="text" v-model="model.phone_number" class="form-control" placeholder="Основний телефон виборця">
            </div>
            <div class="col-md-5">
                <input type="text" v-model="model.additional_phone_number" class="form-control" placeholder="Додатковий телефон">
            </div>
        </div>

        <h4 class="text-center m-b-20">Додаткові данні</h4>

        <div class="form-group" :class="{ 'has-error': controlValidation.email }">
            <label class="col-md-2 control-label">Email</label>
            <div class="col-md-4">
                <input type="text" class="form-control" v-model="model.email" placeholder="Електронна адреса">
            </div>
        </div>
        <div class="form-group col-md-6" :class="{ 'has-error': controlValidation.education_type }">
            <label class="col-md-4 control-label">Освіта</label>
            <div class="col-md-8">
                <v-select-control :list="educationList" v-model="model.education_type" display="value"></v-select-control>
            </div>
        </div>
        <div class="form-group col-md-6" :class="{ 'has-error': controlValidation.occupation_type }">
            <label class="col-md-4 control-label">Професія</label>
            <div class="col-md-8">
                <v-select-control :list="occupacyList" v-model="model.occupation_type" display="value"></v-select-control>
            </div>
        </div>
        <div class="form-group col-md-6" :class="{ 'has-error': controlValidation.party_id }">
            <label class="col-md-4 control-label">Партіність</label>
            <div class="col-md-8">
                <v-select-control :list="partyList" v-model="model.party_id"></v-select-control>
            </div>
        </div>
        <div class="form-group col-md-6" :class="{ 'has-error': controlValidation.campaign_position }">
            <label class="col-md-4 control-label">Посада у виборчій кампанії</label>
            <div class="col-md-8">
                <v-select-control :list="campaignPositionsList" v-model="model.campaign_position" display="value"></v-select-control>
            </div>
        </div>

        <div class="form-group col-md-6">
            <label class="col-md-4 control-label">Місце роботи</label>
            <div class="col-md-8">
                <v-multiselect :options="workplaces" v-model="workplace" @tag="onUpdateWorkplaces" :multiple="false" :taggable="true" placeholder="Оберіть місце роботи"></v-multiselect>
            </div>
        </div>

        <div class="form-group col-md-6">
            <label class="col-md-4 control-label">Посада</label>
            <div class="col-md-8">
                <v-multiselect :options="workpositions" v-model="workposition" @tag="onUpdateWorkpositions" :multiple="false" :taggable="true" placeholder="Оберіть місце роботи"></v-multiselect>
            </div>
        </div>

        <h4 class="text-center m-b-20">Данні про розташування</h4>
        <div class="form-group" :class="{ 'has-error': controlValidation.address }">
            <label class="col-md-2 control-label">Адреса</label>
            <div class="col-md-8">
                <p class="form-control-static"><span>@{{ psc.locality | prettifyLocality }}</span></p>
                <v-multiselect v-model="address" :options="addressList" label="addressH" placeholder="Оберіть адресу"></v-multiselect>
                <p class="help-block" v-if="controlValidation.address" v-html="controlValidation.address[0]"></p>
                @if (Auth::user()->role_id == App\Models\User::ROLE_OPERATOR)
                <a href="#" @click.prevent="showAddressRequestModal()"><i class="fa fa-plus m-r-10 m-t-10"></i>Додати нову адресу</a>
                @endif
            </div>
        </div>

        <div class="form-group" :class="{ 'has-error': controlValidation.is_newspaper_subscribed }">
            <div class="col-md-offset-2">
                <v-checkbox-control label="Чи отримує виборець газету за цією адресою" :value="true" v-model="model.address.is_newspaper_subscribed"></v-checkbox-control>
            </div>
        </div>

        <hr>

        <div class="row">
            @if (Auth::user()->role_id == App\Models\User::ROLE_ADMINISTRATOR)
            <div class="col-xs-6">
                <button type="button" class="btn btn-default btn-bordered" @click="remove()"><i class="fa fa-trash m-r-10"></i>Видалити</button>
            </div>
            <div class="col-xs-6 text-right">
                <button class="btn btn-success"><i class="fa fa-save m-r-10"></i>Зберегти</button>
            </div>
            @else
            <div class="col-xs-12 text-right">
                <button class="btn btn-success"><i class="fa fa-save m-r-10"></i>Зберегти</button>
            </div>
            @endif
        </div>

    </form>

    @if (Auth::user()->role_id == App\Models\User::ROLE_ADMINISTRATOR)
        <hr>
        <h4 class="text-center">Дії користувачів</h4>
        <table class="table table-condensed" v-if="rolesList.length">
            <thead>
                <tr>
                    <th class="col-md-2 text-center">Дата та час</th>
                    <th class="col-md-2">Користувач</th>
                    <th>Повідомлення</th>
                    <th class="col-md-2"></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in model.activities">
                    <td class="text-center">@{{ item.created_at | prettifyDateTime }}</td>
                    <td>
                        <strong>@{{ item.user.fullname || "Ім'я не вказане" }}</strong>
                        <br>
                        <small class="text-muted">(ID: @{{ item.user.id }} - @{{ rolesList[item.user.role_id].name }})<small>
                    </td>
                    <td>@{{ item.body }}</td>
                    <td class="text-center"><button class="btn btn-default" v-show="!_.isEmpty(item.payload)" @click="showActivityPayloadModal(item)">Додатково...</button></td>
                </tr>
            </tbody>
        </table>
    @endif

    <v-modal id="address-request-modal" size="large">
        <address-creating></address-creating>
    </v-modal>

    <v-modal id="user-activity-modal" size="large">
        <user-activity></user-activity>
    </v-modal>
</div>

<script id="psc-placement-tpl" type="text/x-template">
    <div>
        <h5 class="m-t-0 text-info" v-if="psc.locality">@{{ psc.locality | prettifyLocality }}, @{{ psc.locality.area | prettifyLocality}}, @{{ psc.locality.area.region | prettifyLocality}}
        <br>
        <small class="text-muted">@{{ psc.placement }}</small>
        </h5>
    </div>
</script>

<script id="address-creating-tpl" type="text/x-template">
    <div>
        <form v-blockui="preloaders.form" class="form-horizontal" @submit.prevent="send()">
            <v-modal-header title="Запит на додавання нової адреси"></v-modal-header>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-5">
                        <v-control-group label="Номер ДВК" type="horizontal" :errors="controlValidation.psc_id">
                            <v-multiselect v-model="psc" :options="pscList" label="name" placeholder="Виберіть значення"></v-multiselect>
                        </v-control-group>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <div v-if="psc">
                            <psc-placement :psc="psc"></psc-placement>
                        </div>
                    </div>
                </div>
                <hr class="m-t-0">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <v-control-group label="Поштовий індекс" type="horizontal" :errors="controlValidation.postcode">
                            <v-input-control v-model="form.postcode"></v-input-control>
                        </v-control-group>
                    </div>
                    <div class="col-xs-12 col-md-8">
                        <v-control-group label="Населений пункт" type="horizontal" :errors="controlValidation.locality_id">
                            <v-multiselect v-model="locality" :options="localityList" label="name" placeholder="Виберіть значення"></v-multiselect>
                        </v-control-group>
                    </div>

                    <div class="row"></div>

                        <v-control-group label="Вулиця *" labelSize="2" type="horizontal" :errors="controlValidation.street_id">
                            <v-multiselect v-model="street" :options="streetList | transformStreetNames" label="name_humanly" placehoder="Виберіть значення"></v-multiselect>
                        </v-control-group>


                    <div class="row"></div>

                    <div class="col-xs-12 col-md-4">
                        <v-control-group label="№ б-ку *" type="horizontal" :errors="controlValidation.house_number">
                            <v-input-control v-model="form.house_number"></v-input-control>
                        </v-control-group>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <v-control-group label="Корпус" type="horizontal" :errors="controlValidation.building">
                            <v-input-control v-model="form.building"></v-input-control>
                        </v-control-group>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <v-control-group label="№ кв-ри" type="horizontal" :errors="controlValidation.apartment_number">
                            <v-input-control v-model="form.apartment_number"></v-input-control>
                        </v-control-group>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-bordered"><i class="fa fa-bell m-r-10"></i>Відправити</button>
                </div>
            </div>
        </form>
    </div>
</script>

<script type="text/x-template" id="user-activity-tpl">
<div>
    <div class="row">
        <div class="col-xs-6 text-right"><strong>Поле</strong></div>
        <div class="col-xs-6 text-left"><strong>Значення</strong></div>
    </div>
    <hr>
    <div v-if="activity">
        <div class="row p-10" style="border-bottom:dotted 1px #eee" v-for="item in activity">
            <div class="col-xs-6 text-right text-muted">@{{ item.key }}</div>
            <div class="col-xs-6 text-left">@{{ item.value }}</div>
        </div>
    </div>
</div>
</script>
@endsection

@section('scripts')
    <script>
        var voterData = {!! json_encode($voterData) !!};
    </script>
    <script src="/js/master.voters.edit.vue-bundle.js"></script>
@endsection