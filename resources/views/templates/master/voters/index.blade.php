@extends('layouts.master')

@section('content')
<div id="vue-voters-app" v-blockui="preloaders.form">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <button class="btn btn-custom btn-bordered btn-sm" onclick="document.location = '/voters/add'"><i class="fa fa-plus m-r-10"></i>Додати виборця</button>
            </div>
            <h4 class="header-title m-t-10">База виборців</h4>
        </div>
    </div>
    <div class="m-t-10">
            <voter-search-form @change="onFormChangedHandler"></voter-search-form>
    </div>

    <div class="row">
        <hr class="m-t-20">
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-icon alert-white alert-warning" v-show="!isFormSubmitted">
                <i class="mdi mdi-alert m-r-10"></i>Для пошуку оберіть дільницю та введіть ПІБ
            </div>
        </div>
    </div>

    <div class="row" v-if="searchResults.data">
        <div class="col-xs-12">
            <div class="alert alert-icon alert-white alert-warning" v-show="isFormSubmitted && searchResults.data.length == 0">
                <i class="mdi mdi-alert m-r-10"></i>Нажаль, виборця за даними пошуковими параметрами не виявлено
            </div>
        </div>

        <div class="col-xs-12" v-if="searchResults.data.length > 0">
            <h5 style="width: 50%; float: left; margin-bottom: 30px;">
                <span>Результати пошуку: </span><span class="text-muted">Знайдено @{{ searchResults.total }} результатів</span>
            </h5>
            @if (Auth::user()->isAdministration())
            <div class="pull-right">
                <button type="button" class="btn btn-default" @click="resultsExport()"><i class="fa fa-cloud-download m-r-10"></i>Зберегти результати</button>
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-md-3">Виборець</th>
                        <th class="col-md-4">Адреса</th>
                        <th class="col-md-2 text-center">Номер ДВК</th>
                        <th class="col-md-2 text-center">Анкети</th>
                        <th class="col-md-1"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="voter in searchResults.data">
                        <td>
                            <h4>@{{ voter | toFullname }}</h4>
                            <p>
                                <small><i class="fa fa-envelope m-r-10"></i>@{{ voter.email || 'Не введений' }}</small>
                                <small><i class="fa fa-mobile m-r-10"></i>@{{ voter.phone_number | prettifyPhoneNumber }}</small>
                                <small><i class="fa fa-phone m-r-10"></i>@{{ voter.additional_phone_number | prettifyPhoneNumber }}</small>
                            </p>
                        </td>
                        <td>
                            <span>@{{ voter.postcode }}</span>, <span>@{{ voter | prettifyLocality({ typeField: 'city_type', nameField: 'city_name' }) }}</span>,
                            <span>@{{ voter | prettifyLocality({ typeField: 'region_type', nameField: 'region_name' }) }}</span>,<br>
                            <span>@{{ voter | prettifyAddress(false) }}</span>
                        </td>
                        <td class="text-center"><span class="badge"><big>@{{ voter.psc_name }}</big></span></td>
                        <td class="text-center"><a href="#" @click.prevent="showVoterQuestionnariesModal(voter.id)">0 / 2</a></td>
                        <td class="text-center">
                            <a href="#" class="btn btn-default" @click.prevent="followEditing(voter.id)"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <v-paginator :perpage="searchResults.per_page" v-model="searchResults.current_page" :total="searchResults.total" v-if="searchResults.per_page < searchResults.total" @input="loadPage"></v-paginator>
    </div>

    <v-modal id="voter-questionnaries-modal" size="large">
        <voter-questionnaries></voter-questionnaries>
    </v-modal>

    <v-modal id="export-dialog-modal">
        <export-dialog></export-dialog>
    </v-modal>
</div>

<script id="voter-questionnaries-tpl" type="text/x-template">
    <div>
        <v-modal-header :title="'Опитувачі виборця ' + voter.surname" v-if="voter"></v-modal-header>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-12">
                    <v-control-group label="Имя оператора" :errors="controlValidation.questionnaire_id">
                        <v-select-control :list="qList" v-model="model.questionnaire_id" placeholder="Оберіть опитувач"></v-select-control>
                    </v-control-group>
                </div>
            </div>
            <hr>
            <h4 class="text-center">Відповіді</h4>
            <div class="row">
                <div class="col-xs-12" v-for="(item, key) in answers">
                    <v-answer-option :voter="voter" :index="key + 1" v-model="answers[key]"></v-answer-option>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="voter-search-form-tpl" type="text/x-template">
    <form @submit.prevent="submit()" v-blockui="preloaders.form">
        <div class="row">
            <div class="col-md-2 m-t-10 text-center">
                <a href="#" @click.prevent="resetForm()" id="check-minutes" class="waves-effect waves-light"><i class="fa fa-eraser m-r-10"></i>Очистити</a>
            </div>
            <div class="col-xs-6">
                <input class="form-control" id="single-input" v-model="model.keyword" placeholder="Введіть ПІБ або телефон виборця">
            </div>
            <div class="col-md-2 m-t-10 text-center">
                <a href="#" @click.prevent="toggleFiltersShowing()"><i class="fa fa-filter m-r-10"></i>Показати додаткові фільтри</a>
            </div>
            <div class="col-md-2">
                <button type="submit" id="check-minutes" class="btn form-control waves-effect waves-light btn-primary"><i class="fa fa-search m-r-10"></i>Пошук</button>
            </div>
        </div>

        <transition name="slide">
            <div v-show="isFilterShowed">
            <hr>
                <div class="row m-t-10">
                    <div class="col-md-5">
                        <div class="row">
                            <h5 class="text-center">Фільтрація по опитуванням</h5>
                            <div class="col-xs-12">
                                <v-select-control :list="qList" v-model="model.questionnaire_id" placeholder="Оберіть опитувач"></v-select-control>
                            </div>
                        </div>
                        <table class="table m-t-10 table-hover borderless">
                            <tbody>
                                <tr v-for="(item, index) in opinions">
                                    <td class="col-md-7">
                                        <p class="m-b-5"><small>@{{ item.question }}</small></p>
                                    </td>
                                    <td class="col-md-5">
                                        <v-select-control :list="item.answers" @input="setFilter('opinions', item.id, $event)" :value="_.find(model.filters.opinions, { option_id: item.id }) && _.find(model.filters.opinions, { option_id: item.id }).values" display="value" class="input-sm" placeholder="Відповідь"></v-select-control>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7">
                        <h5 class="text-center">Фільтрація за ДВК</h5>
                        <div class="row m-t-10">
                            <div class="col-md-2">
                                <div class="text-right m-t-5">№ ДВК</div>
                            </div>
                            <div class="col-xs-4"><v-multiselect v-model="psc" :options="pscList" label="name" placeholder="Виберіть значення"></v-multiselect></div>
                        </div>

                        <h5 class="text-center m-t-30">Фільтрація за адресою</h5>
                        <div class="row m-t-10">
                            <div class="col-md-2">
                                <div class="text-right m-t-5">Місто</div>
                            </div>
                            <div class="col-md-4"><input type="text" class="form-control input-sm" v-model="model.locality_name"></div>
                        </div>
                        <div class="row m-t-10">
                            <div class="col-md-2">
                                <div class="text-right m-t-5">Вулиця</div>
                            </div>
                            <div class="col-md-2">
                                <v-select-control v-model="model.street_type" :list="streetTypeList" class="form-control input-sm" placeholder="Тип" display="value"></v-select-control>
                            </div>
                            <div class="col-md-8"><input type="text" v-model="model.street_name" class="form-control input-sm"></div>
                        </div>
                        <div class="row m-t-10">
                            <div class="col-md-2">
                                <div class="text-right m-t-5">Буд.</div>
                            </div>
                            <div class="col-md-2"><input type="text" v-model="model.house_number" class="form-control input-sm"></div>
                            <div class="col-md-2">
                                <div class="text-right m-t-5">Кв.</div>
                            </div>
                            <div class="col-md-2"><input type="text" v-model="model.apartment_number" class="form-control input-sm"></div>
                            <div class="col-md-2">
                                <div class="text-right m-t-5">Корп.</div>
                            </div>
                            <div class="col-md-2"><input type="text" v-model="model.building" class="form-control input-sm"></div>
                        </div>
                        <div class="row m-t-10">
                            <div class="col-md-offset-2 col-md-10">
                                <v-checkbox-control label="Чи є підписка на газету?" v-model="model.is_newspaper_subscribed"></v-checkbox-control>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </transition>
    </form>
</script>

<script id="answer-option-tpl" type="text/x-template">
<div>
    <h6 class="text-primary"><span>@{{ index }}. </span>@{{ value.question}}</h6>
    <p v-if="!isEditMode">
        <span>@{{ value.answer_value || 'Відповідь не надана'}}</span>
        <button type="button" class="btn btn-default btn-xs" @click="toggleEditMode()"><i class="fa fa-edit"></i></button>
    </p>
    <div v-else>
        <div class="row">
            <div class="col-md-10">
                <v-multiselect v-model="answer" :options="list" label="value" v-if="value.group_type == 2"></v-multiselect>
                <input type="text" class="form-control" placeholder="Введіть довільну відповідь" v-model="answer.value" v-if="value.group_type == 1">
            </div>
            <div class="col-md-2 text-center">

                <div class="input-group">
                    <span class="input-group-btn text-right"><button type="button" class="btn btn-default" style="height: 38px" @click="toggleEditMode()"><i class="fa fa-times"></i></button></span>
                    <span class="input-group-btn text-left"><button type="button" class="btn btn-success" style="height: 38px" @click="save()"><i class="fa fa-save"></i></button></span>
                </div>
            </div>
        </div>
    </div>
</div>
</script>

<script id="export-dialog-tpl" type="text/x-template">
<form @submit.prevent="save()">
    <v-modal-header title="Експорт результатів"></v-modal-header>
    <div class="modal-body" v-blockui="preloaders.form">
        <div class="row">
            <div class="form-group" :class="{ 'has-error': controlValidation.questionnaire_id }">
                <div class="col-md-4">
                    <label class="control-label">Оберіть опитувач</label>
                    <p class="text-muted"><small>Результати опитування будуть прикріплені до кожного виборця</small></p>
                </div>
                <div class="col-md-8">
                    <v-select-control :list="qList" v-model="form.questionnaire_id" display="name"></v-select-control>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-primary btn-bordered">Зберегти</button>
        </div>
    </div>
</form>
</script>
@endsection

@section('scripts')
<script src="/js/master.voters.index.vue-bundle.js"></script>
@endsection