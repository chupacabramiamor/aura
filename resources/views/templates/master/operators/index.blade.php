@extends('layouts.master')

@section('content')
<div id="vue-operators-app">
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right">
                <button class="btn btn-custom btn-bordered btn-sm" @click="showCreatingModal()">Добавить оператора</button>
            </div>
            <h4 class="header-title m-t-10">Список пользователей</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class="table m-0">
                <thead>
                    <tr>
                        <th class="col-xs-5">Имя оператора</th>
                        <th class="col-xs-3">Логин</th>
                        <th class="col-xs-2 text-center">Активный</th>
                        <th class="col-xs-2"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item in list">
                        <td><p class="m-t-10 m-b-5">@{{ item.fullname | pretifyFullname }}</p></td>
                        <td><p class="m-t-10 m-b-5">@{{ item.login }}</p></td>
                        <td class="text-center"><p class="m-t-10 m-b-5" :class="{'text-danger': item.is_enabled == 0, 'text-success': item.is_enabled == 1}">@{{ item.is_enabled | pretifyEnabled }}</p></td>
                        <td class="text-center">
                            <button class="btn btn-warning btn-bordered btn-sm" @click="showEditingModal(item.id)">Изменить</button>
                            <button class="btn btn-danger btn-bordered btn-sm" @click="remove(item.id)">Удалить</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <v-modal id="operator-creating-modal" size="large">
        <v-creating-form></v-creating-form>
    </v-modal>


    <v-modal id="operator-editing-modal" size="large">
        <v-editing-form></v-editing-form>
    </v-modal>
</div>

<script type="text/x-template" id="creating-form-tpl">
    <form @submit.prevent="save()" v-blockui="preloaders.form" class="form-horizontal">
        <v-modal-header title="Добавление нового оператора"></v-modal-header>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-7">
                    <v-control-group label="Имя оператора" :errors="controlValidation.fullname">
                        <v-input-control v-model="model.fullname"></v-input-control>
                    </v-control-group>
                </div>
                <div class="col-xs-5">
                    <v-checkbox-control label="Активный" :value="true" v-model="model.is_enabled"></v-checkbox-control>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <v-control-group label="Логин" :errors="controlValidation.login">
                        <v-input-control v-model="model.login"></v-input-control>
                    </v-control-group>
                </div>
                <div class="col-xs-6">
                    <v-control-group label="Email" :errors="controlValidation.email">
                        <v-input-control v-model="model.email"></v-input-control>
                    </v-control-group>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <v-control-group label="Пароль" :errors="controlValidation.password">
                        <v-input-control v-model="model.password" type="password"></v-input-control>
                    </v-control-group>
                </div>
                <div class="col-xs-6">
                    <v-control-group label="Подтверждение пароля" :errors="controlValidation.password_confirmation">
                        <v-input-control v-model="model.password_confirmation" type="password"></v-input-control>
                    </v-control-group>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <psc-list :list="pscList" v-model="model.pscs"></psc-list>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-bordered">Сохранить</button>
            </div>
        </div>
    </form>
</script>

<script id="psc-list-tpl" type="text/x-template">
<div>
    <h5 class="text-center">Доступные пользователю участки</h5>
    <div class="row m-b-20"><div class="col-xs-12 text-center"><a href="#" @click.prevent="toggleSelectAll()">@{{ isAllSelected ? 'Зняти всі' : 'Обрати всі' }}</a></div></div>
    <div style="height: 300px" class="slimscroll">
        <div class="row" v-for="row in pscsGrouped">
            <div class="col-md-3" v-for="psc in row">
                <v-checkbox-control :label="psc.name" :value="psc.id" v-model="pscs" @change="onChange(pscs)"></v-checkbox-control>
                <p class="text-muted" style="margin-left:25px; font-size: 0.7em">@{{ psc.placement }}</p>
            </div>
        </div>
    </div>
</div>
</script>
@endsection

@section('scripts')
    <script>
        var operatorList = {!! $operatorList->toJson() !!};
        var pscList  = {!! $pscList->toJson() !!};
    </script>

    <script src="/js/master.operators.index.vue-bundle.js"></script>
@endsection