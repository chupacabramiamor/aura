@extends('layouts.signin')

@section('content')
<div class="row">
    <div class="col-sm-12">

        <div class="wrapper-page">

            <div class="m-t-40 card-box" id="vue-signin-login-app" v-blockui="preloaders.form">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Вход в систему</h4>
                </div>
                <div class="account-content">
                    <form class="form-horizontal" @submit.prevent="submit">
                        <div class="col-xs-12">
                            <v-control-group name="login" label="Ваш логин" :errors="controlValidation.login">
                                <v-input-control v-model="model.login" placeholder="Логин"></v-input-control>
                            </v-control-group>
                        </div>

                        <div class="col-xs-12">
                            <v-control-group name="login" label="Пароль" :errors="controlValidation.password">
                                <input class="form-control" type="password" v-model="model.password" placeholder="Введите свой пароль">
                            </v-control-group>
                        </div>


                        <div class="form-group account-btn text-center m-t-10">
                            <div class="col-xs-12">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
                            </div>
                        </div>

                    </form>

                    <div class="clearfix"></div>

                </div>
            </div>
            <!-- end card-box-->

        </div>
        <!-- end wrapper -->

    </div>
</div>
@endsection

@section('scripts')
    <script src="/js/signin.login.vue-bundle.js"></script>
@endsection