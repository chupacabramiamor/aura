var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

function VueService(url) {
	this.url = url;
}

VueService.prototype.url = null;
VueService.prototype.params = {};
VueService.prototype.data = {};
VueService.prototype.isFetched = false;

VueService.prototype.fetch = function(params, forceLoad) {
	return new Promise(function(resolve, reject) {
		if (_.isEqual(this.params, params) && this.isFetched && !forceLoad) {
			return resolve(this.data);
		}

		this.params = params;

		return Vue.http.get(this.url, { params: params}).then(function(response) {
			this.isFetched = true;
			this.data = response.data;
			return resolve(response.data);
		}.bind(this));
	}.bind(this));
};


VueService.prototype.get = function() {
	return this.data;
};

module.exports = VueService;