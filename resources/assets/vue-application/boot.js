var Vue = require('vue');
var VueResource = require('vue-resource');
var VueMultiselectComponent = require('vue-multiselect');
var errorResponsesInterceptor = require('./interceptors/error-responses.js');
var controlValidateDirective = require('./directives/control-validate.js');
var blockuiDirective = require('./directives/blockui.js');
var controlGroupComponent = require('./components/control-group.js');
var inputControlComponent = require('./components/input-control.js');
var checkboxControlComponent = require('./components/checkbox-control.js');
var selectControlComponent = require('./components/select-control.js');
var textareaControlComponent = require('./components/textarea-control.js');
var fileuploaderControlComponent = require('./components/fileuploader-control.js');
var moment = require('moment');

window.Localization = require('./Localization.js');

require('moment/locale/ru.js');
moment.locale('ru');

Localization.register('uk-ua', {
    title: 'UA',
    caption: 'Українська'
});

Localization.add('uk-ua', require('./locales/uk-ua/ui.json'));
Localization.add('uk-ua', require('./locales/uk-ua/errors.json'));

Localization.setCurrent('uk-ua');

Vue.use(VueResource);

Vue.http.interceptors.push(errorResponsesInterceptor);

Vue.directive('control-validate', controlValidateDirective);
Vue.directive('blockui', blockuiDirective);

Vue.component('v-control-group', controlGroupComponent);
Vue.component('v-input-control', inputControlComponent);
Vue.component('v-checkbox-control', checkboxControlComponent);
Vue.component('v-select-control', selectControlComponent);
Vue.component('v-textarea-control', textareaControlComponent);
Vue.component('v-fileuploader-control', fileuploaderControlComponent);
Vue.component('v-multiselect', VueMultiselectComponent.default);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('content');

module.exports.Localization = Localization;