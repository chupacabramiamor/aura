module.exports = {
	bind: function(el, binding, vnode) {
		var _model = _.find(vnode.data.directives, { name: 'model' });
		vnode.context.$watch(_model.expression, function(key) {
			vnode.context.controlValidation[_model.expression] = false;
		});
	},

	update: function(el, binding, vnode) {
		var _model = _.find(vnode.data.directives, { name: 'model' });

		if(vnode.context.controlValidation[_model.expression]) {
			$(el).parents('.form-group').addClass('has-error');
		} else $(el).parents('.form-group').removeClass('has-error');
	}
};