module.exports = {
	update: function(el, binding, vnode) {
		if(binding.value) {
			$(el).block({
				message: '',
				css: {
					position: 'relative',
					border: 'none',
					backgroundColor: 'none',
					zIndex: 9999
				},
				overlayCSS: {
					backgroundColor: '#fff',
					backgroundImage: 'url("/images/preloader.svg")',
					backgroundRepeat: 'no-repeat',
					backgroundPosition: '50%',
					zIndex: 9998
				}
			});
		} else $(el).unblock();
	}
};