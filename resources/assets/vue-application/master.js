var Vue = require('vue');
var VueResource = require('vue-resource');
var vBlockUIMixin = require('./mixins/blockui.js');

Vue.use(VueResource);

const TYPE_NEW_ADDR_REQUEST = 10;
const TYPE_NEW_ADDR_RESPONSE = 11;

var notifyListComponent = {
    template: '#notify-list-component-tpl',

    data: function() {
        return {
            list: []
        };
    },

    mounted: function() {
    	this.$http.get('/notifies/data').then(function(response) {
			this.list = response.data;
			this.$emit('change', this.list);
    	}.bind(this));
    },

    methods: {
    	markAsOpened: function(id) {
    		this.$http.get('/notifies/mark-as-opened/' + id).then(function (response) {
    			var _index = _.findIndex(this.list, {id: id});
    			this.list[_index].is_opened = 1;

    			this.$emit('change', this.list);
                this.$root.$emit('notifyOpened', this.list[_index]);
    		}.bind(this));
    	}
    }
};

var notifyTrayComponent = {
    template: '#notify-tray-component-tpl',

    data: function() {
    	return {
    		amount: 0
    	};
    },

    components: {
    	'notify-list': notifyListComponent
    },

    methods: {
    	refreshAmount: function(data) {
    		this.amount = data.filter(function(item) {
    			return item.is_opened == 0;
    		}).length;
    	},
    }
};

var vueNotifyApp = new Vue({
    el: '#vue-notifies-app',
    components: {
    	'notify-tray': notifyTrayComponent
    },

    mounted: function() {
        this.$on('notifyOpened', function(payload) {
            if (payload.type == TYPE_NEW_ADDR_REQUEST) {
                document.location = '/addresses/add?notify=' + payload.id;
            }
        });
    }
});