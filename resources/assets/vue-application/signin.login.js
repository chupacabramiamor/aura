require('./boot.js');

var Vue = require('vue');
var VueResource = require('vue-resource');
var vValidateMixin = require('./mixins/validate.js');
var vBlockUIMixin = require('./mixins/blockui.js');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('content');

new Vue({
    el: '#vue-signin-login-app',

    mixins: [ vValidateMixin, vBlockUIMixin ],

    data: {
        model: {
        }
    },

    methods: {
        submit: function() {
            this.setPreloader('form');

            var data = {
                login: this.model.login,
                password: this.model.password
            };

            this.$http.post('/auth/login', data).then(function(response) {
                document.location = response.data.url;
            }, this.unsetPreloader.call(this, 'form'));
        }
    }
});