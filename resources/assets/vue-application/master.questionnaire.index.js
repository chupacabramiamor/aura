require('./boot.js');

var vueElement = '#vue-questionnaire-app';

var Vue = require('vue');
var VueResource = require('vue-resource');
var vValidateMixin = require('./mixins/validate.js');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var modalComponent = require('./components/modal.js');

Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('content');

var questionnaireCreatingFormComponent = {
    template: '#questionnaire-creating-form-tpl',

    mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],

    data: function() {
        return {
            model: {},
            controlValidation: [],
            preloaders: {},
        };
    },

    components: {
        'v-modal-header': modalComponent.modalHeader,
    },

    created: function() {
        this.RESOURCE_BASE_URL = '/resources/questionnaires';
    },

    mounted: function() {
        this.$root.$on('questionnaireCreating', function() {
            this.$root.bsModal = 'questionnaire-creating-modal';
            this.markAllValidated();
            this.model = {};
        }.bind(this));
    },

    methods: {
        save: function() {
            var data = {
                name: this.model.name
            };

            this.setPreloader('form');

            this.$http.post(this.RESOURCE_BASE_URL, data).then(function(response) {
                this.unsetPreloader('form');
                this.$root.bsModal = false;
                this.$root.$emit('questionnaireCreated', { id: response.data.id });
                this.alert('Новый опросник успешно добавлен.');
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        }
    }
};

var questionnaireEditingFormComponent = {
    template: '#questionnaire-editing-form-tpl',

    mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],

    data: function() {
        return {
            model: {},
            controlValidation: [],
            preloaders: {},
        };
    },

    components: {
        'v-modal-header': modalComponent.modalHeader,
    },

    created: function() {
        this.RESOURCE_BASE_URL = '/resources/questionnaires';
    },

    mounted: function () {
        this.$root.$on('questionnaireUpdating', function(payload) {
            this.$root.bsModal = 'questionnaire-editing-modal';
            this.markAllValidated();
            this.setPreloader('form');

            this.$http.get(this.RESOURCE_BASE_URL + '/' + payload.id).then(function(response) {
                this.model = response.data;
                this.unsetPreloader('form');
            }.bind(this));
        }.bind(this));
    },

    methods: {
        save: function() {
            var data = {
                name: this.model.name
            };

            this.$http.put(this.RESOURCE_BASE_URL + '/' + this.model.id, data).then(function(response) {
                this.unsetPreloader('form');
                this.$root.bsModal = false;
                this.$root.$emit('questionnaireUpdated', { id: this.model.id });
                this.alert('Изменения успешно сохранены');
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        }
    }
};

/**
 * [inquiryCreatingFormComponent description]
 * @type {Object}
 */
var inquiryCreatingFormComponent = {
    template: '#inquiry-creating-form-tpl',

    mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],

    components: {
        'v-modal-header': modalComponent.modalHeader,
    },

    data: function() {
        return {
            model: {},
            groupList: [],
            preloaders: {},
            controlValidation: [],
        };
    },

    mounted: function() {
        this.$root.$on('inquiryCreating', function(payload) {
            // Проверка на наличие групп ответов
            this.$http.get('/resources/answer-groups').then(function(response) {
                if (response.data.length > 0) {
                    this.$root.$emit('bsModalShow', {
                        id: 'inquiry-creating-modal'
                    });

                    this.groupList = response.data;

                    this.markAllValidated();
                    this.model = {
                        questionnaire_id: payload.questionnaire_id
                    };
                } else this.alert('answer_groups_is_not_defined');
            }.bind(this));
        }.bind(this));
    },

    methods: {
        save: function() {
            this.setPreloader('form');

            var data = {
                question: this.model.question,
                group_id: this.model.group_id,
                questionnaire_id: this.model.questionnaire_id,
            };

            this.$http.post('/resources/inquiries', data).then(function(response) {
                this.$root.$emit('inquiryCreated', response.data);
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        }
    }
};


/**
 * [inquiryEditingFormComponent description]
 * @type {Object}
 */
var inquiryEditingFormComponent = {
    template: '#inquiry-editing-form-tpl',

    mixins: [ vBlockUIMixin, vSwalMixin ],

    components: {
        'v-modal-header': modalComponent.modalHeader,
    },

    data: function() {
        return {
            model: {},
            preloaders: {},
            controlValidation: {},
            groupList: []
        };
    },

    mounted: function() {
        this.$http.get('/resources/answer-groups').then(function(response) {
            this.groupList = response.data;
        }.bind(this));

        this.$root.$on('inquiryUpdating', function (payload) {
            this.$http.get('/resources/inquiries/' + payload.id).then(function(response) {
                this.model = response.data;
            }.bind(this));
        }.bind(this));
    },

    methods: {
        save: function() {
            this.setPreloader('form');

            var data = {
                question: this.model.question
            };

            this.$http.put('/resources/inquiries/' + this.model.id, data).then(function(response) {
                this.$root.$emit('inquiryUpdated', response.data);
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        },

        remove: function(id) {
            this.confirm('Удаление вопроса').then(function() {
                this.$http.delete('/resources/inquiries/' + id).then(function() {
                    this.$root.$emit('inquiryDeleted', {id: id});
                }.bind(this));
            }.bind(this));
        },
    }
};

var inquiryListComponent = {
    template: '#inquiry-list-tpl',

    mixins: [ vBlockUIMixin, vSwalMixin ],

    props: [ 'questionnaire_id', 'isExpanded' ],

    data: function() {
        return {
            list: [],
            preloaders: {},
        };
    },

    components: {
        'v-modal': modalComponent.modal,
    },

    created: function() {
        this.RESOURCE_BASE_URL = '/resources/questionnaires';
    },

    mounted: function() {
        var params = { questionnaire_id: this.questionnaire_id };
        this.$http.get('/resources/inquiries', { params: params }).then(function(response) {
            this.list = response.data;
        }.bind(this));

        this.$root.$on('inquiryCreated', function(payload) {
            this.$http.get('/resources/inquiries/' + payload.id).then(function(response) {
                this.list.push(response.data);
                this.$emit('change', this.list);
            }.bind(this));
        }.bind(this));

        this.$root.$on('inquiryUpdated', function(payload) {
            var _index = _.findIndex(this.list, { id: payload.id });
            this.$set(this.list, _index, payload);
        }.bind(this));

        this.$root.$on('inquiryDeleted', function(payload) {
            var _index = _.findIndex(this.list, { id: payload.id });
            this.list.splice(_index, 1);
        }.bind(this));
    },

    methods: {
        edit: function(id) {
            this.$root.$emit('inquiryUpdating', {
                id: id
            });
        }
    }
};

var questionnaireListComponent = {
    template: '#questionnaire-list-tpl',

    mixins: [ vBlockUIMixin, vSwalMixin ],

    data: function() {
        return {
            list: window.questionnaireList || [],
            active_id: false,
            preloaders: {},
        };
    },

    components: {
        'v-modal': modalComponent.modal,
        'v-inquiry-list': inquiryListComponent
    },

    created: function() {
        this.RESOURCE_BASE_URL = '/resources/questionnaires';
    },

    mounted: function() {
        this.$root.$on('questionnaireCreated', function(payload) {
            this.setPreloader('list');
            this.$http.get(this.RESOURCE_BASE_URL + '/' + payload.id).then(function(response) {
                this.unsetPreloader('list');
                this.list.push(response.data);
            }.bind(this));
        }.bind(this));

        this.$root.$on('questionnaireUpdated', function(payload) {
            var _index = _.findIndex(this.list, { id: payload.id });
            this.$http.get(this.RESOURCE_BASE_URL + '/' + payload.id).then(function(response) {
                this.$set(this.list, _index, response.data);
            }.bind(this));
        }.bind(this));

        this.$root.$on('inquiryCreated', function(payload) {
            this.active_id = payload.questionnaire_id;
        }.bind(this));
    },

    methods: {
        toggle: function(id) {
            this.active_id = (this.active_id == id) ? false : id;
        },

        showQuestionnaireEditingModal: function(id) {
            this.$root.$emit('questionnaireUpdating', {
                id: id
            });
        },

        showInquiryCreatingModal: function(questionnaire_id) {
            this.$root.$emit('inquiryCreating', {
                questionnaire_id: questionnaire_id
            });
        },

        showQuestionnaireRemovingConfirm: function(id) {
            this.confirm('Удаление опросника').then(function() {
                this.setPreloader('list');
                this.$http.delete(this.RESOURCE_BASE_URL + '/' + id).then(function(response) {
                    this.unsetPreloader('list');
                    var _index = _.findIndex(this.list, { id: id });
                    this.list.splice(_index, 1);
                }, this.unsetPreloader.call(this, 'list'));
            }.bind(this));
        },

        updateItem: function($payload, questionnaire_id) {
            var _index = _.findIndex(this.list, { id: questionnaire_id });

            this.list[_index].inquires_amount = $payload.length;
        }
    }
};

if (document.querySelector(vueElement)) {
    new Vue({
        el: vueElement,

        mixins: [ vValidateMixin, vBlockUIMixin ],

        data: function() {
            return {
                bsModal: false
            };
        },

        components: {
            'v-modal': modalComponent.modal,
            'v-modal-header': modalComponent.modalHeader,
            'v-questionnaire-list': questionnaireListComponent,
            'v-questionnaire-creating-form': questionnaireCreatingFormComponent,
            'v-questionnaire-editing-form': questionnaireEditingFormComponent,
            'v-inquiry-creating-form': inquiryCreatingFormComponent,
            'v-inquiry-editing-form': inquiryEditingFormComponent,
        },

        mounted: function() {
            this.$on('inquiryCreated', function() {
                this.$emit('bsModalHide', {
                    id: 'inquiry-creating-modal'
                });
            });

            this.$on('inquiryUpdating', function() {
                this.$emit('bsModalShow', {
                    id: 'inquiry-editing-modal'
                });
            });

            this.$on('inquiryUpdated', function() {
                this.$emit('bsModalHide', {
                    id: 'inquiry-editing-modal'
                });
            });

            this.$on('inquiryDeleted', function() {
                this.$emit('bsModalHide', {
                    id: 'inquiry-editing-modal'
                });
            });
        },

        methods: {
            showQuestionnaireCreatingModal: function() {
                this.$emit('questionnaireCreating', {});
            },
        }
    });
}
