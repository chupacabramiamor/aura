require('./boot.js');

var Vue = require('vue');
var VueResource = require('vue-resource');

// Mixins
var vValidateMixin = require('./mixins/validate.js');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var vLocalityMixin = require('./mixins/locality.js');

// Services
var pscsSvc = require('./services/pscs.js');
var questionnairesSvc = require('./services/questionnaires.js');
var answersSvc = require('./services/answers.js');
var streetTypesSvc = require('./services/street-types.js');

// Components
var modalComponent = require('./components/modal.js');
var paginator = require('./components/paginator.js');

Vue.component('v-modal', modalComponent.modal);
Vue.component('v-modal-header', modalComponent.modalHeader);
Vue.component('v-paginator', paginator);

var QuestionnairesPromise = questionnairesSvc.fetch();

var ExportDialogComponent = {
    template: '#export-dialog-tpl',

    mixins: [ vValidateMixin, vBlockUIMixin, vSwalMixin ],

    data: function() {
        return {
            form: {},
            qList: [],
            searchParams: {}
        };
    },

    mounted: function() {
        QuestionnairesPromise.then(function(data) {
            this.qList = data;
        }.bind(this));

        this.$root.$on('exportDialogShowing', this.onExportDialogShowing);
        this.$root.$on('exportDialogHiding', this.onExportDialogHiding);
    },

    methods: {
        onExportDialogShowing: function(payload) {
            this.searchParams = JSON.parse(JSON.stringify(payload));

            this.$root.$emit('bsModalShow', {
                id: 'export-dialog-modal'
            });
        },

        onExportDialogHiding: function() {
            this.$root.$emit('bsModalHide', {id: 'export-dialog-modal'});

            this.alert('Завантаження розпочато', 'success');
        },

        save: function() {
            this.setPreloader('form');

            var data = this.searchParams;

            if (this.form.questionnaire_id) {
                data.questionnaire_id = this.form.questionnaire_id;
            }

            this.$http.post('/voters/export', data).then(function(response) {
                this.unsetPreloader('form');

                window.open(response.data.download, '_blank');

                this.$root.$emit('exportDialogHiding');
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        }
    }
};

var AnswerOptionComponent = {
    template: '#answer-option-tpl',

    mixins: [ vValidateMixin, vBlockUIMixin ],

    data: function() {
        return {
            isEditMode: false,
            answer: {},
            list: [],
        };
    },

    props: {
        value: Object,
        index: Number,
        voter: {
            type: Object,
            required: true
        }
    },

    methods: {
        toggleEditMode: function() {
            this.isEditMode = Boolean(1 - this.isEditMode);

            if (this.isEditMode) {
                answersSvc.fetch({ group_id: this.value.group_id }).then(function(data) {
                    this.list = data;

                    if (this.value.answer_id) {
                        this.answer = _.find(this.list, { id: this.value.answer_id });
                    }
                }.bind(this));
            }
        },

        save: function() {
            var data = {
                value: this.answer.value
            };

            this.$http.post('/voters/answer/' + this.value.id + '/' + this.voter.id, data).then(function(response) {
                this.toggleEditMode();
                this.$emit('input', _.extend(this.value, {
                    answer_id: response.data.id,
                    answer_value: response.data.value
                }));
            }.bind(this));

        }
    }
};

var voterSearchFormComponent = {
    template: '#voter-search-form-tpl',

    mixins: [ vValidateMixin, vBlockUIMixin ],

    data: function() {
        return {
            model: {
                psc_id: this.psc_id,
                keyword: '',
                questionnaire_id: null,
                filters: {
                    opinions: []
                }
            },
            pscList: [],
            qList: [],
            answerList: [],
            streetTypeList: [],
            isFilterShowed: false
        };
    },

    props: {
        psc_id: {
            default: 0,
        }
    },

    mounted: function() {
        this.setPreloader('form');

        this.$root.$on('getSearch', function(payload) {
            this.submit(payload);
        }.bind(this));

        var PscsPromise = pscsSvc.fetch().then(function(data) {
            this.pscList = data;
        }.bind(this));

        QuestionnairesPromise.then(function(data) {
            this.qList = data;
        }.bind(this));

        var AnswersPromise = answersSvc.fetch().then(function(data) {
            this.answerList = data;
        }.bind(this));

        var StreetTypesPromise = streetTypesSvc.fetch().then(function(data) {
            this.streetTypeList = data;
        }.bind(this));

        Promise.all([ PscsPromise, QuestionnairesPromise, AnswersPromise, StreetTypesPromise]).then(function() {
            this.unsetPreloader('form');
        }.bind(this));
    },


    methods: {
        submit: function(data) {
            this.setPreloader('form');

            data = data || {};

            if (this.model.psc_id) {
                data.psc_id = this.model.psc_id;
            }

            if (this.model.keyword) {
                data.keyword = this.model.keyword;
            }

            if (this.model.locality_name) {
                data.locality_name = this.model.locality_name;
            }

            if (this.model.street_name) {
                data.street_name = this.model.street_name;
            }

            if (this.model.house_number) {
                data.house_number = this.model.house_number;
            }

            if (this.model.apartment_number) {
                data.apartment_number = this.model.apartment_number;
            }

            if (this.model.building) {
                data.building = this.model.building;
            }

            if (this.model.is_newspaper_subscribed) {
                data.is_newspaper_subscribed = this.model.is_newspaper_subscribed;
            }

            for (var name in this.model.filters) {
                if (this.model.filters[name].length > 0) {
                    if (!data.filters) {
                        data.filters = {};
                    }

                    data.filters[name] = this.model.filters[name].filter(function(item) {
                        return Boolean(item.values);
                    }).map(function(item) {
                        item.values = [ item.values ];
                        return item;
                    });

                    if (data.filters[name].length == 0) {
                        delete data.filters[name];
                    }

                    if (_.isEmpty(data.filters)) {
                        delete data.filters;
                    }
                }
            }

            this.$http.post('/voters/search', data).then(function(response) {
                this.$emit('change', {
                    results: response.data,
                    params: data
                });
                this.unsetPreloader('form');
            }.bind(this), this.unsetPreloader.call(this, 'form'));

        },

        setFilter: function(filterName, option_id, value) {
            this.model.filters[filterName].unshift({
                option_id: option_id,
                values: value
            });

            this.model.filters[filterName] = _.uniqBy(this.model.filters[filterName], 'option_id');
        },

        resetForm: function() {
            this.model.psc_id = this.$props.psc_id;
            this.model.keyword = '';
            this.resetFiltration();
        },

        resetFiltration: function () {
            this.model.filters = {
                opinions: []
            };
        },

        getQueryParams: function() {
            return {
                keyword: this.model.keyword,
                psc_id: this.model.psc_id
            };
        },

        toggleFiltersShowing: function() {
            this.isFilterShowed = Boolean(1 - this.isFilterShowed);
        }
    },

    computed: {
        psc: {
            get: function() {
                var _index = _.findIndex(this.pscList, { id: this.model.psc_id });
                return this.pscList[_index];
            },
            set: function(value) {
                this.$set(this.model, 'psc_id', value ? value.id : 0);
                this.$emit('input', this.getQueryParams());
            }
        },

        opinions: function() {
            if (!this.model.questionnaire_id) {
                return [];
            }

            if (parseInt(this.model.questionnaire_id) < 1) {
                return [];
            }

            return _.find(this.qList, { id: parseInt(this.model.questionnaire_id) }).inquiries.map(function(item) {
                item.answers = _.filter(this.answerList, { group_id: item.group_id });
                return item;
            }.bind(this));
        }
    },

    watch: {
        isFilterShowed: function(value) {
            if (!value) {
                this.resetFiltration();
            }
        }
    }
};

var voterQuestionnariesComponent = {
    template: '#voter-questionnaries-tpl',

    mixins: [ vBlockUIMixin, vValidateMixin ],

    data: function() {
        return {
            model: {},
            qList: [],
            answers: [],
            voter: null,
        };
    },

    components: {
        'v-answer-option': AnswerOptionComponent
    },

    mounted: function() {
        questionnairesSvc.fetch().then(function(data) {
            this.qList = data;
        }.bind(this));

        this.$root.$on('voterQuestionnariesShowing', function(payload) {
            this.$root.$emit('bsModalShow', {
                id: 'voter-questionnaries-modal'
            });

            this.voter = payload;
        }.bind(this));
    },

    watch: {
        'model.questionnaire_id': function(value) {
            this.$http.get('/voters/answers/' + value + '/' + this.voter.id).then(function(response) {
                this.answers = response.data;
            }.bind(this));
        }
    }
};


new Vue({
    el: '#vue-voters-app',

    mixins: [ vLocalityMixin, vBlockUIMixin ],

    components: {
        'voter-search-form': voterSearchFormComponent,
        'voter-questionnaries': voterQuestionnariesComponent,
        'export-dialog': ExportDialogComponent
    },

    data: {
        isFormSubmitted: false,
        searchResults: {},
        searchParams: {}
    },

    methods: {
        onFormChangedHandler: function(payload) {
            this.isFormSubmitted = true;
            this.searchResults = payload.results;
            this.searchParams = payload.params;
        },

        onFormInputChangeHandler: function() {

        },

        showVoterQuestionnariesModal: function(voter_id) {
            var voter = _.find(this.searchResults.data, {id: voter_id});
            this.$emit('voterQuestionnariesShowing', voter);
        },

        followEditing: function(id) {
            document.location.href = '/voters/edit/' + id;
        },

        loadPage: function(page) {
            this.$emit('getSearch', { page: page });
        },

        resultsExport: function() {
            this.$emit('exportDialogShowing', this.searchParams);
        }
    },

    filters: {
        toFullname: function(value) {
            var surname = value.surname;
            var firstname = value.firstname && value.firstname.slice(0, 1) + '.';
            var patronim = value.patronymic && value.patronymic.slice(0, 1) + '.';

            return [ surname, firstname, patronim ].join(' ');
        },

        prettifyPhoneNumber: function(value) {
            return value ? '+' + value : 'Не задано';
        }
    }
});