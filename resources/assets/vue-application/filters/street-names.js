var streetTypesVueService = require('./../services/street-types.js');

module.exports = function (values) {
    var streetTypeList = streetTypesVueService.get();

    return values.map(function(item) {
        var streetType = _.find(streetTypeList, {id: item.type});

        return _.extend(item, {
            name_humanly: streetType.value.toLowerCase() + ' ' + item.name
        });
    });
};