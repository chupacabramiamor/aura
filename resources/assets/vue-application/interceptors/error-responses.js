module.exports = function (request, next) {
	next(function(response) {
		if (response.status >= 400) {
			if (response.headers.get('Content-Type') != 'application/json') {
				alert('Виникла непередбачувана помилка');
			}

			if (response.status == 401) {
				swal(Localization.translate('not_authorized')).then(function() {
					document.location = '/';
				});
			}

			if (response.data.message) {
				if (response.status == 422) return;

				swal(Localization.translate(response.data.message));
			}
		}
	});
};