var vValidateMixin = require('./../mixins/validate.js');
var vBlockUIMixin = require('./../mixins/blockui.js');

var streetTypesVueService = require('./../services/street-types.js');
var streetVueService = require('./../services/streets.js');

module.exports = {
    template: '#street-form-tpl',
    mixins: [ vValidateMixin, vBlockUIMixin ],
    data: function() {
        return {
            form: {},
            localityList: localityList,
            streetTypeList: [],
            streetList: []
        };
    },

    mounted: function() {
        streetTypesVueService.fetch().then(function(data) {
            this.streetTypeList = data;
        }.bind(this));
    },

    methods: {
        save: function() {
            var data = {
                locality_id: this.form.locality_id,
                name: this.form.name,
                type: this.form.type,
            };

            if (this.form.prev_id) {
                data.prev_id = this.form.prev_id;
            }

            this.setPreloader('form');

            this.$http.post('/streets', data).then(function(response) {
                this.unsetPreloader('form');
                this.$root.$emit('streetCreated', response.data);
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        }
    },

    computed: {
        locality: {
            get: function() {
                var _index = _.findIndex(this.localityList, { id: this.form.locality_id });

                if (_index > -1) {
                    this.setPreloader('form');
                    streetVueService.fetch({ locality_id: this.form.locality_id }).then(function(data) {
                        this.streetList = data;
                        this.unsetPreloader('form');
                    }.bind(this));
                }

                return this.localityList[_index];
            },

            set: function(value) {
                this.$set(this.form, 'locality_id', value.id);
            },
        },

        prevStreet: {
            get: function() {
                var _index = _.findIndex(this.streetList, { id: this.form.prev_id });

                return this.streetList[_index];
            },

            set: function(value) {
                this.$set(this.form, 'prev_id', value ? value.id : null);
            },
        }
    },
};