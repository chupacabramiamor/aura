var modal = {
    template: '<div id="modal-\' + Date.now() + \'" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog" :class="modalSizeClass"><div class="modal-content"><slot></slot></div></div></div>',
    props: {
        size: {
            default: 'medium'
        }
    },

    created: function() {
        this.$on('isbusy', function(payload) {
            this.setBusy(payload);
        });
    },

    mounted: function() {
        this.$root.$on('bsModalShow', function(payload) {
            if (this.$attrs.id == payload.id) {
                $(this.$el).modal('show');
            }
        }.bind(this));

        this.$root.$on('bsModalHide', function(payload) {
            if (this.$attrs.id == payload.id) {
                $(this.$el).modal('hide');
            }
        }.bind(this));

        this.$watch('$root.bsModal', function(value, oldValue) {
            if (this.$attrs.id == value) {
                $(this.$el).modal('show');

                $(this.$el).on('hidden.bs.modal', function() {
                    this.$root.bsModal = false;
                }.bind(this));
            }

            if (value == false && oldValue == this.$attrs.id) {
                $(this.$el).modal('hide');
            }
        });
    },

    methods: {
        setBusy: function(value) {
            // this.placePreloader('dataForm', value);
        },

        getModalSizeClass: function() {
            switch (this.size) {
                case 'meduim':
                    return { 'modal-md': true };
                case 'large':
                    return { 'modal-lg': true };
                default:
                    return {};
            }
        }
    },

    computed: {
        modalSizeClass: function() {
            switch (this.size) {
                case 'meduim':
                    return { 'modal-md': true };
                case 'large':
                    return { 'modal-lg': true };
                default:
                    return {};
            }
        }
    }
};

var modalHeader = {
    template: '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title">{{ title }}</h4></div>',
    props: {
        title: {
            default: 'Unnamed title',
            type: String
        }
    },
};

module.exports.modal = modal;
module.exports.modalHeader = modalHeader;