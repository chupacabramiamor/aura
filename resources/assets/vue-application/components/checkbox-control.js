module.exports = {
    template: '<div class="checkbox checkbox-dark">\
        <input :id="\'checkbox-\' + _uid" type="checkbox" :value="value" :checked="isChecked()" @change="changeHandler">\
        <label :for="\'checkbox-\' + _uid">{{ label }}</label>\
        </div>',

    model: {
        prop: 'checked',
        event: 'change'
    },

    props: [ 'checked', 'value', 'label' ],

    methods: {
        changeHandler: function(e) {
            var value = this.checked;

            if (value instanceof Array) {
                if (e.target.checked) {
                    value.push(this.value);
                } else value.splice(value.indexOf(this.value), 1);
            } else {
                value = !Boolean(value);
            }

            this.$emit('change', value);
        },

        isChecked: function() {
            if (this.checked == undefined) {
                return false;
            }

            if (! (this.checked instanceof Array)) {
                return Boolean(this.checked);
            }

            return this.checked && this.checked.indexOf(this.value) != -1;
        }
    }
};