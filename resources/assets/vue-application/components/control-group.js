module.exports = {
    template: '<div class="form-group" :class="{ \'has-error\': errors }">\
        <div v-if="type == \'basic\'" class="col-xs-12">\
            <label :class="{\'sr-only\' : srOnly}" :for="\'form-control-\' + name ">{{ label }}</label>\
            <slot></slot>\
            <p class="help-block small" v-if="errors" v-html="errors[0]"></p>\
        </div>\
        <div v-if="type == \'horizontal\'">\
            <label class="control-label col-xs-12" :class="\'col-md-\' + labelSize" :for="\'form-control-\' + name ">{{ label }}:</label>\
            <div class="col-xs-12" :class="\'col-md-\' + (12 - labelSize)">\
                <slot></slot>\
                <p class="help-block small" v-if="errors" v-html="errors[0]"></p>\
            </div>\
        </div>\
    </div>',

    props: {
        label: String,
        errors: null,
        type: {
            type: String,
            default: 'basic'
        },
        labelSize: {
            default: 4
        },
        srOnly: {
            type: Boolean,
            default: true
        }
    },

    computed: {
        name: function() {
            return Date.now() + Math.floor(Math.random() * 10000);
        }
    }
};

