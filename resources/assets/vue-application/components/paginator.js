module.exports = {
    template: '<div><div class="col-sm-6 text-left" v-if="total">\
                        <div class="dataTables_info">Showing {{ value * perpage - perpage + 1 }} to {{ value * perpage }} of {{ total }} entries</div>\
                    </div>\
                    <div class="col-sm-6 text-right">\
                        <div class="dataTables_paginate paging_simple_numbers">\
                            <ul class="pagination pagination-split">\
                                <li :class="{ disabled: value == 1 }"><a href="#" @click.prevent="backward()"><i class="fa fa-angle-left"></i></a></li>\
                                <li :class="{active: item.number == value}" v-for="item in getPagesItems" v-if="total">\
                                    <div v-if="item.type == TYPE_SEPARATOR">{{ item.caption }}</div>\
                                    <a href="#" v-if="item.type == TYPE_PAGE" @click.prevent="jump(item.number)">{{ item.caption }}</a>\
                                </li>\
                                <li :class="{ disabled: value == getPagesCount || count < perpage }"><a href="#" @click.prevent="forward()"><i class="fa fa-angle-right"></i></a></li>\
                            </ul>\
                        </div>\
                    </div></div>',

    props: [ 'value', 'total', 'perpage', 'siblings', 'count' ],

    data: function() {
        return {
            pages: [],
            TYPE_SEPARATOR: 1,
            TYPE_PAGE: 2,
        };
    },

    mounted: function() {
        if (this.total) {
            for (var i = 1; i <= this.getPagesCount; i++) {
                this.pages.push(i);
            }
        }
    },

    created: function() {
    },


    methods: {
        forward: function() {
            if (this.count !== undefined) {
                if (this.count < this.perpage) {
                    return;
                }
            }

            this.jump(this.value + 1);
        },
        backward: function() {
            this.jump(this.value - 1);
        },
        jump: function(page) {
            page = page || 1;

            if (page < 1 || page > this.getPagesCount) {
                return;
            }

            this.value = page;
            this.$emit('input', page);
        }
    },

    computed: {
        getPagesCount: function() {
            return Math.ceil(this.total / this.perpage);
        },

        getPagesItems: function() {
            var generateItem = function(type, number, caption) {
                return {
                    number: number,
                    caption: caption || number,
                    type: type
                };
            };

            var siblings = siblings || 1;
            var result = [];

            if (this.pages.length <= 1) {
                return result;
            }

            for (var i = this.value - siblings; i <= this.value + siblings; i++) {
                if (i > 1 && i < this.pages.length) {
                    result.push(generateItem(this.TYPE_PAGE, i));
                }
            }

            if (this.value - siblings > 2) {
                result.unshift(generateItem(this.TYPE_SEPARATOR, '...'));
            }

            if (this.value + siblings < this.pages.length) {
                result.push(generateItem(this.TYPE_SEPARATOR, '...'));
            }

            result.unshift(generateItem(this.TYPE_PAGE, 1));
            result.push(generateItem(this.TYPE_PAGE, this.pages.length));

            return result;
        },
    },

    watch: {
        total: function(value) {
            if (value > 0) {
                this.pages = [];

                for (var i = 1; i <= this.getPagesCount; i++) {
                    this.pages.push(i);
                }
            }
        }
    }
};