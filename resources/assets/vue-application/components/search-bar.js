module.exports = {
    template: '<div class="form-group has-feedback"><input type="text" @keyup="change" class="form-control" placeholder="Search..."><i class="fa fa-search form-control-feedback l-h-34"></i></div>',

    props: ['value', 'timer'],

    methods: {
        change: function() {
            var timer = this.timer == undefined ? 800 : this.timer;

            if (timer > 0) {
                clearTimeout(this.timeout);
            }

            this.timeout = setTimeout(function() {
                this.value = this.$el.querySelector('input').value;
                this.$emit('input', this.value);
            }.bind(this), timer);
        }
    }
};