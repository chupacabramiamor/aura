module.exports = {

    template: '\n\n        <div v-show="isActive" class="tab-pane fade in"><slot></slot></div>\n\n    ',

    props: {
        name: { required: true },
        selected: { default: false }
    },

    data: function data() {

        return {
            isActive: false
        };
    },


    computed: {
        href: function href() {
            return '#' + this.name.toLowerCase().replace(/ /g, '-');
        }
    },

    mounted: function mounted() {

        this.isActive = this.selected;
    }
};