var datepickerFormat = {
    toDisplay: function (date, format, language) {
        var result = moment(date).format('D MMMM YYYY');
        return result;
    },
    toValue: function (date, format, language) {
        return new Date(date);
    }
};

module.exports = {
    template: '<input type="text" class="form-control" @change="change()">',

    props: [ 'value', 'format', 'startDate', 'endDate', 'todayBtn' ],

    mounted: function() {
        var format = this.format || 'yyyy-mm-dd';
        var startDate = this.startDate || moment(Date.now()).format('YYYY-MM-DD');
        var endDate = this.endDate || false;
        var todayBtn = this.todayBtn;

        this.$el.setAttribute('placeholder', format);

        $(this.$el).datepicker({
            language: "uk",
            weekStart: 1,
            autoclose: true,
            todayHighlight: true,
            startDate: startDate,
            endDate: endDate,
            todayBtn: todayBtn,
            format: datepickerFormat,
        }).on('changeDate', this.onChangeHandler);

        var _value = (this.value !== undefined) ? this.value : endDate;

        $(this.$el).datepicker('setDate', _value);
    },

    methods: {
        onChangeHandler: function(e) {
            this.change(e.date);
        },
        change: function(value) {
            if (value) {
                this.$emit('input', moment(value).format('YYYY-MM-DD'));
            }
        }
    }
};