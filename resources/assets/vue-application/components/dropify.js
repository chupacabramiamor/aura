module.exports = {
    template: '<input type="file" class="dropify" @change="change($event)"/>',

    props: ['value', 'src'],

    data: {
        dropify: null,
    },

    mounted: function() {
        this.init();
    },

    updated: function() {
        this.dropify.resetPreview();
        this.dropify.setPreview(this.dropify.cleanFilename(this.src));
    },

    watch: {
        src: function() {
            this.dropify.resetPreview();

            try {
                this.dropify.file.name = this.dropify.cleanFilename(this.src);
            } catch (e) {
                return;
            }

            this.dropify.setPreview(this.src);
        }
    },

    methods: {
        init: function() {
            this.dropify = new Dropify(this.$el, {
                messages: {
                    'default': '',
                    'replace': 'Перекиньте файл сюди',
                    'remove': 'Видалити',
                    'error': 'Щось стряслося'
                }
            });

            if (this.src) {
                this.dropify.setPreview(this.src);
            }
        },

        change: function(e) {
            this.value = e.target.files[0];
            this.$emit('input', this.value);
        }
    }
};