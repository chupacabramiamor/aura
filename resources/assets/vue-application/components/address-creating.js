var pscsVueService = require('./../services/pscs.js');
var streetTypesVueService = require('./../services/street-types.js');
var localitiesVueService = require('./../services/localities.js');
var streetVueService = require('../services/streets.js');

var vValidateMixin = require('./../mixins/validate.js');
var vBlockUIMixin = require('./../mixins/blockui.js');
var vSwalMixin = require('../mixins/swal.js');

var pscPlacementComponent = require('./psc-placement.js');

var transformStreetNamesFilter = require('../filters/street-names.js');

var promises  = {};

module.exports = {
    template: '#address-creating-tpl',

    data: function() {
        return {
            pscList: [],
            localityList: [],
            streetTypeList: [],
            streetList: [],
            form: {}
        };
    },

    mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],

    components: {
        'psc-placement': pscPlacementComponent
    },

    mounted: function() {
        this.setPreloader('form');

        promises.pscs = pscsVueService.fetch().then(function(data) {
            this.pscList = data;
            return data;
        }.bind(this));

        promises.streetTypes = streetTypesVueService.fetch().then(function(data) {
            this.streetTypeList = data;
            return data;
        }.bind(this));

        promises.localities = localitiesVueService.fetch().then(function(data) {
            this.localityList = data;
            return data;
        }.bind(this));

        promises.streets = streetVueService.fetch({ locality_id: this.form.locality_id }).then(function(data) {
            this.streetList = data;
            return data;
        }.bind(this));

        Promise.all([ promises.pscs, promises.streetTypes, promises.localities, promises.streets ]).then(function(values) {
            this.unsetPreloader('form');
        }.bind(this));

        this.$root.$on('addressRequestCreating', function (payload) {
            this.$emit('bsModalShow', {
                id: 'address-request-modal'
            });
        });
    },

    methods: {
        send: function() {

            if (!this.locality) {
                return this.markUnvalidated('locality_id', [ 'Будь ласка вкажіть населений пункт' ]);
            }

            this.setPreloader('form');

            var data = {
                psc_id: this.form.psc_id,
                locality_id: this.locality.id,
                postcode: this.form.postcode,
                street_id: this.form.street_id,
                house_number: this.form.house_number,
                apartment_number: this.form.apartment_number,
                building: this.form.building
            };

            this.$http.post('/addresses/send-creating-request', data).then(function(response) {
                this.$emit('bsModalHide', {
                    id: 'address-request-modal'
                });
                this.alert('Запит створено. Найблищим часом, адміністратор перевіре введені дані та надасть свою відповідь', 'success').then(function() {
                    document.location.href = '/voters';
                });
                this.unsetPreloader('form');
            }.bind(this), function() {
                this.unsetPreloader('form');
            }.bind(this));
        },

        getQueryParams: function() {
            return {
                psc_id: this.form.psc_id
            };
        }
    },

    computed: {
        psc: {
            get: function() {
                var _index = _.findIndex(this.pscList, { id: this.form.psc_id });
                return this.pscList[_index];
            },
            set: function(value) {
                if (value) {
                    this.$set(this.form, 'psc_id', value.id);
                    this.$emit('input', this.getQueryParams());
                }
            }
        },
        locality: {
            get: function() {
                var _index = _.findIndex(this.localityList, { id: this.form.locality_id });

                if (_index > -1) {
                    this.setPreloader('form');
                    promises.streets = streetVueService.fetch({ locality_id: this.form.locality_id }).then(function(data) {
                        this.streetList = data;
                        this.unsetPreloader('form');
                        return data;
                    }.bind(this));
                }

                return this.localityList[_index];
            },

            set: function(value) {
                this.$set(this.form, 'locality_id', value.id);
            },
        },
        street: {
            get: function() {
                var _index = _.findIndex(this.streetList, { id: this.form.street_id });

                return this.streetList[_index];
            },

            set: function(value) {
                this.$set(this.form, 'street_id', value && value.id);
            }
        }
    },

    filters: {
        transformStreetNames: transformStreetNamesFilter
    }
};