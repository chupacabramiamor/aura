module.exports = {
    template: '<textarea class="form-control" :id="\'form-control-\' + ($parent.name || Date.now())" @keyup="change">{{ value }}</textarea>',

    props: [ 'value' ],

    methods: {
        change: function(e) {
            this.$emit('input', e.target.value);
        }
    }
};