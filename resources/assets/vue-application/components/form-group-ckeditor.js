module.exports = {
    mounted: function() {
        var _this = this;
        var textarea = $(this.$el).find('textarea').get(0);
        var editorInstance = CKEDITOR.replace(textarea, CKEDITOR.editorConfig);
        console.log(editorInstance);
        editorInstance.on('change', function() {
            _this.$emit('input', this.getData());
        });
    },
    template: '#form-control-ckeditor-tpl',
};