module.exports = {
    template: '\n        <div>\n            <ul class="nav nav-tabs">\n                <li v-for="tab in tabs" :class="{ \'active\': tab.isActive }">\n                    <a :href="tab.href" @click="selectTab(tab)">{{ tab.name }}</a>\n                </li>\n              </ul>\n            <div class="tabs-content">\n                <slot></slot>\n            </div>\n        </div>\n    ',

    data: function data() {
        return { tabs: [] };
    },
    created: function created() {

        this.tabs = this.$children;
    },

    methods: {
        selectTab: function selectTab(selectedTab) {
            this.tabs.forEach(function (tab) {
                tab.isActive = tab.name == selectedTab.name;
            });
        }
    }
};