module.exports = {
    template: '<input type="file" @change="change($event)">',

    props: [ 'value' ],

    data: function () {
        return {
            _value: false
        };
    },

    mounted: function() {
        $(this.$el).filestyle({
            placeholder: "Файл не выбран",
        });
    },

    methods: {
        change: function(e) {
            this.$emit('input', e.target.files[0]);
        }
    }
};