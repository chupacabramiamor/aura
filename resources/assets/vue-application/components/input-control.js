var vUUIDMixin = require('./../mixins/uuid.js');

module.exports = {
    template: '<input type="text" class="form-control" :id="\'form-control-\' + ($parent.name || uuidv4())" :value="value" @keyup="change" :placeholder="($parent.srOnly) ? $parent.label : placeholder">',

    props: [ 'value', 'placeholder' ],

    mixins: [ vUUIDMixin ],

    methods: {
        change: function(e) {
            this.$emit('input', e.target.value);
        },
    }
};