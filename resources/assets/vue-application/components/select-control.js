module.exports = {
    template: '<select class="form-control" style="cursor: pointer" @change="change">\
        <option :disabled="!keepNothingChoosed" :value="undefined" :selected="value == undefined || value == null">{{ placeholder || "Зробіть вибір..."}}</option>\
        <option :value="item.id" v-for="item in list" :selected="item.id == value">{{ item[display] }}</option>\
        </select>',

    props: {
        value: null,
        list: Array,
        placeholder: null,
        display: {
            default: 'name',
            type: String
        },
        keepNothingChoosed: {
            default: true,
            type: Boolean
        }
    },

    mounted: function() {
    },

    data: function() {
        return {
            val: this.value
        };
    },

    methods: {
        change: function(e) {
            this.$emit('input', e.target.value);
        }
    }
};