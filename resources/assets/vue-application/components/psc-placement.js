var vLocalityMixin = require('./../mixins/locality.js');

module.exports = {
    template: '#psc-placement-tpl',
    props: [ 'psc' ],
    mixins: [ vLocalityMixin ]
};