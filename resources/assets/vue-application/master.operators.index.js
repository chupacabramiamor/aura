require('./boot.js');

var vueElement = '#vue-operators-app';

var Vue = require('vue');
var VueResource = require('vue-resource');
var vValidateMixin = require('./mixins/validate.js');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var modalComponent = require('./components/modal.js');

Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('content');

var pscListComponent = {
    template: '#psc-list-tpl',

    props: [ 'list', 'value' ],

    data: function() {
        return {
            isAllSelected: false
        };
    },

    mounted: function() {
        this.$el.setAttribute('id', this.$vnode.tag);

        $(this.$el).find('.slimscroll').slimscroll({
            size: "9px",
            color: '#9ea5ab'
        });
    },

    methods: {
        toggleSelectAll: function() {
            this.pscs = [];

            if (this.isAllSelected == false) {
                for (var _index in this.list) {
                    this.pscs.push(this.list[_index].id);
                }
                this.isAllSelected = true;
            } else this.isAllSelected = false;

            this.$emit('input', this.pscs);
        },

        onChange: function(pscs) {
            this.isAllSelected = false;
            this.$emit('input', pscs);
        }
    },

    computed: {
        pscs: {
            get: function(value) {
                return this.value || [];
            },

            set: function() {},
        },

        pscsGrouped: function() {
            return _.chunk(this.list, 4);
        }
    }
};

var creatingFormComponent = {
    template: '#creating-form-tpl',

    mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],

    data: function() {
        return {
            model: {
                pscs: []
            },
            pscList: pscList,
            controlValidation: [],
            preloaders: {},
        };
    },

    created: function() {
        this.RESOURCE_BASE_URL = '/resources/operators';
    },

    mounted: function() {
        this.$root.$on('creating', function() {
            $('#operator-creating-modal').modal('show');
            this.markAllValidated();
            this.$set(this, 'model', {
                pscs: []
            });
        }.bind(this));
    },

    components: {
        'v-modal-header': modalComponent.modalHeader,
        'psc-list': pscListComponent
    },

    methods: {
        save: function () {
            var data = {
                fullname: this.model.fullname,
                login: this.model.login,
                is_enabled: this.model.is_enabled,
                password: this.model.password,
                password_confirmation: this.model.password_confirmation,
                pscs: this.model.pscs,
                email: this.model.email
            };

            this.setPreloader('form');

            this.$http.post(this.RESOURCE_BASE_URL, data).then(function(response) {
                this.unsetPreloader('form');
                $('#operator-creating-modal').modal('hide');
                this.$root.$emit('created', { id: response.data.id });
                this.alert('Новый пользователь был успешно добавлен');
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        }
    }
};

/** editingFormComponent */
var editingFormComponent = {
    template: '#creating-form-tpl',

    mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],

    data: function() {
        return {
            model: {},
            preloaders: {},
            controlValidation: [],
            pscList: pscList,
        };
    },

    created: function() {
        this.RESOURCE_BASE_URL = '/resources/operators';
    },

    mounted: function() {
        this.$root.$on('editing', function(payload) {
            $('#operator-editing-modal').modal('show');
            this.markAllValidated();
            this.setPreloader('form');

            this.$http.get(this.RESOURCE_BASE_URL + '/' + payload.id).then(function(response) {
                this.model = response.data;
                this.model.pscs = this.model.pscs.map(function(item) {
                    return item.id;
                });
                this.unsetPreloader('form');
            }.bind(this));
        }.bind(this));
    },

    components: {
        'v-modal-header': modalComponent.modalHeader,
        'psc-list': pscListComponent
    },

    methods: {
        save: function () {
            var data = {
                fullname: this.model.fullname,
                login: this.model.login,
                password: this.model.password,
                password_confirmation: this.model.password_confirmation,
                is_enabled: this.model.is_enabled,
                pscs: this.model.pscs,
                email: this.model.email
            };

            this.setPreloader('form');

            this.$http.put(this.RESOURCE_BASE_URL + '/' + this.model.id, data).then(function(response) {
                this.unsetPreloader('form');
                $('#operator-editing-modal').modal('hide');
                this.$root.$emit('updated', { id: this.model.id });
                this.alert('Изменения успешно сохранены');
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        }
    },
};

if (document.querySelector(vueElement)) {
    new Vue({
        el: vueElement,

        mixins: [ vValidateMixin, vBlockUIMixin, vSwalMixin ],

        data: function() {
            return {
                list: window.operatorList || [],
            };
        },

        components: {
            'v-modal': modalComponent.modal,
            'v-modal-header': modalComponent.modalHeader,
            'v-creating-form': creatingFormComponent,
            'v-editing-form': editingFormComponent,
        },

        mounted: function() {
            this.$on('created', function() {
                this.$http.get('/resources/operators').then(function(response) {
                    this.list = response.data;
                }.bind(this));
            }.bind(this));

            this.$on('updated', function(payload) {
                var _index = _.findIndex(this.list, { id: payload.id });
                this.$http.get('/resources/operators/' + payload.id).then(function(response) {
                    this.$set(this.list, _index, response.data);
                }.bind(this));
            }.bind(this));
        },

        methods: {
            showCreatingModal: function () {
                this.$emit('creating', {});
            },

            showEditingModal: function(id) {
                this.$emit('editing', {
                    id: id
                });
            },

            remove: function(id) {
                this.confirm().then(function() {
                    this.$http.delete('/resources/operators/' + id).then(function(response) {
                        var _index = _.findIndex(this.list, {id: id});
                        this.list.splice(_index, 1);
                    }.bind(this));
                }.bind(this));
            },
        },

        filters: {
            pretifyEnabled: function(value) {
                return value == 0 ? 'Нет' : 'Да';
            },

            pretifyFullname: function(value) {
                return value || 'Nonamed';
            }
        }
    });
}