var boot = require('./boot.js');

var Vue = require('vue');
var VueResource = require('vue-resource');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var vValidateMixin = require('./mixins/validate.js');
var vLocalityMixin = require('./mixins/locality.js');

var modalComponent = require('./components/modal.js');
var streetCreatingFormComponent = require('./components/street-creating.js');
var pscPlacementComponent = require('./components/psc-placement.js');

var streetTypesVueService = require('./services/street-types.js');
var pscsSvc = require('./services/pscs.js');
var streetVueService = require('./services/streets.js');

Vue.component('v-modal', modalComponent.modal);
Vue.component('v-modal-header', modalComponent.modalHeader);

var transformStreetNamesFilter = require('./filters/street-names.js');

Vue.use(VueResource);

var addressFormComponent = {
    template: '#address-form-tpl',
    mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],
    data: function() {
        return {
            promises: {},
            form: {},
            pscList: [],
            localityList: localityList,
            streetTypeList: [],
            streetList: [],
            // locality: {},
            notifyIniter: null
        };
    },

    components: {
        'psc-placement': pscPlacementComponent
    },

    mounted: function() {
        if (window.notify_id) {
            this.$http.get('/notifies/row-data/' + notify_id).then(function(response) {
                this.form = response.data.payload;
                this.notifyIniter = response.data.initer;
            }.bind(this));
        }

        if (this.psc) {
            this.fetchLocalityData(this.psc.locality_id);
        }

        this.promises.pscs = pscsSvc.fetch().then(function(data) {
            this.pscList = data;
            return data;
        }.bind(this));

        this.promises.streetTypes = streetTypesVueService.fetch().then(function(data) {
            this.streetTypeList = data;
            return data;
        }.bind(this));

        this.promises.streets = streetVueService.fetch({ locality_id: this.form.locality_id }).then(function(data) {
            this.streetList = data;
            this.unsetPreloader('form');
            return data;
        }.bind(this));

        Promise.all([ this.promises.streets, this.promises.streetTypes ]).then(function(values) {
            this.street = _.find(values[0], { id: parseInt(this.form.street_id) });
        }.bind(this));

        this.$root.$on('streetCreated', function(payload) {
            this.locality = _.find(this.localityList, { id: payload.locality_id });

            streetVueService.fetch({ locality_id: payload.locality_id }, true).then(function(data) {
                this.streetList = data;
                this.street = _.find(this.streetList, { id: payload.id });
            }.bind(this));
        }.bind(this));
    },

    methods: {
        fetchLocalityData: function(locality_id) {
            return this.$http.get('/addresses/locality-data/' + locality_id).then(function(response) {
                this.form.locality_id = response.data.id;
            }.bind(this));
        },

        save: function() {
            this.setPreloader('form');

            var data = {
                psc_id: this.form.psc_id,
                locality_id: this.locality.id,
                street_id: this.street.id,
                house_number: this.form.house_number,
            };

            if (this.form.postcode) {
                data.postcode = this.form.postcode;
            }

            if (this.form.apartment_number) {
                data.apartment_number = this.form.apartment_number;
            }

            if (this.form.building) {
                data.building = this.form.building;
            }

            this.$http.post('/addresses/add', data).then(function(response) {
                this.alert('item_was_added_successfully', 'success').then(function(){
                    document.location = response.data.url;
                });
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        },

        rejectNewAddr: function() {
            this.alert('Ви дійсно впевнені, що бажаєте відмовити в заявці?').then(function() {
                this.setPreloader('form');

                this.$http.get('/addresses/reject/' + this.notifyIniter.id).then(function(response) {
                    this.alert('У заявці відмовлено!', 'info').then(function() {
                        document.location = response.data.url;
                    });
                }.bind(this), this.unsetPreloader.call(this, 'form'));
            }.bind(this));
        },

        newStreetModal: function() {
            this.$root.$emit('streetCreating', {});
        }
    },

    computed: {
        psc: {
            get: function() {
                if (!this.form.psc_id) {
                    return null;
                }

                var _index = _.findIndex(this.pscList, { id: this.form.psc_id });
                return this.pscList[_index];
            },
            set: function(value) {
                this.$set(this.form, 'psc_id', value && value.id);
            }
        },

        localities: function() {
            if (!this.psc) {
                return this.localityList;
            }

            return this.localityList.filter(function(item) {
                return item.submission_id == this.psc.locality.submission_id;
            }.bind(this));
        },

        locality: {
            get: function() {
                var _index = _.findIndex(this.localityList, { id: this.form.locality_id });

                if (_index > -1) {
                    this.setPreloader('form');
                    this.promises.streets = streetVueService.fetch({ locality_id: this.form.locality_id }).then(function(data) {
                        this.streetList = data;
                        this.unsetPreloader('form');
                        return data;
                    }.bind(this));
                }

                return this.localityList[_index];
            },

            set: function(value) {
                this.$set(this.form, 'locality_id', value.id);
            },
        },

        street: {
            get: function() {
                var _index = _.findIndex(this.streetList, { id: this.form.street_id });

                return this.streetList[_index];
            },

            set: function(value) {
                this.$set(this.form, 'street_id', value && value.id);
            }
        }
    },

    filters: {
        transformStreetNames: transformStreetNamesFilter
    }
};

new Vue({
    el: '#vue-address-adding-app',
    components: {
        'address-form': addressFormComponent,
        'street-form': streetCreatingFormComponent
    },
    data: {},
    mounted: function() {
        this.$on('streetCreating', function (payload) {
            this.$emit('bsModalShow', {
                id: 'street-creating-modal'
            });
        });

        this.$on('streetCreated', function (payload) {
            this.$emit('bsModalHide', {
                id: 'street-creating-modal'
            });
        });
    }
});