require('./boot.js');

var vueElement = '#vue-questionnaire-app';

var Vue = require('vue');
var VueResource = require('vue-resource');
var vValidateMixin = require('./mixins/validate.js');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var modalComponent = require('./components/modal.js');

Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('content');

const STATE_EMPTY = 0;
const STATE_PREPARED = 1;
const STATE_UPLOADING = 2;
const STATE_UPLOADED = 3;
const STATE_CONFIRMED = 4;
const STATE_FINISHED = 5;

var uploaderComponent = {
    template: '#uploader-tpl',

    data: function() {
        return {
            isUploaded: false,
            preparedFile: null,
            mod: null,
        };
    },

    created: function() {
        this.modList = [
            { id: 1, value: 'Кременчук' },
            { id: 2, value: 'Горішні Плавні' },
            { id: 3, value: 'Кременчуцький район' },
            { id: 4, value: 'Глобинський район' },
        ];
    },

    methods: {
        doUpload: function() {
            this.$root.$emit('uploadingStarted', {
                preparedFile: this.preparedFile
            });

            var data = new FormData();

            data.append('file', this.preparedFile);
            data.append('mod', this.mod);

            this.$http.post('/sync', data).then(function(response) {
                this.$root.$emit('showConfirmationDialog', {
                    state: STATE_UPLOADED,
                    data: response.data
                });
            }.bind(this)).finally(function() {
                this.$root.$emit('uploadingFinished');
            }.bind(this));
        }

    }
};

new Vue({
	el: '#vue-sync-app',

    mixins: [ vBlockUIMixin, vSwalMixin ],

    data: {
        xlsx: STATE_EMPTY
    },

	components: {
        'v-uploader': uploaderComponent,
	},

    mounted: function() {
        this.$on('uploadingStarted', function() {
            this.setPreloader('form');
        });

        this.$on('uploadingFinished', function() {
            this.unsetPreloader('form');
        });

        this.$on('showConfirmationDialog', function(payload) {
            this.xlsx = payload;
        });
    },

    methods: {
        confirmation: function() {
            this.setPreloader('form');
            this.xlsx.state = STATE_CONFIRMED;

            this.$http.post('/sync/uploading-confirm', { filename: this.xlsx.data.filename }).then(function(response) {
                this.xlsx.state = STATE_FINISHED;
                this.alert('Імпорт успішно виконаний!', 'success');
            }.bind(this)).finally(function() {
                this.unsetPreloader('form');
            }.bind(this));
        }
    }
});