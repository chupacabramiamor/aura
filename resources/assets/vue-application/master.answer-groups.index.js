require('./boot.js');

var vueRootElement = '#vue-answer-groups-app';

var Vue = require('vue');
var VueResource = require('vue-resource');
var vValidateMixin = require('./mixins/validate.js');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var modalComponent = require('./components/modal.js');

Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('content');

var RESOURCE_BASE_URL = '/resources/answer-groups';

if (document.querySelector(vueRootElement)) {
    var answerListComponent = {
        template: '#answer-list-tpl',

        props: [ 'group_id' ],

        mixins: [ vBlockUIMixin, vSwalMixin ],

        data: function() {
            return {
                list: [],
                deletingList: [],
                preloaders: {},
                answer: null,
                alterData: null,
                editable: false
            };
        },

        mounted: function() {
            if (this.$parent.model.id) {
                this.$http.get('/resources/answers', {params: { group_id: this.$parent.model.id }}).then(function(response) {
                    this.list = response.data;
                    this.deletingList = [];
                    this.$parent.isRemovable = this.list == 0;
                }.bind(this));
            }

            this.$root.$on('answerGroupUpdating', function(payload) {
                this.$http.get('/resources/answers', {params: { group_id: payload.id }}).then(function(response) {
                    this.list = response.data;
                    this.deletingList = [];
                    this.$parent.isRemovable = this.list == 0;
                }.bind(this));
            }.bind(this));


            this.$root.$on('answerGroupUpdated', function(payload) {
                var answerPromises = [];
                var item;

                if (this.list.length > 0) {

                    for (var i = 0; i < this.list.length; i++) {
                        item = this.list[i];
                        var promise;

                        if (item.id) {
                            promise = this.$http.put('/resources/answers/' + item.id, {
                                value: item.value
                            });
                        } else {
                            promise = this.$http.post('/resources/answers', {
                                group_id: payload.id,
                                value: item.value
                            });
                        }

                        answerPromises.push(promise);
                    }
                }

                if (this.deletingList.length > 0) {
                    for (var j = 0; j < this.deletingList.length; j++) {
                        item = this.deletingList[j];
                        answerPromises.push(this.$http.delete('/resources/answers/' + item.id));
                    }
                }

                Promise.all(answerPromises).then(function () {
                    this.$root.$emit('answersSaved', {});
                }.bind(this));
            }.bind(this));

            this.$root.$on('answerGroupCreating', function() {
                this.list = [];
                this.deletingList = [];
                this.$parent.isRemovable = 0;
            }.bind(this));

            this.$root.$on('answerGroupCreated', function(payload) {
                var answerPromises = [];

                if (this.list.length > 0) {

                    for (var i = 0; i < this.list.length; i++) {
                        var item = this.list[i];
                        var data = {
                            group_id: payload.id,
                            value: item.value
                        };

                        answerPromises.push(this.$http.post('/resources/answers', data));
                    }
                }

                Promise.all(answerPromises).then(function () {
                    this.$root.$emit('answersSaved', {});
                }.bind(this));
            }.bind(this));
        },

        methods: {
            add: function() {
                if (!this.answer) {
                    return;
                }

                this.list.unshift({
                    value: this.answer
                });

                this.answer = '';
            },

            edit: function(index) {
                this.editable = (this.editable === false) ? index : false;
                this.alterData = JSON.parse(JSON.stringify(this.list[index]));
            },

            save: function(index) {
                var data = JSON.parse(JSON.stringify(this.alterData));

                this.list[index].value = data.value;
                this.edit(index);
            },

            remove: function(index) {
                this.confirm('Удаление ответа').then(function() {
                    this.deletingList.push(this.list.splice(index, 1)[0]);
                }.bind(this));
            }
        }
    };

    var answerGroupListComponent = {
        template: '#answer-groups-list-tpl',

        mixins: [ vBlockUIMixin, vSwalMixin ],

        data: function() {
            return {
                list: [],
                preloaders: {}
            };
        },

        mounted: function() {
            this.setPreloader('list');

            this.$http.get(RESOURCE_BASE_URL).then(function (response) {
                this.list = response.data;
                this.unsetPreloader('list');
            }.bind(this));

            this.$root.$on('answerGroupCreated', function(payload) {
                this.$http.get(RESOURCE_BASE_URL + '/' + payload.id).then(function(response) {
                    this.list.push(response.data);
                    this.alert('item_was_added_successfuly');
                }.bind(this));
            }.bind(this));

            this.$root.$on('answerGroupUpdated', function(payload) {
                var _index = _.findIndex(this.list, {id: payload.id});

                this.$http.get(RESOURCE_BASE_URL + '/' + payload.id).then(function(response) {
                    this.$set(this.list, _index, response.data);
                }.bind(this));
            }.bind(this));

            this.$root.$on('answerGroupDeleted', function(payload) {
                var _index = _.findIndex(this.list, { id: payload.id });
                this.list.splice(_index, 1);
            }.bind(this));
        },

        methods: {
            edit: function(group_id) {
                this.$root.$emit('answerGroupUpdating', {
                    id: group_id
                });
            }
        }

    };


/**
 * [answerGroupCreatingFormComponent description]
 * @type {Object}
 */
    var answerGroupCreatingFormComponent = {
        template: '#answer-group-form-tpl',

        mixins: [ vBlockUIMixin, vValidateMixin ],

        components: {
            'v-modal-header': modalComponent.modalHeader,
            'answer-list': answerListComponent
        },

        data: function() {
            return {
                model: {},
                typeList: [],
                preloaders: {},
                controlValidation: {},
                isRemovable: false
            };
        },

        mounted: function() {
            this.setPreloader('form');

            this.$http.get('/questionnaire/answer-groups/type-list').then(function (response) {
                this.typeList = response.data;
                this.unsetPreloader('form');
            }.bind(this));

            this.$root.$on('answerGroupCreating', function() {
                this.model = {};
            }.bind(this));
        },

        methods: {
            save: function() {
                this.setPreloader('form');

                var data = {
                    name: this.model.name,
                    type_id: this.model.type_id
                };

                this.$http.post(RESOURCE_BASE_URL, data).then(function(response) {
                    this.$root.$emit('answerGroupCreated', {
                        id: response.data.id
                    });
                }.bind(this), this.unsetPreloader.call(this, 'form'));
            }
        }
    };

    var answerGroupEditingFormComponent = {
        template: '#answer-group-form-tpl',

        mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],

        components: {
            'v-modal-header': modalComponent.modalHeader,
            'answer-list': answerListComponent
        },

        data: function() {
            return {
                model: {},
                isRemovable: false,
                typeList: [],
                preloaders: {},
                controlValidation: {}
            };
        },

        mounted: function() {
            this.setPreloader('form');

            this.$http.get('/questionnaire/answer-groups/type-list').then(function (response) {
                this.typeList = response.data;
                this.unsetPreloader('form');
            }.bind(this));

            this.$root.$on('answerGroupUpdating', function(payload) {
                this.$http.get(RESOURCE_BASE_URL + '/' + payload.id).then(function(response) {
                    this.model = response.data;

                    this.$root.$emit('bsModalShow', {
                        id: 'answer-group-editing-modal'
                    });
                }.bind(this));

            }.bind(this));
        },

        methods: {
            save: function() {
                this.setPreloader('form');

                var data = {
                    name: this.model.name,
                    type_id: this.model.type_id
                };

                this.$http.put(RESOURCE_BASE_URL + '/' + this.model.id, data).then(function(response) {
                    this.$root.$emit('answerGroupUpdated', {
                        id: this.model.id
                    });
                }.bind(this), this.unsetPreloader.call(this, 'form'));
            },

            remove: function() {
                this.confirm('Удаление группы').then(function() {
                    this.setPreloader('form');
                    this.$http.delete(RESOURCE_BASE_URL + '/' + this.model.id).then(function(){
                        this.unsetPreloader('form');
                        this.$root.$emit('answerGroupDeleted', { id: this.model.id });
                        // this.$root.$emit('bsModalHide', { id: 'answer-group-editing-modal' });
                    }.bind(this), this.unsetPreloader.call(this, 'form'));
                }.bind(this));
            }
        }
    };

    new Vue({
        el: vueRootElement,

        data: {
            bsModal: false
        },

        components: {
            'v-answer-groups-list': answerGroupListComponent,
            'v-answer-group-creating-form': answerGroupCreatingFormComponent,
            'v-answer-group-editing-form': answerGroupEditingFormComponent,
            'v-modal': modalComponent.modal
        },

        mounted: function() {
            this.$on('answerGroupCreated', function(payload) {
                this.bsModal = false;
            });

            this.$on('answerGroupUpdated', function() {
                this.$emit('bsModalHide', {
                    id: 'answer-group-editing-modal'
                });
            });

            this.$on('answerGroupDeleted', function() {
                this.$emit('bsModalHide', {
                    id: 'answer-group-editing-modal'
                });
            });
        },

        methods: {
            showGroupCreatingModal: function() {
                this.bsModal = 'answer-group-creating-modal';
                this.$emit('answerGroupCreating', {});
            }
        }

    });
}