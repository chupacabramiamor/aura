require('./boot.js');

var Vue = require('vue');
var VueResource = require('vue-resource');
var vValidateMixin = require('./mixins/validate.js');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var vLocalityMixin = require('./mixins/locality.js');
var modalComponent = require('./components/modal.js');
var pscPlacementComponent = require('./components/psc-placement.js');
var addressCreatingComponent = require('./components/address-creating.js');
var datepickerComponent = require('./components/datepicker.js');

var pscsVueService = require('./services/pscs.js');
var educationsVueService = require('./services/educations.js');
var addressesVueService = require('./services/addresses.js');
var streetTypesVueService = require('./services/street-types.js');
var occupationsVueService = require('./services/occupations.js');
var partiesVueService = require('./services/parties.js');
var localitiesVueService = require('./services/localities.js');
var campaignPositionsVueService = require('./services/campaign-positions.js');
var rolesVueService = require('./services/roles.js');
var streetVueService = require('./services/streets.js');
var dictonaryVueService = require('./services/dictonary.js');

var transformStreetNamesFilter = require('./filters/street-names.js');

Vue.component('v-modal', modalComponent.modal);
Vue.component('v-modal-header', modalComponent.modalHeader);

var WorkplaceDictonarySvcPromise = dictonaryVueService.fetch({scope: 2});
var WorkpositionDictonarySvcPromise = dictonaryVueService.fetch({scope: 3});

var UserActivityComponent = {
    template: '#user-activity-tpl',

    mixins: [ vLocalityMixin ],

    data: function() {
        return {
            activity: null,
            campaignPositionsList: [],
            educationList: [],
            occupacyList: [],
            partyList: []
        };
    },

    mounted: function() {
        var CampaignPositionsPromise = campaignPositionsVueService.fetch().then(function(data) {
            this.campaignPositionsList = data;
        }.bind(this));

        var EducationsSvcPromise = educationsVueService.fetch().then(function(data) {
            this.educationList = data;
        }.bind(this));

        var OccupationsSvcPromise = occupationsVueService.fetch().then(function(data) {
            this.occupacyList = data;
        }.bind(this));

        var PartiesSvcPromise = partiesVueService.fetch().then(function(data) {
            this.partyList = data;
        }.bind(this));

        this.$root.$on('activityPayloadShowing', function(payload) {
            var _education = _.find(this.educationList, {id: payload.activity.payload.education_type });
            var _occupacy = _.find(this.occupacyList, {id: payload.activity.payload.occupation_type });
            var _party = _.find(this.partyList, {id: payload.activity.payload.party_id });
            var _campaingPosition = _.find(this.campaignPositionsList, {id: parseInt(payload.activity.payload.campaign_position) });
            var _gender = _.find([ { id: 1, value: 'Чоловіча' }, { id: 2, value: 'Жіноча' } ], {id: payload.activity.payload.gender_type });

            this.activity = [
                { key: 'ID', value: payload.activity.payload.id },
                { key: 'Прізвище', value: payload.activity.payload.surname },
                { key: "Ім'я", value: payload.activity.payload.firstname },
                { key: 'По-батькові', value: payload.activity.payload.patronymic },
                { key: 'Email', value: payload.activity.payload.email },
                { key: 'Номер телефону', value: payload.activity.payload.phone_number },
                { key: 'Додатковий номер телефону', value: payload.activity.payload.additional_phone_number },
                { key: 'Дата народження', value: payload.activity.payload.birthday ? moment(payload.activity.payload.birthday).format('D MMMM YYYY') : 'Не визначено' },
                { key: 'Освіта', value: _education ? _education.value : 'Не визначено' },
                { key: 'Стать', value: _gender ? _gender.value : 'Не визначено' },
                { key: 'Професія', value:  _occupacy ? _occupacy.value : 'Не визначено' },
                { key: 'Партійність', value:  _party ? _party.name : 'Не визначено' },
                { key: 'Місце народження', value: payload.activity.payload.birth_locality || 'Не визначено' },
                { key: 'Додаткова інформація', value: payload.activity.payload.description || 'Немає' },
                { key: 'Посада у виборчій кампанії', value: _campaingPosition ? _campaingPosition.value : 'Не визначено' },
                { key: 'Адреса', value: this.$options.filters.prettifyAddress(payload.activity.payload.address) }
            ];
        }.bind(this));
    }
};

new Vue({
    el: '#vue-voters-edit-app',

    mixins: [ vValidateMixin, vBlockUIMixin, vLocalityMixin, vSwalMixin ],

    data: function() {
        return {
            model: voterData,
            pscList: [],
            educationList: [],
            occupacyList: [],
            partyList: [],
            addressList: [],
            streetTypeList: [],
            campaignPositionsList: [],
            workplaceList: [],
            workpositionList: [],
            rolesList: []
        };
    },

    created: function() {
        this.setPreloader('form');

        var PscsSvcPromise = pscsVueService.fetch().then(function(data) {
            this.pscList = data;
        }.bind(this));

        var EducationsSvcPromise = educationsVueService.fetch().then(function(data) {
            this.educationList = data;
        }.bind(this));

        var OccupationsSvcPromise = occupationsVueService.fetch().then(function(data) {
            this.occupacyList = data;
        }.bind(this));

        var PartiesSvcPromise = partiesVueService.fetch().then(function(data) {
            this.partyList = data;
        }.bind(this));

        var StreetTypesSvcPromise = streetTypesVueService.fetch().then(function(data) {
            this.streetTypeList = data;
        }.bind(this));

        var CampaignPositionsPromise = campaignPositionsVueService.fetch().then(function(data) {
            this.campaignPositionsList = data;
        }.bind(this));

        var rolesPromise = rolesVueService.fetch().then(function(data) {
            this.rolesList = data;
        }.bind(this));

        WorkplaceDictonarySvcPromise.then(function(data) {
            this.workplaceList = data;
        }.bind(this));

        WorkpositionDictonarySvcPromise.then(function(data) {
            this.workpositionList = data;
        }.bind(this));

        Promise.all([
            PscsSvcPromise,
            EducationsSvcPromise,
            OccupationsSvcPromise,
            PartiesSvcPromise,
            CampaignPositionsPromise,
            WorkplaceDictonarySvcPromise,
            WorkpositionDictonarySvcPromise
        ]).then(function() {
            this.unsetPreloader('form');
        }.bind(this));
    },

    mounted: function() {
        this.$watch('psc', function() {
            if (this.controlValidation.psc) {
                this.markValidated('psc');
            }
        }.bind(this));

        this.$watch('address', function() {
            if (this.controlValidation.address) {
                this.markValidated('address');
            }
        }.bind(this));

        this.$root.$on('activityPayloadShowing', function (payload) {
            this.$emit('bsModalShow', {
                id: 'user-activity-modal'
            });
        });
    },

    components: {
        'psc-placement': pscPlacementComponent,
        'address-creating': addressCreatingComponent,
        'user-activity': UserActivityComponent,
        'datepicker': datepickerComponent
    },

    methods: {
        save: function() {
            this.setPreloader('form');

            if (_.isEmpty(this.psc)) {
                this.markUnvalidated('psc', [ 'Будь ласка вкажіть дільницю' ]);
                this.unsetPreloader('form');
                return;
            }

            if (_.isEmpty(this.address)) {
                this.markUnvalidated('address', [ 'Будь ласка вкажіть адресу проживання виборця' ]);
                this.unsetPreloader('form');
                return;
            }

            var data = {
                gender_type: this.model.gender_type,
                surname: this.model.surname,
                firstname: this.model.firstname,
                patronymic: this.model.patronymic,
                phone_number: this.model.phone_number,
                additional_phone_number: this.model.additional_phone_number,
                email: this.model.email,
                education_type: this.model.education_type,
                occupation_type: this.model.occupation_type,
                party_id: this.model.party_id,
                address_id: this.address.id,
                campaign_position: this.model.campaign_position,
                is_newspaper_subscribed: this.model.address.is_newspaper_subscribed,
                workplace_keyword: this.model.workplace_keyword,
                workposition_keyword: this.model.workposition_keyword,
                birthday: this.model.birthday
            };

            this.$http.post('/voters/edit/' + this.model.id, data).then(function(response) {
                this.alert('Дані виборця успішно змінені', 'success').then(function() {
                    document.location.reload();
                });
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        },

        showAddressRequestModal: function() {
            this.$emit('addressRequestCreating', {});
        },

        showActivityPayloadModal: function(activity) {
            this.$emit('activityPayloadShowing', {
                activity: activity
            });
        },

        remove: function() {
            this.confirm('Ви дійсно бажаєте видалити цього виборця?').then(function() {
                this.setPreloader('form');
                this.$http.get('/voters/remove/' + this.model.id).then(function(response) {
                    this.alert('Виборець успішно видалений', 'success').then(function() {
                        document.location.href = response.data.url;
                    });
                }.bind(this));
            }.bind(this));
        },

        onUpdateWorkplaces: function (value) {
            this.workplaces.push(value);
            this.$set(this.model, 'workplace_keyword', value);
        },

        onUpdateWorkpositions: function(value) {
            this.workpositions.push(value);
            this.$set(this.model, 'workposition_keyword', value);
        }
    },

    filters: {
        prettifyDateTime: function(value) {
            return moment(value).format('D MMMM YYYY HH:mm:ss');
        },

        momentDiffYears: function(value) {
            return moment().add(value, 'years');
        },

        momentFormat: function(value, format) {
            return moment(value).format(format || 'YYYY-MM-DD');
        }
    },

    computed: {
        psc: {
            get: function() {
                var _index = _.findIndex(this.pscList, { id: this.model.address.psc_id });
                return _index < 0 ? {} : this.pscList[_index];
            },
            set: function(value) {
                if (value) {
                    this.$set(this.model.address, 'psc_id', value.id);
                }
            }
        },
        address: {
            get: function() {
                var _index = _.findIndex(this.addressList, { id: this.model.address_id });
                return _index < 0 ? {} : this.addressList[_index];
            },

            set: function(value) {
                this.$set(this.model, 'address_id', value.id);
            },
        },
        workplace: {
            get: function() {
                if (!this.model.workplace_keyword) {
                    var item = _.find(this.workplaceList, { id: this.model.workplace_id });
                    return item ? item.value : null;
                } else {
                    return this.model.workplace_keyword;
                }
            },

            set: function(value) {
                this.$set(this.model, 'workplace_keyword', value);
            },
        },
        workplaces: {
            get: function() {
                return _.map(this.workplaceList, 'value');
            },

            set: function(value) {
                this.workplaceList.push({ id: 0, value: value });
            },
        },
        workposition: {
            get: function() {
                if (!this.model.workposition_keyword) {
                    var item = _.find(this.workpositionList, { id: this.model.workposition_id });
                    return item ? item.value : null;
                } else {
                    return this.model.workposition_keyword;
                }
            },

            set: function(value) {
                this.$set(this.model, 'workposition_keyword', value);
            },
        },
        workpositions: {
            get: function() {
                return _.map(this.workpositionList, 'value');
            },

            set: function(value) {
                this.workpositionList.push({ id: 0, value: value });
            },
        }
    },

    watch: {
        psc: function(value) {
        	if (!value.locality) return;

            addressesVueService.fetch({ psc_id: value.id }).then(function(data) {
                this.addressList = data.map(function(item) {
                    var streetType = _.find(streetTypesVueService.get(), { id: item.street_type });
                    item.addressH = item.postcode + ', ' + streetType.short + ' ' + item.street_name + ', буд. ' + item.house_number;

                    if (item.building) {
                        item.addressH += ', корпус ' + item.building;
                    }

                    if (item.apartment_number) {
                        item.addressH += ', кв. ' + item.apartment_number;
                    }

                    return item;
                }.bind(this));
            }.bind(this));
        },
    }
});