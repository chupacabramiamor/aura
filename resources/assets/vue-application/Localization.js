module.exports = (function() {
    var currentIndex;
    var locales = [];

    return {
        register: function(localeName, options) {
            var name = _.camelCase(localeName);

            var _existedLocaleIndex = _.findIndex(locales, { name: name });

            locales.push({
                name: name,
                localeName: localeName,
                title: options.title || null,
                caption: options.caption || null,
                data: {}
            });
        },

        add: function(localeName, data) {
            var name = _.camelCase(localeName);
            var _existedLocaleIndex = _.findIndex(locales, { name: name });
            locales[_existedLocaleIndex].data = _.extend(locales[_existedLocaleIndex].data, data);

            /*var _existedLocaleIndex = _.findIndex(locales, { name: name });

            if (_existedLocaleIndex >= 0) {
                locales[_existedLocaleIndex].data = _.extend(locales[_existedLocaleIndex].data, data);
            } else locales.push(this.makeTransaltion(data));*/
        },

        setCurrent: function(locale) {
            var name = _.camelCase(locale);
            currentIndex = _.findIndex(locales, { name: name });

            if (currentIndex < 0) {
                throw 'locale_does_not_exist';
            }

            return locales[currentIndex];
        },

        translate: function(key) {
            return locales[currentIndex].data[key] || key;
        },

        next: function() {
            currentIndex++;

            if (currentIndex > locales.length - 1) {
                currentIndex = 0;
            }

            return this.setCurrent(locales[currentIndex].localeName);
        },

        prev: function() {
            currentIndex--;

            if (currentIndex < 0) {
                currentIndex = locales.length - 1;
            }

            return this.setCurrent(locales[currentIndex].localeName);
        },

        getCurrent: function() {
            return locales[currentIndex];
        },

        all: function() {
            return locales;
        },
    };
})();
