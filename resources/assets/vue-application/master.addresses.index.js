require('./boot.js');

var Vue = require('vue');
var VueResource = require('vue-resource');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var vValidateMixin = require('./mixins/validate.js');
var modalComponent = require('./components/modal.js');
var streetTypesVueService = require('./services/street-types.js');

Vue.use(VueResource);

streetTypesVueService.fetch().then(function(data) {
    window.streetTypeList = data;
});

var searchFormComponent = {
	template: '#search-form-tpl',
	mixins: [ vBlockUIMixin, vValidateMixin, vSwalMixin ],
	data: function() {
		return {
			model: {},
			locationList: []
		};
	},

	mounted: function() {
		this.$http.get('/addresses/locality-list').then(function(response) {
			this.locationList = response.data.map(function(item) {
				return {
					id: item.id,
					name: item.name
				};
			});
		}.bind(this));
	},

	methods: {
		submit: function() {
			var data = {
				locality_id: this.model.locality_id,
				street_name: this.model.street_name,
				house_number: this.model.house_number,
				building: this.model.building,
				apartment_number: this.model.apartment_number
			};

			this.setPreloader('form');
			this.$root.$emit('finding', { data: data });
			this.$http.post('/addresses/search', data).then(function(response) {
				this.$root.$emit('finded', response.data);
				this.unsetPreloader('form');

				if (response.data.length == 0) {
					this.alert('Неможливо знайти відповідні дані', 'info');
				}
			}.bind(this), function() {
				this.$root.$emit('finded', []);
				this.unsetPreloader('form');
			}.bind(this));
		}
	}
};


var addressListComponent = {
	template: '#address-list-tpl',
	mixins: [ vBlockUIMixin, vSwalMixin ],
	data: function() {
		return {
			list: [],
		};
	},

	mounted: function() {
		this.$root.$on('finding', function(payload) {
			this.setPreloader('list');
		}.bind(this));

		this.$root.$on('finded', function(payload) {
			this.unsetPreloader('list');
			this.list = payload;
		}.bind(this));
	},

	methods: {
		followEditing: function(id) {
			document.location = '/addresses/edit/' + id;
		}
	},

	filters: {
		transformStreetType: function(value) {
			return _.find(window.streetTypeList, {id: value}).value;
		}
	}
};

new Vue({
    el: '#vue-addresses-app',
    components: {
        'search-form': searchFormComponent,
        'address-list': addressListComponent
    },
    data: {},

    methods: {
    	followAdding: function() {
    		document.location = '/addresses/add';
    	}
    }
});