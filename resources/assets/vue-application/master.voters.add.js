require('./boot.js');

var Vue = require('vue');
var VueResource = require('vue-resource');
var vValidateMixin = require('./mixins/validate.js');
var vBlockUIMixin = require('./mixins/blockui.js');
var vSwalMixin = require('./mixins/swal.js');
var vLocalityMixin = require('./mixins/locality.js');
var modalComponent = require('./components/modal.js');
var addressCreatingComponent = require('./components/address-creating.js');
var datepickerComponent = require('./components/datepicker.js');

var pscsVueService = require('./services/pscs.js');
var educationsVueService = require('./services/educations.js');
var addressesVueService = require('./services/addresses.js');
var streetTypesVueService = require('./services/street-types.js');
var occupationsVueService = require('./services/occupations.js');
var partiesVueService = require('./services/parties.js');
var localitiesVueService = require('./services/localities.js');
var campaignPositionsVueService = require('./services/campaign-positions.js');
var streetVueService = require('./services/streets.js');
var dictonaryVueService = require('./services/dictonary.js');

Vue.component('v-modal', modalComponent.modal);
Vue.component('v-modal-header', modalComponent.modalHeader);

var WorkplaceDictonarySvcPromise = dictonaryVueService.fetch({scope: 2});
var WorkpositionDictonarySvcPromise = dictonaryVueService.fetch({scope: 3});

var pscPlacementComponent = {
    template: '#psc-placement-tpl',
    props: [ 'psc' ],
    mixins: [ vLocalityMixin ]
};

new Vue({
    el: '#vue-voters-add-app',

    mixins: [ vValidateMixin, vBlockUIMixin, vLocalityMixin, vSwalMixin ],

    data: function() {
        return {
            model: {},
            psc: {},
            locality: {},
            address: {},
            pscList: [],
            educationList: [],
            occupacyList: [],
            partyList: [],
            addressList: [],
            streetTypeList: [],
            campaignPositionsList: [],
            workplaceList: [],
            workpositionList: [],
        };
    },

    created: function() {
        this.setPreloader('form');

        var PscsSvcPromise = pscsVueService.fetch().then(function(data) {
            this.pscList = data;
        }.bind(this));

        var EducationsSvcPromise = educationsVueService.fetch().then(function(data) {
            this.educationList = data;
        }.bind(this));

        var OccupationsSvcPromise = occupationsVueService.fetch().then(function(data) {
            this.occupacyList = data;
        }.bind(this));

        var PartiesSvcPromise = partiesVueService.fetch().then(function(data) {
            this.partyList = data;
        }.bind(this));

        var StreetTypesSvcPromise = streetTypesVueService.fetch().then(function(data) {
            this.streetTypeList = data;
        }.bind(this));

        var CampaignPositionsPromise = campaignPositionsVueService.fetch().then(function(data) {
            this.campaignPositionsList = data;
        }.bind(this));

        WorkplaceDictonarySvcPromise.then(function(data) {
            this.workplaceList = data;
        }.bind(this));

        WorkpositionDictonarySvcPromise.then(function(data) {
            this.workpositionList = data;
        }.bind(this));

        Promise.all([ PscsSvcPromise, EducationsSvcPromise, OccupationsSvcPromise, PartiesSvcPromise, CampaignPositionsPromise ]).then(function() {
            this.unsetPreloader('form');
        }.bind(this));
    },

    mounted: function() {
        this.$watch('psc', function() {
            if (this.controlValidation.psc) {
                this.markValidated('psc');
            }
        }.bind(this));

        this.$watch('address', function() {
            if (this.controlValidation.address) {
                this.markValidated('address');
            }
        }.bind(this));
    },

    components: {
        'psc-placement': pscPlacementComponent,
        'address-creating': addressCreatingComponent,
        'datepicker': datepickerComponent
    },

    methods: {
        save: function() {
            this.setPreloader('form');

            if (_.isEmpty(this.psc)) {
                this.markUnvalidated('psc', [ 'Будь ласка вкажіть дільницю' ]);
                this.unsetPreloader('form');
                return;
            }

            if (_.isEmpty(this.address)) {
                this.markUnvalidated('address', [ 'Будь ласка вкажіть адресу проживання виборця' ]);
                this.unsetPreloader('form');
                return;
            }

            var data = {
                gender_type: this.model.gender_type,
                surname: this.model.surname,
                firstname: this.model.firstname,
                patronymic: this.model.patronymic,
                phone_number: this.model.phone_number,
                additional_phone_number: this.model.additional_phone_number,
                email: this.model.email,
                education_type: this.model.education_type,
                occupation_type: this.model.occupation_type,
                party_id: this.model.party_id,
                address_id: this.address.id,
                campaign_position: this.model.campaign_position,
                is_newspaper_subscribed: this.address.is_newspaper_subscribed,
                workplace_keyword: this.model.workplace_keyword,
                workposition_keyword: this.model.workposition_keyword,
                birthday: this.model.birthday
            };

            this.$http.post('/voters/add', data).then(function(response) {
                this.alert('Виборець успішно доданий', 'success').then(function() {
                    document.location = response.data.url;
                });
            }.bind(this), this.unsetPreloader.call(this, 'form'));
        },

        showAddressRequestModal: function() {
            this.$emit('addressRequestCreating', {});
        },

        onUpdateWorkplaces: function (value) {
            this.workplaces.push(value);
            this.$set(this.model, 'workplace_keyword', value);
        },

        onUpdateWorkpositions: function(value) {
            this.workpositions.push(value);
            this.$set(this.model, 'workposition_keyword', value);
        }
    },

    filters: {
        prettifyDateTime: function(value) {
            return moment(value).format('D MMMM YYYY HH:mm:ss');
        },

        momentDiffYears: function(value) {
            return moment().add(value, 'years');
        },

        momentFormat: function(value, format) {
            return moment(value).format(format || 'YYYY-MM-DD');
        }
    },

    computed: {
       workplace: {
            get: function() {
                if (!this.model.workplace_keyword) {
                    var item = _.find(this.workplaceList, { id: this.model.workplace_id });
                    return item ? item.value : null;
                } else {
                    return this.model.workplace_keyword;
                }
            },

            set: function(value) {
                this.$set(this.model, 'workplace_keyword', value);
            },
        },
        workplaces: {
            get: function() {
                return _.map(this.workplaceList, 'value');
            },

            set: function(value) {
                this.workplaceList.push({ id: 0, value: value });
            },
        },
        workposition: {
            get: function() {
                if (!this.model.workposition_keyword) {
                    var item = _.find(this.workpositionList, { id: this.model.workposition_id });
                    return item ? item.value : null;
                } else {
                    return this.model.workposition_keyword;
                }
            },

            set: function(value) {
                this.$set(this.model, 'workposition_keyword', value);
            },
        },
        workpositions: {
            get: function() {
                return _.map(this.workpositionList, 'value');
            },

            set: function(value) {
                this.workpositionList.push({ id: 0, value: value });
            },
        }
    },

    watch: {
        psc: function(value) {
            this.setPreloader('form');

            addressesVueService.fetch({ psc_id: value.id }).then(function(data) {
                this.addressList = data.map(function(item) {
                    var streetType = _.find(streetTypesVueService.get(), { id: item.street_type });
                    item.addressH = item.postcode + ', ' + streetType.short + ' ' + item.street_name + ', буд. ' + item.house_number;

                    if (item.building) {
                        item.addressH += ', корпус ' + item.building;
                    }

                    if (item.apartment_number) {
                        item.addressH += ', кв. ' + item.apartment_number;
                    }

                    return item;
                }.bind(this));

                this.unsetPreloader('form');
            }.bind(this));
        }
    }

});