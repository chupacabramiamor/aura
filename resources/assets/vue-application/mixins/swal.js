module.exports = {
    methods: {
        confirm: function(message, type) {
            type = type || 'warning';

            return new Promise(function(resolve, reject){
                swal({
                    title: message || 'Ви впевнені?',
                    text: "Усі зміни можуть будти втрачені!",
                    icon: type,
                    buttons: {
                        cancel: {
                            text: "Cancel",
                            value: false,
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "OK",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then(function(value) {
                    return value ? resolve() : reject();
                });
            });
        },

        alert: function(text, type) {
            type = type || 'warning';  // waring, success, info, error

            return new Promise(function(resolve, reject){
                swal({
                    text: text,
                    icon: type,
                    buttons: {
                        ok: {
                            text: "OK",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then(function(value) {
                    return value ? resolve() : reject();
                });
            });
        }
    }
};