// vBlockUIMixin

module.exports = {
    data: function() {
        return {
            preloaders: {}
        }
    },

    methods: {
        setPreloader: function(scope) {
            this.$set(this.preloaders, scope, true);
        },

        unsetPreloader: function(scope) {
            this.$set(this.preloaders, scope, false);
        },

        unsetAllPreloaders: function() {

            Object.keys(this.preloaders).map(function(item) {
                this.$set(this.preloaders, item, false);
            }.bind(this));
        }
    }
};