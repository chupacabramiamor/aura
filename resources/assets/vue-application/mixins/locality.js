var streetTypesSvc = require('./../services/street-types.js');
var streetTypeList = [];

streetTypesSvc.fetch().then(function(data) {
    streetTypeList = data;
});

module.exports = {
    filters: {
        prettifyLocality: function(location, params) {
            if (!location) return;

            var defaults = {
                typeField: 'type',
                nameField: 'name'
            };

            params = params ? _.extend(defaults, params) : defaults;

			const LOCALITY_TYPE_SPECIAL_STATUS = 1;         // Міста що мають спеціальний статус
			const LOCALITY_TYPE_REGION = 2;          // Область
			const LOCALITY_TYPE_AREA = 3;            // Район области
			const LOCALITY_TYPE_CITY = 4;
			const LOCALITY_TYPE_URBAN_VILLAGE = 5;
			const LOCALITY_TYPE_VILLAGE = 6;
			const LOCALITY_TYPE_LARGE_VILLAGE = 7;

            switch (location[params.typeField]) {
                case LOCALITY_TYPE_CITY:
                    return 'м. ' + location[params.nameField];

                case LOCALITY_TYPE_URBAN_VILLAGE:
                    return 'смт. ' + location[params.nameField];

                case LOCALITY_TYPE_VILLAGE:
                    return 'с. ' + location[params.nameField];

                case LOCALITY_TYPE_LARGE_VILLAGE:
                    return 'с-ще ' + location[params.nameField];

                case LOCALITY_TYPE_REGION:
                    return location[params.nameField] + ' обл.';

                case LOCALITY_TYPE_AREA:
                    return location[params.nameField] + ' р-н';

                default:
                return location[params.nameField];
            }
        },

        prettifyAddress: function(address, showPostcode) {
            showPostcode = showPostcode && false;

            var streetType = _.find(streetTypeList, { id: address.street_type });
            var result = (showPostcode ? address.postcode + ', ' : '') + streetType.short + ' ' + address.street_name + ', буд. ' + address.house_number;

            if (address.building) {
                result += ', корпус ' + address.building;
            }

            if (address.apartment_number) {
                result += ', кв. ' + address.apartment_number;
            }

            return result;
        }
    }
};