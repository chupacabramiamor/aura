var Vue = require('vue');

var vValidateMixin = {
    data: function() {
        return {
            controlValidation: {},
            prefix: 'model'
        }
    },

    created: function() {
        var $this = this;

        // if (Vue.http.interceptors.indexOf(this.onUnprocessedEntityException) == -1) {
            Vue.http.interceptors.push(this.onUnprocessedEntityException);
        // }
    },

    methods: {
        markAllValidated: function() {
            var $this = this;
            _.each(Object.keys(this.controlValidation), function(item) {
                $this.markValidated(item);
            });
        },

        markAllUnvalidated: function() {
            var $this = this;
            _.each(Object.keys(this.controlValidation), function(item) {
                $this.markUnvalidated(item);
            });
        },

        markValidated: function(field) {
            this.controlValidation[field] = false;
        },

        markUnvalidated: function(field, message) {
            this.$set(this.controlValidation, field, message);
            this.$watch(this.prefix + '.' + field, function() {
                if (this.controlValidation[field]) {
                    this.markValidated(field);
                }
            }.bind(this));
        },

        onUnprocessedEntityException: function(request, next) {
            var $this = this;

            next(function(response) {
                if (response.status == 422) {
                    $this.controlValidation = {};

                    _.each(response.data.errors, function(value, key) {
                        $this.markUnvalidated(key, value);
                    });
                }
            });
        }
    }
};

module.exports = vValidateMixin;